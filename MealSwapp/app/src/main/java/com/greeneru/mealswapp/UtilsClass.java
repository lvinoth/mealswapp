package com.greeneru.mealswapp;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.support.v4.widget.NestedScrollView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

public class UtilsClass {

    private static boolean isChangesMadeInRecipes;
    private static boolean isChangesMadeInProducts;

    private static SimpleDateFormat sdf = new SimpleDateFormat("HHmmss");
    private static Dialog progressDialog;

    public static void makeStatusBarTransparent(Window window, Context context)
    {
        if (Build.VERSION.SDK_INT >= 23) {
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }
        else
        {
            window.setStatusBarColor(context.getResources().getColor(R.color.darker_gray));
        }
    }

    public static SimpleDateFormat getSimpleDateTimeFormat()
    {
        return sdf;
    }

    public static String getBase64HashedString(String time)
    {
        String base64String = "";
        String key = "JO-G5lejF.4{G5(FC6e";

        String toHash = time + key;

        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(toHash.getBytes(StandardCharsets.UTF_8));

            base64String = Base64.encodeToString(md.digest(),Base64.DEFAULT);

            Log.d(RecipeFragment.class.getSimpleName(), "Base64 Key: " + base64String);

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return base64String;
    }

    public static boolean isValidEmail(String string){
        final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(string);
        return matcher.matches();
    }


    public static void showBusyAnimation(final Activity baseActivity, final String progressText) {

        baseActivity.runOnUiThread(new Runnable() {

            @Override
            public void run() {
                Log.d(UtilsClass.class.getSimpleName(),"showBusyAnimation");

                LayoutInflater inflater = (LayoutInflater) baseActivity.getSystemService(LAYOUT_INFLATER_SERVICE);
                View progressLayout = inflater.inflate(R.layout.progressdialog, null);

                TextView loadingText = progressLayout.findViewById(R.id.tv_loadingmsg);

                loadingText.setText(progressText);

                progressDialog = new Dialog(baseActivity);
                progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                progressDialog.setContentView(progressLayout);
                progressDialog.setCancelable(false);
                progressDialog.setCanceledOnTouchOutside(false);
                progressDialog.show();
            }
        });
    }


    public static void hideBusyAnimation(final Activity baseActivity) {

        baseActivity.runOnUiThread(new Runnable() {
            public void run() {
                Log.d(UtilsClass.class.getSimpleName(),"hideBusyAnimation");

                try {
                    if ((progressDialog != null) && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }
                } catch (final Exception e) {
                    e.printStackTrace();
                } finally {
                    progressDialog = null;
                }
            }
        });

    }

    public static boolean setListViewHeightBasedOnItems(ListView listView, final NestedScrollView scrollView) {

        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter != null) {

            int numberOfItems = listAdapter.getCount();

            // Get total height of all items.
            int totalItemsHeight = 0;
            int listWidth = listView.getMeasuredWidth();
            for (int i = 0; i < listAdapter.getCount(); i++) {
                View mView = listAdapter.getView(i, null, listView);
                mView.measure(View.MeasureSpec.makeMeasureSpec(listWidth, View.MeasureSpec.EXACTLY),
                        View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
                totalItemsHeight += mView.getMeasuredHeight();
            }

            // Get total height of all item dividers.
            int totalDividersHeight = listView.getDividerHeight() * (numberOfItems - 1);

            // Set list height.
            ViewGroup.LayoutParams params = listView.getLayoutParams();
            params.height = totalItemsHeight + totalDividersHeight;
            listView.setLayoutParams(params);
            listView.requestLayout();
            if (scrollView != null) {
                scrollView.getParent().requestChildFocus(scrollView, scrollView);
            }


            return true;

        } else {
            return false;
        }

    }

    public static boolean isIsChangesMadeInRecipes() {
        return isChangesMadeInRecipes;
    }

    public static void setIsChangesMadeInRecipes(boolean isChangesMadeInRecipes) {
        UtilsClass.isChangesMadeInRecipes = isChangesMadeInRecipes;
    }

    public static boolean isIsChangesMadeInProducts() {
        return isChangesMadeInProducts;
    }

    public static void setIsChangesMadeInProducts(boolean isChangesMadeInProducts) {
        UtilsClass.isChangesMadeInProducts = isChangesMadeInProducts;
    }
}
