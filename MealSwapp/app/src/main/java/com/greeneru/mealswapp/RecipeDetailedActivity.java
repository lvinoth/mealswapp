package com.greeneru.mealswapp;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.greeneru.mealswapp.Adapters.ApiRecipeDetailsAdapter;
import com.greeneru.mealswapp.Adapters.RecipeAdapter;
import com.greeneru.mealswapp.Adapters.RecipeDetailsAdapter;
import com.greeneru.mealswapp.Models.ApiRecipeDetailModel;
import com.greeneru.mealswapp.Models.RecipeDetailsModel;
import com.greeneru.mealswapp.Models.StatusModel;
import com.greeneru.mealswapp.Retrofit.ApiClient;
import com.greeneru.mealswapp.Retrofit.ApiInterface;
import com.greeneru.mealswapp.Retrofit.Data;
import com.greeneru.mealswapp.Utilities.SharedPreferenceUtility;
import com.squareup.picasso.Picasso;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.widget.Toast.LENGTH_LONG;

public class RecipeDetailedActivity extends AppCompatActivity {


    private TextView recipeTitle;
    private TextView recipePrice;
    private TextView recipePrepTime;
    private TextView recipeTotalFavorites;
    private TextView recipeCalories;
    private TextView recipeFat;
    private TextView recipeProtien;
    private TextView recipeCarbo;
    private TextView recipeSugar;
    private ImageView recipeImage;
    private TextView dataNotFoundView;
    private Button addIngredientsToCartBtn;
    private Button addIngredientsToCartBtn1;
    private Button recipeFavourited;
    private TextView utensilsData;
    private TextView instructions;
    private LinearLayout addMoreToCartLayout;
    private TextView decrementRecipeOrder;
    private TextView recipeCartQuantity;
    private TextView incrementRecipeOrder;
    private LinearLayout addMoreToCartLayout1;
    private TextView decrementRecipeOrder1;
    private TextView recipeCartQuantity1;
    private TextView incrementRecipeOrder1;



    private ListView ingredientsList;
    private static RecipeDetailsAdapter adapter;
    private static ApiRecipeDetailsAdapter apiAdapter;
    private NestedScrollView nestedScroll;
    private ApiInterface apiService;
    private Activity baseActivity;
    private static final String TAG = RecipeDetailedActivity.class.getSimpleName();
    private RecipeDetailsModel.Recipe recipeData;
    private List<RecipeDetailsModel.IngredientsDetail> recipeDetails;
    private RecipeDetailsModel.IngredientNutrition recipeNutritionData;

    private ApiRecipeDetailModel.Recipe apiRecipeData;
    private List<ApiRecipeDetailModel.Ingredients> apiRecipeDetails;
    private List<ApiRecipeDetailModel.Nutrition> apiRecipeNutritionData;
    private Toast mToast;
    private String recipeID;
    private String source;
    private String selectedTab;
    private ActionBar actionBar;
    private Toolbar toolbar;
    private String apiRecipePrice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe_detailed);
        Intent intent = getIntent();
        recipeID = intent.getStringExtra("recipeID");
        source = intent.getStringExtra("source");
        selectedTab = intent.getStringExtra("tab");

        baseActivity = this;

        apiService = ApiClient.getClient().create(ApiInterface.class);
        UtilsClass.makeStatusBarTransparent(getWindow(), this);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final TextView view = findViewById(R.id.toolbarTitleView);
        view.setText("RECIPE DETAILS");

        actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);

        recipeTitle = findViewById(R.id.recipeTitle);
        recipePrice = findViewById(R.id.recipePrice);
        recipePrepTime = findViewById(R.id.recipePreparationTime);
        recipeTotalFavorites = findViewById(R.id.recipeTotalFavourites);
        recipeCalories = findViewById(R.id.caloriesValue);
        recipeFat = findViewById(R.id.fatValue);
        recipeProtien = findViewById(R.id.proteinValue);
        recipeCarbo = findViewById(R.id.carboValue);
        recipeSugar = findViewById(R.id.sugarValue);
        recipeImage = findViewById(R.id.recipeImageView);
        dataNotFoundView = findViewById(R.id.ingredientsNotFoundView);
        addIngredientsToCartBtn = findViewById(R.id.addIngredientsToCartBtn);
        addIngredientsToCartBtn1 = findViewById(R.id.addIngredientsToCartBtn1);
        recipeFavourited = findViewById(R.id.favouriteButton);
        utensilsData = findViewById(R.id.utensilsData);
        instructions = findViewById(R.id.instructionsData);

        addMoreToCartLayout = findViewById(R.id.addMoreToCartLayout);
        decrementRecipeOrder = findViewById(R.id.decrementRecipeOrder);
        recipeCartQuantity = findViewById(R.id.recipeCartQuantity);
        incrementRecipeOrder = findViewById(R.id.incrementRecipeOrder);
        addMoreToCartLayout1 = findViewById(R.id.addMoreToCartLayout1);
        decrementRecipeOrder1 = findViewById(R.id.decrementRecipeOrder1);
        recipeCartQuantity1 = findViewById(R.id.recipeCartQuantity1);
        incrementRecipeOrder1 = findViewById(R.id.incrementRecipeOrder1);

        recipeFavourited.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (source != null)
                {
                    setFavoriteApiRecipe();
                }
                else {
                    setFavoriteRecipe();
                }
            }
        });

        ingredientsList = findViewById(R.id.ingredientsListView);
        nestedScroll = findViewById(R.id.nestedScroll);

        /*dataModels = new ArrayList<>();

        dataModels.add(new DataModel("Mustard","20","50","2 table spoon", "gms"));
        dataModels.add(new DataModel("Tomato","30","250","50 gms", "gms"));
        dataModels.add(new DataModel("Onion","40","250","50 gms", "gms"));
        dataModels.add(new DataModel("Potato","20","250","100 gms", "gms"));
        dataModels.add(new DataModel("Turmeric","10","100","1 pinch", "gms"));
        dataModels.add(new DataModel("Olive oil","80","2","4 table spoon", "ltr"));
        dataModels.add(new DataModel("Dhal","20","1","5 spoon", "kg"));*/
        recipeDetails = new ArrayList<>();
        adapter = new RecipeDetailsAdapter(recipeDetails, baseActivity, "-1");

        apiRecipeDetails = new ArrayList<>();
        apiAdapter = new ApiRecipeDetailsAdapter(apiRecipeDetails, baseActivity, "-1");

        if (source != null)
        {
            addIngredientsToCartBtn.setOnClickListener(apiRecipeListener);
            addIngredientsToCartBtn1.setOnClickListener(apiRecipeListener);
            decrementRecipeOrder.setOnClickListener(apiRecipeDecrementListener);
            decrementRecipeOrder1.setOnClickListener(apiRecipeDecrementListener);
            incrementRecipeOrder.setOnClickListener(apiRecipeIncrementListener);
            incrementRecipeOrder1.setOnClickListener(apiRecipeIncrementListener);
            getApiRecipesDetail(recipeID);
        }
        else {
            addIngredientsToCartBtn.setOnClickListener(recipeListener);
            addIngredientsToCartBtn1.setOnClickListener(recipeListener);
            decrementRecipeOrder.setOnClickListener(recipeDecrementListener);
            decrementRecipeOrder1.setOnClickListener(recipeDecrementListener);
            incrementRecipeOrder.setOnClickListener(recipeIncrementListener);
            incrementRecipeOrder1.setOnClickListener(recipeIncrementListener);
            getRecipesDetail();
        }
        setAddToCartBg();
    }

    private void setAddToCartBg()
    {

        if (selectedTab.equals("0") || selectedTab.equals("-1"))
        {
            addIngredientsToCartBtn.setBackground(getDrawable(R.drawable.add_cart_blue));
            addIngredientsToCartBtn1.setBackground(getDrawable(R.drawable.add_cart_blue));
            addMoreToCartLayout.setBackground(getDrawable(R.drawable.add_cart_blue));
            addMoreToCartLayout1.setBackground(getDrawable(R.drawable.add_cart_blue));
        }
        else if (selectedTab.equals("1"))
        {
            addIngredientsToCartBtn.setBackground(getDrawable(R.drawable.add_cart_green));
            addIngredientsToCartBtn1.setBackground(getDrawable(R.drawable.add_cart_green));
            addMoreToCartLayout.setBackground(getDrawable(R.drawable.add_cart_green));
            addMoreToCartLayout1.setBackground(getDrawable(R.drawable.add_cart_green));
        }
        else {
            addIngredientsToCartBtn.setBackground(getDrawable(R.drawable.add_cart_red));
            addIngredientsToCartBtn1.setBackground(getDrawable(R.drawable.add_cart_red));
            addMoreToCartLayout.setBackground(getDrawable(R.drawable.add_cart_red));
            addMoreToCartLayout1.setBackground(getDrawable(R.drawable.add_cart_red));
        }
    }


    private void setFavoriteRecipe()
    {
        String isFav = recipeData.getFavID()!=null?"0":"1";

                /*if (recipe.getFavID()!= null) {
                    holder.favouriteBtn.setBackground(mContext.getResources().getDrawable(R.drawable.ic_fav_unsel));
                } else {
                    holder.favouriteBtn.setBackground(mContext.getResources().getDrawable(R.drawable.ic_fav_sel));
                }*/
        UtilsClass.showBusyAnimation(baseActivity,"");
        Call<StatusModel> call = apiService.makeFavorite(SharedPreferenceUtility.getUserId(getApplicationContext()),recipeData.getRecipeID(),"2", isFav);

        call.enqueue(new Callback<StatusModel>() {
            @Override
            public void onResponse(Call<StatusModel> call, final Response<StatusModel> response) {
                UtilsClass.hideBusyAnimation(baseActivity);
                if (response.body() != null) {
                    if (response.body().getStatus())
                    {
                        // change the button background
                        if (recipeData.getFavID() == null){
                            recipeData.setFavID("2");
                            recipeFavourited.setBackground(getResources().getDrawable(R.drawable.fav_yes_det));
                        }
                        else {
                            recipeData.setFavID(null);
                            recipeFavourited.setBackground(getResources().getDrawable(R.drawable.fav_no_det));
                        }
                        UtilsClass.setIsChangesMadeInRecipes(true);

                    }
                    else {
                        if (recipeData.getFavID() == null){
                            recipeFavourited.setBackground(getResources().getDrawable(R.drawable.fav_no_det));
                        }
                        else {
                            recipeFavourited.setBackground(getResources().getDrawable(R.drawable.fav_yes_det));
                        }
                        UtilsClass.setIsChangesMadeInRecipes(false);
                        Log.d(RecipeAdapter.class.getSimpleName(), "Response: " + response.body().getMessage());

                    }

                } else {
                    if (recipeData.getFavID() == null){
                        recipeFavourited.setBackground(getResources().getDrawable(R.drawable.fav_no_det));
                    }
                    else {
                        recipeFavourited.setBackground(getResources().getDrawable(R.drawable.fav_yes_det));
                    }
                    UtilsClass.setIsChangesMadeInRecipes(false);
                    Log.d(RecipeAdapter.class.getSimpleName(), "Response: " + response.raw().message());
                }
            }

            @Override
            public void onFailure(Call<StatusModel> call, Throwable t) {
                UtilsClass.hideBusyAnimation(baseActivity);
                if (recipeData.getFavID() == null){
                    recipeFavourited.setBackground(getResources().getDrawable(R.drawable.fav_no_det));
                }
                else {
                    recipeFavourited.setBackground(getResources().getDrawable(R.drawable.fav_yes_det));
                }
                UtilsClass.setIsChangesMadeInRecipes(false);
                Log.d(RecipeAdapter.class.getSimpleName(), "Response: " + "Something went wrong!!!!");

            }
        });
    }

    private void setFavoriteApiRecipe()
    {
        String isFav = apiRecipeData.getFavID()!=null?"0":"1";

                /*if (recipe.getFavID()!= null) {
                    holder.favouriteBtn.setBackground(mContext.getResources().getDrawable(R.drawable.ic_fav_unsel));
                } else {
                    holder.favouriteBtn.setBackground(mContext.getResources().getDrawable(R.drawable.ic_fav_sel));
                }*/
        UtilsClass.showBusyAnimation(baseActivity,"");
        Call<StatusModel> call = apiService.makeFavorite(SharedPreferenceUtility.getUserId(getApplicationContext()),apiRecipeData.getRecipeID(),"3", isFav);

        call.enqueue(new Callback<StatusModel>() {
            @Override
            public void onResponse(Call<StatusModel> call, final Response<StatusModel> response) {
                UtilsClass.hideBusyAnimation(baseActivity);
                if (response.body() != null) {
                    if (response.body().getStatus())
                    {
                        // change the button background
                        if (apiRecipeData.getFavID() == null){
                            apiRecipeData.setFavID("2");
                            recipeFavourited.setBackground(getResources().getDrawable(R.drawable.fav_yes_det));
                        }
                        else {
                            apiRecipeData.setFavID(null);
                            recipeFavourited.setBackground(getResources().getDrawable(R.drawable.fav_no_det));
                        }
                        UtilsClass.setIsChangesMadeInRecipes(true);

                    }
                    else {
                        if (apiRecipeData.getFavID() == null){
                            recipeFavourited.setBackground(getResources().getDrawable(R.drawable.fav_no_det));
                        }
                        else {
                            recipeFavourited.setBackground(getResources().getDrawable(R.drawable.fav_yes_det));
                        }
                        UtilsClass.setIsChangesMadeInRecipes(false);
                        Log.d(RecipeAdapter.class.getSimpleName(), "Response: " + response.body().getMessage());

                    }

                } else {
                    if (apiRecipeData.getFavID() == null){
                        recipeFavourited.setBackground(getResources().getDrawable(R.drawable.fav_no_det));
                    }
                    else {
                        recipeFavourited.setBackground(getResources().getDrawable(R.drawable.fav_yes_det));
                    }
                    UtilsClass.setIsChangesMadeInRecipes(false);
                    Log.d(RecipeAdapter.class.getSimpleName(), "Response: " + response.raw().message());
                }
            }

            @Override
            public void onFailure(Call<StatusModel> call, Throwable t) {
                UtilsClass.hideBusyAnimation(baseActivity);
                if (apiRecipeData.getFavID() == null){
                    recipeFavourited.setBackground(getResources().getDrawable(R.drawable.fav_no_det));
                }
                else {
                    recipeFavourited.setBackground(getResources().getDrawable(R.drawable.fav_yes_det));
                }
                UtilsClass.setIsChangesMadeInRecipes(false);
                Log.d(RecipeAdapter.class.getSimpleName(), "Response: " + "Something went wrong!!!!");

            }
        });
    }

    private View.OnClickListener recipeDecrementListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            decrementCartRecipe();
        }
    };

    private View.OnClickListener recipeIncrementListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            incrementCartRecipe();
        }
    };

    private View.OnClickListener apiRecipeDecrementListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            decrementApiCartRecipe();
        }
    };

    private View.OnClickListener apiRecipeIncrementListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            incrementApiCartRecipe();
        }
    };


    private View.OnClickListener recipeListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            List<Data> data = new ArrayList<>();

            Data ingData;

            for(int i=0;i<recipeDetails.size();i++)
            {
                if (!recipeDetails.get(i).getSalePrice().equals("0")/* && !recipeDetails.get(i).getIngredient_qty().equals("0")*/) {
                    ingData = new Data();
                    ingData.setIngredientId(recipeDetails.get(i).getId());
                    ingData.setIngredientName(recipeDetails.get(i).getItemName());
                    ingData.setQty(recipeDetails.get(i).getIngredient_qty());
                    data.add(ingData);
                }
            }

            Gson gson = new Gson();
            Type type = new TypeToken<List<Data>>() {}.getType();
            String objListString = gson.toJsonTree(data, type).toString();


            addRecipesToCart(recipeData.getRecipeID(),recipeNutritionData.getTotprice(),"1","2","1",objListString);
        }
    };

    private View.OnClickListener apiRecipeListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            List<Data> data = new ArrayList<>();

            Data ingData;

            for(int i=0;i<apiRecipeDetails.size();i++)
            {
                ingData = new Data();
                ingData.setIngredientId(apiRecipeDetails.get(i).getId());
                ingData.setIngredientName(apiRecipeDetails.get(i).getItemName());
                ingData.setQty(apiRecipeDetails.get(i).getWeight());
                data.add(ingData);
            }

            Gson gson = new Gson();
            Type type = new TypeToken<List<Data>>() {}.getType();
            String objListString = gson.toJsonTree(data, type).toString();

            addApiRecipesToCart(apiRecipeData.getRecipeID(),apiRecipePrice,"1","3","1",objListString);
        }
    };

    private void addRecipesToCart(String recipeId, String price, String quantity, final String cartType, String addDetail, String data) {

        UtilsClass.showBusyAnimation(baseActivity,"");
        Call<StatusModel> call = apiService.addItemsToCart(SharedPreferenceUtility.getUserId(getApplicationContext()),recipeId,price,quantity,cartType,addDetail,data,"","1");

        call.enqueue(new Callback<StatusModel>() {
            @Override
            public void onResponse(Call<StatusModel> call, final Response<StatusModel> response) {
                UtilsClass.hideBusyAnimation(baseActivity);
                if (response.body() != null) {
                    if (response.body().getStatus())
                    {
                        // change the button background
                        addIngredientsToCartBtn.setVisibility(View.GONE);
                        addIngredientsToCartBtn1.setVisibility(View.GONE);
                        addMoreToCartLayout.setVisibility(View.VISIBLE);
                        addMoreToCartLayout1.setVisibility(View.VISIBLE);
                        String quantity = "1 serving";
                        recipeData.setCartqty("1");
                        recipeCartQuantity.setText(quantity);
                        adapter.setItemsAddedToCart(true);
                        UtilsClass.setIsChangesMadeInRecipes(true);
                        refreshRecipes();

                    }
                    else {
                        // retain the button background
                        adapter.setItemsAddedToCart(false);
                        addIngredientsToCartBtn.setVisibility(View.VISIBLE);
                        addIngredientsToCartBtn1.setVisibility(View.VISIBLE);
                        addMoreToCartLayout.setVisibility(View.GONE);
                        addMoreToCartLayout1.setVisibility(View.GONE);
                        UtilsClass.setIsChangesMadeInRecipes(false);
                        Log.d(RecipeAdapter.class.getSimpleName(), "Response: " + response.body().getMessage());
                    }

                } else {
                    // retain the button background
                    adapter.setItemsAddedToCart(false);
                    addIngredientsToCartBtn.setVisibility(View.VISIBLE);
                    addIngredientsToCartBtn1.setVisibility(View.VISIBLE);
                    addMoreToCartLayout.setVisibility(View.GONE);
                    addMoreToCartLayout1.setVisibility(View.GONE);
                    UtilsClass.setIsChangesMadeInRecipes(false);
                    Log.d(RecipeAdapter.class.getSimpleName(), "Response: " + response.raw().message());
                }
            }

            @Override
            public void onFailure(Call<StatusModel> call, Throwable t) {
                UtilsClass.hideBusyAnimation(baseActivity);
                // retain the button background
                adapter.setItemsAddedToCart(false);
                addIngredientsToCartBtn.setVisibility(View.VISIBLE);
                addIngredientsToCartBtn1.setVisibility(View.VISIBLE);
                addMoreToCartLayout.setVisibility(View.GONE);
                addMoreToCartLayout1.setVisibility(View.GONE);
                UtilsClass.setIsChangesMadeInRecipes(false);
                Log.d(RecipeAdapter.class.getSimpleName(), "Response: " + "Something went wrong!!!!");

            }
        });

    }

    private void addApiRecipesToCart(String recipeId, String price, String quantity, final String cartType, String addDetail, String data) {
        UtilsClass.showBusyAnimation(baseActivity,"");
        Call<StatusModel> call = apiService.addItemsToCart(SharedPreferenceUtility.getUserId(getApplicationContext()),recipeId,price,quantity,cartType,addDetail,data,"","1");

        call.enqueue(new Callback<StatusModel>() {
            @Override
            public void onResponse(Call<StatusModel> call, final Response<StatusModel> response) {
                UtilsClass.hideBusyAnimation(baseActivity);
                if (response.body() != null) {
                    if (response.body().getStatus())
                    {
                        // change the button background
                        addIngredientsToCartBtn.setVisibility(View.GONE);
                        addIngredientsToCartBtn1.setVisibility(View.GONE);
                        addMoreToCartLayout.setVisibility(View.VISIBLE);
                        addMoreToCartLayout1.setVisibility(View.VISIBLE);
                        String quantity = "1 serving";
                        apiRecipeData.setCartqty("1");
                        recipeCartQuantity.setText(quantity);
                        apiAdapter.setItemsAddedToCart(true);
                        UtilsClass.setIsChangesMadeInRecipes(true);
                        refreshRecipes();
                    }
                    else {
                        // retain the button background
                        apiAdapter.setItemsAddedToCart(false);
                        addIngredientsToCartBtn.setVisibility(View.VISIBLE);
                        addIngredientsToCartBtn1.setVisibility(View.VISIBLE);
                        addMoreToCartLayout.setVisibility(View.GONE);
                        addMoreToCartLayout1.setVisibility(View.GONE);
                        UtilsClass.setIsChangesMadeInRecipes(false);
                        Log.d(RecipeAdapter.class.getSimpleName(), "Response: " + response.body().getMessage());
                    }

                } else {
                    // retain the button background
                    apiAdapter.setItemsAddedToCart(false);
                    addIngredientsToCartBtn.setVisibility(View.VISIBLE);
                    addIngredientsToCartBtn1.setVisibility(View.VISIBLE);
                    addMoreToCartLayout.setVisibility(View.GONE);
                    addMoreToCartLayout1.setVisibility(View.GONE);
                    UtilsClass.setIsChangesMadeInRecipes(false);
                    Log.d(RecipeAdapter.class.getSimpleName(), "Response: " + response.raw().message());
                }
            }

            @Override
            public void onFailure(Call<StatusModel> call, Throwable t) {
                UtilsClass.hideBusyAnimation(baseActivity);
                // retain the button background
                apiAdapter.setItemsAddedToCart(false);
                addIngredientsToCartBtn.setVisibility(View.VISIBLE);
                addIngredientsToCartBtn1.setVisibility(View.VISIBLE);
                addMoreToCartLayout.setVisibility(View.GONE);
                addMoreToCartLayout1.setVisibility(View.GONE);
                UtilsClass.setIsChangesMadeInRecipes(false);
                Log.d(RecipeAdapter.class.getSimpleName(), "Response: " + "Something went wrong!!!!");

            }
        });

    }

    private void refreshRecipes()
    {
        if (source != null)
        {
            getApiRecipesDetail(recipeID);
        }
        else {
            getRecipesDetail();
        }
    }

    private void decrementCartRecipe() {


        String q = recipeData.getCartqty();
        final int quantity = Integer.parseInt(q) - 1;

        if (quantity != 0) {


            String cartId = recipeData.getCartId() == null ? "" : recipeData.getCartId();

            List<Data> data = new ArrayList<>();

            Data ingData;

            for (int i = 0; i < recipeDetails.size(); i++) {
                if (!recipeDetails.get(i).getSalePrice().equals("0")/* && !recipeDetails.get(i).getIngredient_qty().equals("0")*/) {
                    ingData = new Data();
                    ingData.setIngredientId(recipeDetails.get(i).getId());
                    ingData.setIngredientName(recipeDetails.get(i).getItemName());
                    ingData.setQty(recipeDetails.get(i).getIngredient_qty());
                    data.add(ingData);
                }
            }

            Gson gson = new Gson();
            Type type = new TypeToken<List<Data>>() {
            }.getType();
            String objListString = gson.toJsonTree(data, type).toString();
            UtilsClass.showBusyAnimation(baseActivity,"");
            Call<StatusModel> call = apiService.addItemsToCart(SharedPreferenceUtility.getUserId(this), recipeData.getRecipeID(), recipeNutritionData.getTotprice(), String.valueOf(quantity), "2", "1", objListString, cartId, "0");

            call.enqueue(new Callback<StatusModel>() {
                @Override
                public void onResponse(Call<StatusModel> call, final Response<StatusModel> response) {
                    UtilsClass.hideBusyAnimation(baseActivity);
                    if (response.body() != null) {
                        if (response.body().getStatus()) {

                            recipeData.setCartqty(String.valueOf(quantity));

                            if (quantity == 0) {

                                addIngredientsToCartBtn.setVisibility(View.VISIBLE);
                                addIngredientsToCartBtn1.setVisibility(View.VISIBLE);
                                addMoreToCartLayout.setVisibility(View.GONE);
                                addMoreToCartLayout1.setVisibility(View.GONE);

                            } else {
                                String quant = quantity > 1 ? quantity + " servings" : quantity + " serving";
                                recipeCartQuantity.setText(quant);
                                recipeCartQuantity1.setText(quant);
                            }
                            UtilsClass.setIsChangesMadeInRecipes(true);
                            refreshRecipes();

                        } else {
                            // retain the button background
                            UtilsClass.setIsChangesMadeInRecipes(false);
                            Log.d(RecipeAdapter.class.getSimpleName(), "Response: " + response.body().getMessage());
                        }

                    } else {
                        // retain the button background
                        UtilsClass.setIsChangesMadeInRecipes(false);
                        Log.d(RecipeAdapter.class.getSimpleName(), "Response: " + response.raw().message());
                    }
                }

                @Override
                public void onFailure(Call<StatusModel> call, Throwable t) {
                    UtilsClass.hideBusyAnimation(baseActivity);
                    // retain the button background
                    UtilsClass.setIsChangesMadeInRecipes(false);
                    Log.d(RecipeAdapter.class.getSimpleName(), "Response: " + "Something went wrong!!!!");

                }
            });
        }
    }

    private void decrementApiCartRecipe() {

        String q = apiRecipeData.getCartqty();
        final int quantity = Integer.parseInt(q) - 1;
        if (quantity != 0) {

            String cartId = apiRecipeData.getCartId() == null ? "" : apiRecipeData.getCartId();

            List<Data> data = new ArrayList<>();

            Data ingData;

            for (int i = 0; i < apiRecipeDetails.size(); i++) {
                if (!apiRecipeDetails.get(i).getSalePrice().equals("0")/* && !recipeDetails.get(i).getIngredient_qty().equals("0")*/) {
                    ingData = new Data();
                    ingData.setIngredientId(apiRecipeDetails.get(i).getId());
                    ingData.setIngredientName(apiRecipeDetails.get(i).getItemName());
                    ingData.setQty(apiRecipeDetails.get(i).getIngredient_qty());
                    data.add(ingData);
                }
            }

            Gson gson = new Gson();
            Type type = new TypeToken<List<Data>>() {
            }.getType();
            String objListString = gson.toJsonTree(data, type).toString();
            UtilsClass.showBusyAnimation(baseActivity,"");
            Call<StatusModel> call = apiService.addItemsToCart(SharedPreferenceUtility.getUserId(this), apiRecipeData.getRecipeID(), apiRecipePrice, String.valueOf(quantity), "3", "1", objListString, cartId, "0");

            call.enqueue(new Callback<StatusModel>() {
                @Override
                public void onResponse(Call<StatusModel> call, final Response<StatusModel> response) {
                    UtilsClass.hideBusyAnimation(baseActivity);
                    if (response.body() != null) {
                        if (response.body().getStatus()) {

                            apiRecipeData.setCartqty(String.valueOf(quantity));

                            if (quantity == 0) {

                                addIngredientsToCartBtn.setVisibility(View.VISIBLE);
                                addIngredientsToCartBtn1.setVisibility(View.VISIBLE);
                                addMoreToCartLayout.setVisibility(View.GONE);
                                addMoreToCartLayout1.setVisibility(View.GONE);

                            } else {
                                String quant = quantity > 1 ? quantity + " servings" : quantity + " serving";
                                recipeCartQuantity.setText(quant);
                                recipeCartQuantity1.setText(quant);
                            }
                            UtilsClass.setIsChangesMadeInRecipes(true);
                            refreshRecipes();
                        } else {
                            // retain the button background
                            UtilsClass.setIsChangesMadeInRecipes(false);
                            Log.d(RecipeAdapter.class.getSimpleName(), "Response: " + response.body().getMessage());
                        }

                    } else {
                        // retain the button background
                        UtilsClass.setIsChangesMadeInRecipes(false);
                        Log.d(RecipeAdapter.class.getSimpleName(), "Response: " + response.raw().message());
                    }
                }

                @Override
                public void onFailure(Call<StatusModel> call, Throwable t) {
                    UtilsClass.hideBusyAnimation(baseActivity);
                    // retain the button background
                    UtilsClass.setIsChangesMadeInRecipes(false);
                    Log.d(RecipeAdapter.class.getSimpleName(), "Response: " + "Something went wrong!!!!");

                }
            });
        }
    }


    private void incrementCartRecipe() {


        String q = recipeData.getCartqty();
        final int quantity = Integer.parseInt(q) + 1;

        String cartId = recipeData.getCartId() == null ? "" : recipeData.getCartId();

        List<Data> data = new ArrayList<>();

        Data ingData;

        for (int i = 0; i < recipeDetails.size(); i++) {
            if (!recipeDetails.get(i).getSalePrice().equals("0")/* && !recipeDetails.get(i).getIngredient_qty().equals("0")*/) {
                ingData = new Data();
                ingData.setIngredientId(recipeDetails.get(i).getId());
                ingData.setIngredientName(recipeDetails.get(i).getItemName());
                ingData.setQty(recipeDetails.get(i).getIngredient_qty());
                data.add(ingData);
            }
        }

        Gson gson = new Gson();
        Type type = new TypeToken<List<Data>>() {
        }.getType();
        String objListString = gson.toJsonTree(data, type).toString();
        UtilsClass.showBusyAnimation(baseActivity,"");
        Call<StatusModel> call = apiService.addItemsToCart(SharedPreferenceUtility.getUserId(this), recipeData.getRecipeID(), recipeNutritionData.getTotprice(), String.valueOf(quantity), "2", "1", objListString, cartId,"1");

        call.enqueue(new Callback<StatusModel>() {
            @Override
            public void onResponse(Call<StatusModel> call, final Response<StatusModel> response) {
                UtilsClass.hideBusyAnimation(baseActivity);
                if (response.body() != null) {
                    if (response.body().getStatus()) {

                        recipeData.setCartqty(String.valueOf(quantity));
                        String quant = quantity > 1 ? quantity + " servings" : quantity + " serving";
                        recipeCartQuantity.setText(quant);
                        recipeCartQuantity1.setText(quant);
                        UtilsClass.setIsChangesMadeInRecipes(true);
                        refreshRecipes();

                    } else {
                        // retain the button background
                        UtilsClass.setIsChangesMadeInRecipes(false);
                        Log.d(RecipeAdapter.class.getSimpleName(), "Response: " + response.body().getMessage());
                    }

                } else {
                    // retain the button background
                    UtilsClass.setIsChangesMadeInRecipes(false);
                    Log.d(RecipeAdapter.class.getSimpleName(), "Response: " + response.raw().message());
                }
            }

            @Override
            public void onFailure(Call<StatusModel> call, Throwable t) {
                UtilsClass.hideBusyAnimation(baseActivity);
                // retain the button background
                UtilsClass.setIsChangesMadeInRecipes(false);
                Log.d(RecipeAdapter.class.getSimpleName(), "Response: " + "Something went wrong!!!!");

            }
        });
    }

    private void incrementApiCartRecipe() {


        String q = apiRecipeData.getCartqty();
        final int quantity = Integer.parseInt(q) + 1;

        String cartId = apiRecipeData.getCartId() == null ? "" : apiRecipeData.getCartId();

        List<Data> data = new ArrayList<>();

        Data ingData;

        for (int i = 0; i < apiRecipeDetails.size(); i++) {
            if (!apiRecipeDetails.get(i).getSalePrice().equals("0")/* && !apiRecipeDetails.get(i).getIngredient_qty().equals("0")*/) {
                ingData = new Data();
                ingData.setIngredientId(apiRecipeDetails.get(i).getId());
                ingData.setIngredientName(apiRecipeDetails.get(i).getItemName());
                ingData.setQty(apiRecipeDetails.get(i).getIngredient_qty());
                data.add(ingData);
            }
        }

        Gson gson = new Gson();
        Type type = new TypeToken<List<Data>>() {
        }.getType();
        String objListString = gson.toJsonTree(data, type).toString();
        UtilsClass.showBusyAnimation(baseActivity,"");
        Call<StatusModel> call = apiService.addItemsToCart(SharedPreferenceUtility.getUserId(this), apiRecipeData.getRecipeID(), apiRecipePrice, String.valueOf(quantity), "3", "1", objListString, cartId,"1");

        call.enqueue(new Callback<StatusModel>() {
            @Override
            public void onResponse(Call<StatusModel> call, final Response<StatusModel> response) {
                UtilsClass.hideBusyAnimation(baseActivity);
                if (response.body() != null) {
                    if (response.body().getStatus()) {

                        apiRecipeData.setCartqty(String.valueOf(quantity));

                        String quant = quantity > 1 ? quantity + " servings" : quantity + " serving";
                        recipeCartQuantity.setText(quant);
                        recipeCartQuantity1.setText(quant);
                        UtilsClass.setIsChangesMadeInRecipes(true);
                        refreshRecipes();

                    } else {
                        // retain the button background
                        UtilsClass.setIsChangesMadeInRecipes(false);
                        Log.d(RecipeAdapter.class.getSimpleName(), "Response: " + response.body().getMessage());
                    }

                } else {
                    // retain the button background
                    UtilsClass.setIsChangesMadeInRecipes(false);
                    Log.d(RecipeAdapter.class.getSimpleName(), "Response: " + response.raw().message());
                }
            }

            @Override
            public void onFailure(Call<StatusModel> call, Throwable t) {
                UtilsClass.hideBusyAnimation(baseActivity);
                // retain the button background
                UtilsClass.setIsChangesMadeInRecipes(false);
                Log.d(RecipeAdapter.class.getSimpleName(), "Response: " + "Something went wrong!!!!");

            }
        });
    }


    private void getApiRecipesDetail(String recipeID) {
        UtilsClass.showBusyAnimation(baseActivity, "Loading..");
        Call<ApiRecipeDetailModel> call = apiService.getApiRecipesDetail(SharedPreferenceUtility.getUserId(getApplicationContext()), recipeID);

        call.enqueue(new Callback<ApiRecipeDetailModel>() {
            @Override
            public void onResponse(Call<ApiRecipeDetailModel> call, final Response<ApiRecipeDetailModel> response) {
                UtilsClass.hideBusyAnimation(baseActivity);

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (response.body() != null) {
                            if (response.body().isStatus()) {

                                apiRecipeData = response.body().getRecipe();

                                recipeTitle.setText(apiRecipeData.getRecipeName());
                                Picasso.with(baseActivity).load(apiRecipeData.getRecipeImage())
                                        .placeholder(R.drawable.food_ph)
                                        .error(R.drawable.food_ph)
                                        .into(recipeImage);
                                String timeToCook = (apiRecipeData.getTotalTime() == null ? "0 mins": apiRecipeData.getTotalTime());
                                recipePrepTime.setText(timeToCook);

                                apiRecipeNutritionData = response.body().getNutrition();
                                apiRecipeDetails = response.body().getIngredients();

                                recipeCalories.setText(apiRecipeData.getCalories());
                                recipeProtien.setText(apiRecipeNutritionData.get(3).getItemValue()+"g");
                                recipeFat.setText(apiRecipeNutritionData.get(0).getItemValue()+"g");
                                recipeCarbo.setText(apiRecipeNutritionData.get(1).getItemValue()+"g");
                                recipeSugar.setText(apiRecipeNutritionData.get(2).getItemValue()+"g");

                                if (apiRecipeData.getCartId() != null)
                                {
                                    addIngredientsToCartBtn.setVisibility(View.GONE);
                                    addIngredientsToCartBtn1.setVisibility(View.GONE);
                                    addMoreToCartLayout.setVisibility(View.VISIBLE);
                                    addMoreToCartLayout1.setVisibility(View.VISIBLE);
                                    String q = apiRecipeData.getCartqty();
                                    int quantity = Integer.parseInt(q);

                                    String quant = quantity > 1 ? quantity + " servings" : quantity + " serving";
                                    recipeCartQuantity.setText(quant);
                                    recipeCartQuantity1.setText(quant);
                                }
                                else {
                                    addIngredientsToCartBtn.setVisibility(View.VISIBLE);
                                    addIngredientsToCartBtn1.setVisibility(View.VISIBLE);
                                    addMoreToCartLayout.setVisibility(View.GONE);
                                    addMoreToCartLayout1.setVisibility(View.GONE);
                                }


                                utensilsData.setText("No data found");
                                instructions.setText("No data found");

                                if (apiRecipeData.getFavID() == null)
                                {
                                    recipeFavourited.setBackground(getResources().getDrawable(R.drawable.fav_no_det));
                                }
                                else
                                {
                                    recipeFavourited.setBackground(getResources().getDrawable(R.drawable.fav_yes_det));
                                }

                                apiRecipePrice = response.body().getTotalprice();
                                String price = getString(R.string.rupee_symbol) +" "+ apiRecipePrice;
                                recipePrice.setText(price);

                                if(apiRecipeDetails.size() >= 1 && apiRecipeDetails.get(0) != null)
                                {
                                    apiAdapter = new ApiRecipeDetailsAdapter(apiRecipeDetails, baseActivity, apiRecipeData.getRecipeID());
                                    if (apiRecipeData.getCartId() != null)
                                    {
                                        apiAdapter.setItemsAddedToCart(true);
                                    }
                                    else {
                                        apiAdapter.setItemsAddedToCart(false);
                                    }
                                    ingredientsList.setFocusable(false);
                                    ingredientsList.setAdapter(apiAdapter);
                                    apiAdapter.notifyDataSetChanged();
                                    UtilsClass.setListViewHeightBasedOnItems(ingredientsList, nestedScroll);
                                }
                                else
                                {
                                    dataNotFoundView.setVisibility(View.VISIBLE);
                                    ingredientsList.setVisibility(View.INVISIBLE);
                                }
                            }
                            else // Handle failure cases here
                            {
                                String mess = response.body().getMessage();
                                Log.d(TAG, "Response: " + mess);

                            }
                        } else {
                            showToastAtCentre("Response: " + response.raw().message(), LENGTH_LONG);
                        }
                    }
                });




            }

            @Override
            public void onFailure(Call<ApiRecipeDetailModel> call, Throwable t) {
                UtilsClass.hideBusyAnimation(baseActivity);
                Log.d(TAG, "Response: " + "Something went wrong!!!!");
                showToastAtCentre("Response: " + t.getMessage(), LENGTH_LONG);
            }
        });

    }

    private void getRecipesDetail() {
        UtilsClass.showBusyAnimation(baseActivity, "Loading..");

        Call<RecipeDetailsModel> call = apiService.getRecipesDetail(SharedPreferenceUtility.getUserId(getApplicationContext()), recipeID);

        call.enqueue(new Callback<RecipeDetailsModel>() {
            @Override
            public void onResponse(Call<RecipeDetailsModel> call, final Response<RecipeDetailsModel> response) {
                UtilsClass.hideBusyAnimation(baseActivity);

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (response.body() != null) {
                            if (response.body().getStatus()) {

                                recipeData = response.body().getRecipe();

                                recipeTitle.setText(recipeData.getRecipeName());
                                Picasso.with(baseActivity).load(recipeData.getRecipeImage())
                                        .placeholder(R.drawable.food_ph)
                                        .error(R.drawable.food_ph)
                                        .into(recipeImage);
                                String timeToCook = (recipeData.getTimeToCook() == null ? "0 mins": recipeData.getTimeToCook()) +" " +recipeData.getUnitOfTime();
                                recipePrepTime.setText(timeToCook);

                                recipeNutritionData = response.body().getIngrednutr();
                                recipeCalories.setText(recipeData.getCalorieskcal());
                                recipeProtien.setText(recipeData.getProtein()+"g");
                                recipeFat.setText(recipeData.getFat()+"g");
                                recipeCarbo.setText(recipeData.getCarbs()+"g");
                                recipeSugar.setText(recipeData.getSugar()+"g");
                                String price = getString(R.string.rupee_symbol) +" "+recipeNutritionData.getTotprice();
                                recipePrice.setText(price);

                                if (recipeData.getCartId() != null)
                                {
                                    addIngredientsToCartBtn.setVisibility(View.GONE);
                                    addIngredientsToCartBtn1.setVisibility(View.GONE);
                                    addMoreToCartLayout.setVisibility(View.VISIBLE);
                                    addMoreToCartLayout1.setVisibility(View.VISIBLE);
                                    String q = recipeData.getCartqty();
                                    int quantity = Integer.parseInt(q);

                                    String quant = quantity > 1 ? quantity + " servings" : quantity + " serving";
                                    recipeCartQuantity.setText(quant);
                                    recipeCartQuantity1.setText(quant);

                                }
                                else {
                                    addIngredientsToCartBtn.setVisibility(View.VISIBLE);
                                    addIngredientsToCartBtn1.setVisibility(View.VISIBLE);
                                    addMoreToCartLayout.setVisibility(View.GONE);
                                    addMoreToCartLayout1.setVisibility(View.GONE);
                                }

                                String utensils = recipeData.getUtensilsNeeded();
                                String inst = recipeData.getCookDescription();
                                utensilsData.setText(utensils != null? utensils.isEmpty()? "No data found": utensils : "No data found");
                                instructions.setText(inst != null? inst.isEmpty()? "No data found": inst : "No data found");

                                if (recipeData.getFavID() == null)
                                {
                                    recipeFavourited.setBackground(getResources().getDrawable(R.drawable.fav_no_det));
                                }
                                else
                                {
                                    recipeFavourited.setBackground(getResources().getDrawable(R.drawable.fav_yes_det));
                                }

                                recipeDetails = recipeNutritionData.getList();


                                if(recipeDetails.size() >= 1 && recipeDetails.get(0) != null)
                                {
                                    adapter = new RecipeDetailsAdapter(recipeDetails, baseActivity, recipeData.getRecipeID());
                                    if (recipeData.getCartId() != null)
                                    {
                                        adapter.setItemsAddedToCart(true);
                                    }
                                    else {
                                        adapter.setItemsAddedToCart(false);
                                    }
                                    ingredientsList.setFocusable(false);
                                    ingredientsList.setAdapter(adapter);
                                    adapter.notifyDataSetChanged();
                                    UtilsClass.setListViewHeightBasedOnItems(ingredientsList, nestedScroll);
                                }
                                else
                                {
                                    dataNotFoundView.setVisibility(View.VISIBLE);
                                    ingredientsList.setVisibility(View.INVISIBLE);
                                }
                            }
                            else // Handle failure cases here
                            {
                                String mess = response.body().getMessage();
                                Log.d(TAG, "Response: " + mess);

                            }
                        } else {
                            showToastAtCentre("Response: " + response.raw().message(), LENGTH_LONG);
                        }
                    }
                });




            }

            @Override
            public void onFailure(Call<RecipeDetailsModel> call, Throwable t) {
                UtilsClass.hideBusyAnimation(baseActivity);
                Log.d(TAG, "Response: " + "Something went wrong!!!!");
                showToastAtCentre("Response: " + t.getMessage(), LENGTH_LONG);
            }
        });

    }

    private void showToastAtCentre(String message, int duration) {
        if (mToast != null) {
            mToast = null;
        }
        mToast = Toast.makeText(getApplicationContext(), message, duration);
        mToast.setGravity(Gravity.CENTER, 0, 0);
        mToast.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
