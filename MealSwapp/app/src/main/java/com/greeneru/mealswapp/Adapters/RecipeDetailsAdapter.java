package com.greeneru.mealswapp.Adapters;


import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.greeneru.mealswapp.HomeActivity;
import com.greeneru.mealswapp.Models.DataModel;
import com.greeneru.mealswapp.Models.RecipeDetailsModel;
import com.greeneru.mealswapp.Models.StatusModel;
import com.greeneru.mealswapp.R;
import com.greeneru.mealswapp.RecipeDetailedActivity;
import com.greeneru.mealswapp.Retrofit.ApiClient;
import com.greeneru.mealswapp.Retrofit.ApiInterface;
import com.greeneru.mealswapp.Utilities.SharedPreferenceUtility;
import com.greeneru.mealswapp.UtilsClass;


import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RecipeDetailsAdapter extends ArrayAdapter<RecipeDetailsModel.IngredientsDetail> implements View.OnClickListener{

    private List<RecipeDetailsModel.IngredientsDetail> dataSet;
    Context mContext;
    Activity baseActivity;
    private boolean isItemsAddedToCart;
    private String recipeId;
    private ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

    public boolean getItemsAddedToCart() {
        return isItemsAddedToCart;
    }

    public void setItemsAddedToCart(boolean itemsAddedToCart) {
        isItemsAddedToCart = itemsAddedToCart;
    }

    // View lookup cache
    private static class ViewHolder {
        TextView ingredientName;
        TextView ingredientPrice;
        TextView ingredientQuantity;
        TextView ingredientRequiredQuantity;
        TextView ingredientStoreWeight;
        Button incrementView;
        Button decrementView;
        LinearLayout quantityLayout;
        LinearLayout outOfStockLayout;

    }

    public RecipeDetailsAdapter(List<RecipeDetailsModel.IngredientsDetail> data, Context context, String recipeID) {
        super(context, R.layout.ingredient_item_new, data);
        this.dataSet = data;
        this.mContext = context;
        this.baseActivity = (RecipeDetailedActivity)context;
        this.recipeId = recipeID;
    }



    @Override
    public void onClick(View v) {

        int position=(Integer) v.getTag();
        Object object= getItem(position);
        DataModel dataModel=(DataModel)object;

        switch (v.getId())
        {
            case R.id.incrementView:
                Snackbar.make(v, "Quantity incremented", Snackbar.LENGTH_LONG)
                        .setAction("No action", null).show();
                break;

                case R.id.decrementView:
                Snackbar.make(v, "Quantity decremented", Snackbar.LENGTH_LONG)
                        .setAction("No action", null).show();
                break;
        }
    }

    private int lastPosition = -1;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        final RecipeDetailsModel.IngredientsDetail dataModel = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        final ViewHolder viewHolder; // view lookup cache stored in tag

        final View result;

        if (convertView == null) {

            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.ingredient_item_new, parent, false);
            final Typeface boldFont = Typeface.createFromAsset(mContext.getAssets(), "fonts/gotham_rounded_bold.otf");
            final Typeface bookFont = Typeface.createFromAsset(mContext.getAssets(), "fonts/gotham_rounded_book.otf");

            viewHolder.ingredientName = convertView.findViewById(R.id.ingredientName);
            viewHolder.ingredientPrice =  convertView.findViewById(R.id.ingredientPriceView);
            viewHolder.ingredientQuantity = convertView.findViewById(R.id.quantityView);
            viewHolder.ingredientRequiredQuantity =  convertView.findViewById(R.id.ingredientRequiredView);
            viewHolder.incrementView  =convertView.findViewById(R.id.incrementView);
            viewHolder.decrementView  =convertView.findViewById(R.id.decrementView);
            viewHolder.ingredientStoreWeight  =convertView.findViewById(R.id.ingredientAvailableQuantity);
            viewHolder.quantityLayout  =convertView.findViewById(R.id.quantityLayout);
            viewHolder.outOfStockLayout  =convertView.findViewById(R.id.outOfStockLayout);

            viewHolder.ingredientName.setTypeface(boldFont);
            viewHolder.ingredientPrice.setTypeface(bookFont);
            viewHolder.ingredientQuantity.setTypeface(bookFont);
            viewHolder.ingredientRequiredQuantity.setTypeface(bookFont);
            viewHolder.incrementView.setTypeface(bookFont);
            viewHolder.decrementView.setTypeface(bookFont);
            viewHolder.ingredientStoreWeight.setTypeface(bookFont);


            result=convertView;

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            result=convertView;
        }

        lastPosition = position;

        viewHolder.ingredientName.setText(dataModel.getItemName());
        viewHolder.ingredientPrice.setText("₹ " +dataModel.getSalePrice());
        viewHolder.ingredientRequiredQuantity.setText(dataModel.getRecipes_quantity());
        String weight = "Quantity: " + dataModel.getWeight() + " " + dataModel.getUnitName();
        viewHolder.ingredientStoreWeight.setText(weight);
        if (dataModel.getIngredient_Id() != null)
        {
            String q = dataModel.getIngredient_qty();
            int count = Integer.parseInt(q);
            /*if (count == 0 && !dataModel.isUserRemoved()) // Ask raja sekar to add this column in table
            {
                viewHolder.ingredientQuantity.setText("1");
                dataModel.setIngredient_qty("1");
            }
            else {*/
                viewHolder.ingredientQuantity.setText(q);
                dataModel.setIngredient_qty(q);
            /*}*/

        }
        else
        {
            dataModel.setIngredient_qty("1");
            viewHolder.ingredientQuantity.setText("1");
        }


        if(dataModel.getSalePrice().equals("0"))
        {
            viewHolder.outOfStockLayout.setVisibility(View.VISIBLE);
            viewHolder.quantityLayout.setVisibility(View.GONE);
        }
        else
        {
            viewHolder.outOfStockLayout.setVisibility(View.GONE);
            viewHolder.quantityLayout.setVisibility(View.VISIBLE);
        }


        viewHolder.incrementView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final int quantity =  Integer.parseInt(dataModel.getIngredient_qty().trim()) + 1;
                if (getItemsAddedToCart())
                {
                    //updateIngredientQty
                    UtilsClass.showBusyAnimation(baseActivity,"");
                    Call<StatusModel> call = apiService.updateIngredientQty(SharedPreferenceUtility.getUserId(mContext), recipeId, dataModel.getSalePrice(),String.valueOf(quantity), dataModel.getId(),dataModel.getIngredient_Id(), "1");

                    call.enqueue(new Callback<StatusModel>() {
                        @Override
                        public void onResponse(Call<StatusModel> call, final Response<StatusModel> response) {
                            UtilsClass.hideBusyAnimation(baseActivity);
                            if (response.body() != null) {
                                if (response.body().getStatus()) {

                                    dataModel.setIngredient_qty((String.valueOf(quantity)));
                                    viewHolder.ingredientQuantity.setText(String.valueOf(quantity));

                                } else {
                                    // retain the button background
                                    Log.d(RecipeAdapter.class.getSimpleName(), "Response: " + response.body().getMessage());
                                    Toast.makeText(mContext,"" + response.body().getMessage(),Toast.LENGTH_LONG).show();
                                }

                            } else {
                                // retain the button background
                                Log.d(RecipeAdapter.class.getSimpleName(), "Response: " + response.raw().message());
                                Toast.makeText(mContext,"" + response.raw().message(),Toast.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<StatusModel> call, Throwable t) {
                            UtilsClass.hideBusyAnimation(baseActivity);
                            // retain the button background
                            Log.d(RecipeAdapter.class.getSimpleName(), "Response: " + "Something went wrong!!!!");

                        }
                    });
                }
                else
                {
                    dataModel.setIngredient_qty((String.valueOf(quantity)));
                    viewHolder.ingredientQuantity.setText(String.valueOf(quantity));
                }

            }
        });
        viewHolder.decrementView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final int quantity =  Integer.parseInt(dataModel.getIngredient_qty().trim());
                final int quant = quantity - 1;
                if (quantity != 0)
                {
                    if (getItemsAddedToCart())
                    {
                        UtilsClass.showBusyAnimation(baseActivity,"");
                        Call<StatusModel> call = apiService.updateIngredientQty(SharedPreferenceUtility.getUserId(mContext), recipeId, dataModel.getSalePrice(),String.valueOf(quant), dataModel.getId(), dataModel.getIngredient_Id(), "0");

                        call.enqueue(new Callback<StatusModel>() {
                            @Override
                            public void onResponse(Call<StatusModel> call, final Response<StatusModel> response) {
                                UtilsClass.hideBusyAnimation(baseActivity);
                                if (response.body() != null) {
                                    if (response.body().getStatus()) {

                                        dataModel.setIngredient_qty((String.valueOf(quant)));
                                        viewHolder.ingredientQuantity.setText(String.valueOf(quant));

                                    } else {
                                        // retain the button background
                                        Toast.makeText(mContext,"" + response.body().getMessage(),Toast.LENGTH_LONG).show();
                                        Log.d(RecipeAdapter.class.getSimpleName(), "Response: " + response.body().getMessage());
                                    }

                                } else {
                                    // retain the button background
                                    Toast.makeText(mContext,"" + response.raw().message(),Toast.LENGTH_LONG).show();
                                    Log.d(RecipeAdapter.class.getSimpleName(), "Response: " + response.raw().message());
                                }
                            }

                            @Override
                            public void onFailure(Call<StatusModel> call, Throwable t) {
                                UtilsClass.hideBusyAnimation(baseActivity);
                                // retain the button background
                                Log.d(RecipeAdapter.class.getSimpleName(), "Response: " + "Something went wrong!!!!");

                            }
                        });
                    }
                    else {
                        dataModel.setIngredient_qty((String.valueOf(quant)));
                        viewHolder.ingredientQuantity.setText(String.valueOf(quant));
                    }
                }
                else {
                    dataModel.setIngredient_qty((String.valueOf(quantity)));
                    viewHolder.ingredientQuantity.setText(String.valueOf(quantity));
                }
            }
        });

        // Return the completed view to render on screen
        return convertView;
    }

    public int getCount() {
        return dataSet.size();
    }

    public RecipeDetailsModel.IngredientsDetail getItem(int position) {
        return dataSet.get(position);
    }

    public long getItemId(int position) {
        return position;
    }
}

