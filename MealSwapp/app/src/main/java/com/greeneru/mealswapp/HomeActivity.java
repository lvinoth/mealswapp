package com.greeneru.mealswapp;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class HomeActivity extends AppCompatActivity {


    private static final int PROFILE_REQUEST_CODE = 102;
    private Toolbar toolbar;
    private TabLayout tabLayout;
    private CustomPageView viewPager;
    private ActionBar actionBar;
    public TextView titleView;
    public ImageView profileView;
    private int[] resIs = {
            R.drawable.recipe_unselect,
            R.drawable.product_unselect,
            R.drawable.cart_unselect};
    private ViewPagerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        UtilsClass.makeStatusBarTransparent(getWindow(), this);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        titleView = findViewById(R.id.toolbarTitleView);
        profileView = findViewById(R.id.profileView);
        actionBar = getSupportActionBar();
        actionBar.setDisplayShowTitleEnabled(true);

        final ImageView profileView = findViewById(R.id.profileView);
        profileView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivityForResult(new Intent(getApplicationContext(), ProfileActivity.class), PROFILE_REQUEST_CODE);
//                startActivity(new Intent(getApplicationContext(), ProfileActivity.class));
            }
        });


        viewPager = findViewById(R.id.viewpager);
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(2);

        tabLayout = findViewById(R.id.mealTab);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setOnTabSelectedListener (new TabLayout.ViewPagerOnTabSelectedListener(viewPager) {

            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                super.onTabSelected(tab);
                setCurrentTabFragment(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                super.onTabUnselected(tab);

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                super.onTabReselected(tab);
            }
        });
        viewPager.setPagingEnabled(false);
        setupTabIcons();
        tabLayout.getTabAt(0).setIcon(R.drawable.recipe_select);
        tabLayout.setTabTextColors(Color.parseColor("#cccccc"), Color.parseColor("#000000"));
    }

    private void setupTabIcons() {
        tabLayout.getTabAt(0).setIcon(resIs[0]);
        tabLayout.getTabAt(1).setIcon(resIs[1]);
        tabLayout.getTabAt(2).setIcon(resIs[2]);
    }

    public void selectTab(int index)
    {
        TabLayout.Tab tab = tabLayout.getTabAt(index);
        tab.select();
    }

    public void setTitleParams(int color)
    {
//        titleView.setTextColor(color);
        titleView.setTextColor(getResources().getColor(color));
        DrawableCompat.setTint(profileView.getDrawable(), ContextCompat.getColor(this, color));
    }


    private boolean isfirstTimeProd = true;
    private boolean isfirstTimeCart = true;

    public void setCurrentTabFragment(int tabPosition)
    {
        switch (tabPosition)
        {
            case 0 :
                titleView.setText("EXPLORE HEALTHY RECIPES");
                tabLayout.getTabAt(0).setIcon(R.drawable.recipe_select);
                tabLayout.getTabAt(1).setIcon(R.drawable.product_unselect);
                tabLayout.getTabAt(2).setIcon(R.drawable.cart_unselect);
                ((RecipeFragment) adapter.getItem(tabPosition)).setTitleParams();
                break;
            case 1 :
                titleView.setText("VIEW HEALTHY PRODUCTS");
                if (isfirstTimeProd)
                {
                    isfirstTimeProd = false;
                    setTitleParams(R.color.app_blue_color);
                    ProductFragment frag = (ProductFragment) adapter.getItem(tabPosition);
                    frag.callOnFirstTime();
                }
                if (UtilsClass.isIsChangesMadeInProducts())
                {
                    UtilsClass.setIsChangesMadeInProducts(false);
                    ProductFragment frag = (ProductFragment) adapter.getItem(tabPosition);
                    frag.refreshProducts();
                }
                tabLayout.getTabAt(0).setIcon(R.drawable.recipe_unselect);
                tabLayout.getTabAt(1).setIcon(R.drawable.product_select);
                tabLayout.getTabAt(2).setIcon(R.drawable.cart_unselect);
                ((ProductFragment) adapter.getItem(tabPosition)).setTitleParams();
                break;
            case 2 :
                titleView.setText("SHOPPING CART");
                setTitleParams(R.color.app_blue_color);
                CartFragment frag = (CartFragment) adapter.getItem(tabPosition);
                frag.getItemsInCart();
                tabLayout.getTabAt(0).setIcon(R.drawable.recipe_unselect);
                tabLayout.getTabAt(1).setIcon(R.drawable.product_unselect);
                tabLayout.getTabAt(2).setIcon(R.drawable.cart_select);
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PROFILE_REQUEST_CODE && resultCode == Activity.RESULT_OK)
        {
            finish();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.e(HomeActivity.class.getSimpleName(),"Home Activity Destroyed");
    }
}
