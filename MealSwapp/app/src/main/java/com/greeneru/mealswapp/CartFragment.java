package com.greeneru.mealswapp;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.GridLayoutManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.greeneru.mealswapp.Adapters.CartExpandableListAdapter;
import com.greeneru.mealswapp.Adapters.ProductInCartAdapter;
import com.greeneru.mealswapp.Adapters.RecipeAdapter;
import com.greeneru.mealswapp.Models.CartModel;
import com.greeneru.mealswapp.Models.RecipeModel;
import com.greeneru.mealswapp.Models.StatusModel;
import com.greeneru.mealswapp.Retrofit.ApiClient;
import com.greeneru.mealswapp.Retrofit.ApiInterface;
import com.greeneru.mealswapp.Utilities.SharedPreferenceUtility;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.widget.Toast.LENGTH_SHORT;


/**
 * A simple {@link Fragment} subclass.
 */
public class CartFragment extends Fragment implements CartExpandableListAdapter.quantityChangedInterface {
    private static final String TAG = CartFragment.class.getSimpleName();
    ExpandableListView expandableListView;
    ListView productListView;
    ExpandableListAdapter expandableListAdapter;
    private ApiInterface apiService;
    private List<CartModel.RecipesInCart> recipesInCart;
    private List<CartModel.ProductsInCart> productsInCart;
    private CartModel.PaymentDetails CartPaymentDetail;
    private ProductInCartAdapter productInCartAdapter;
    private NestedScrollView cartLayout;
    private TextView noItemsInCart;
    private Adapter initializedAdapter;
    private List<CartModel.RecipesInCart> recipesCart;
    private Toast mToast;
    private Spinner spinner;
    private Button orderDelivery;
    private TextView deliveryCharges;
    private TextView gstPrice;
    private TextView basePrice;
    private TextView discountPrice;
    private TextView totalPrice;
    private TextView grandPrice;
    private CartExpandableListAdapter.quantityChangedInterface listener;

    public CartFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_cart, container, false);
        listener = this;
        apiService = ApiClient.getClient().create(ApiInterface.class);
        expandableListView = (ExpandableListView) view.findViewById(R.id.expandableListView);
        productListView = view.findViewById(R.id.productsListView);
        cartLayout = view.findViewById(R.id.cartLayout);
        noItemsInCart = view.findViewById(R.id.noItemsFound);
        spinner = view.findViewById(R.id.sortItems);
        orderDelivery = view.findViewById(R.id.orderDelivery);
        deliveryCharges = view.findViewById(R.id.deliveryCharges);
        gstPrice = view.findViewById(R.id.gstPrice);
        basePrice = view.findViewById(R.id.basePrice);
        discountPrice = view.findViewById(R.id.discountPrice);
        totalPrice = view.findViewById(R.id.totalPrice);
        grandPrice = view.findViewById(R.id.grandPrice);

        orderDelivery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pushOrder();
            }
        });

        expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            @Override
            public void onGroupExpand(int groupPosition) {
                /*Toast.makeText(getActivity(),
                        expandableListTitle.get(groupPosition) + " List Expanded.",
                        Toast.LENGTH_SHORT).show();*/
            }
        });

        expandableListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

            @Override
            public void onGroupCollapse(int groupPosition) {
                /*Toast.makeText(getActivity()," List Collapsed.",
                        Toast.LENGTH_SHORT).show();*/

            }
        });

        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
               /* Toast.makeText(
                        getActivity(),
                        expandableListTitle.get(groupPosition)
                                + " -> "
                                + expandableListDetail.get(
                                expandableListTitle.get(groupPosition)).get(
                                childPosition), Toast.LENGTH_SHORT
                ).show();*/
                return false;
            }
        });
        setUpSort();

        return view;
    }

    private void setUpSort()
    {
        final ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(getActivity(), R.layout.spinner_item1, getResources().getStringArray(R.array.sort_types_cart));
        spinner.setAdapter(dataAdapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // This is to avoid the initial fire up while loading this screen. Do not remove
                if( initializedAdapter !=parent.getAdapter() ) {
                    initializedAdapter = parent.getAdapter();
                    return;
                }
                String data = dataAdapter.getItem(position);
                Log.d(TAG, "Sorting Chosen: " + data);
                //showToastAtCentre(data + " used for sorting! ",LENGTH_SHORT);

                if (recipesCart != null && recipesCart.size() > 0)
                {

                    switch (position)
                    {
                        case 0: // popularity
                            break;
                        case 1: // price low to high
                            // Sorts calories
                            Collections.sort(recipesCart, new Comparator<CartModel.RecipesInCart>() {
                                @Override
                                public int compare(CartModel.RecipesInCart s1, CartModel.RecipesInCart s2) {
                                    return Double.valueOf(s1.getRecipeCalories()).compareTo(Double.valueOf(s2.getRecipeCalories()));
                                }
                            });
                            break;
                        case 2: // calories low to high
                            Collections.sort(recipesCart, new Comparator<CartModel.RecipesInCart>() {
                                @Override
                                public int compare(CartModel.RecipesInCart s1, CartModel.RecipesInCart s2) {
                                    return Double.valueOf(s1.getRecipePrice()).compareTo(Double.valueOf(s2.getRecipePrice()));
                                }
                            });
                            break;
                        case 3: // alphabetical
                            // Sorts alphabetically
                            Collections.sort(recipesCart, new Comparator<CartModel.RecipesInCart>() {
                                @Override
                                public int compare(CartModel.RecipesInCart s1, CartModel.RecipesInCart s2) {
                                    return s1.getRecipeName().compareToIgnoreCase(s2.getRecipeName());
                                }
                            });
                            break;
                        /*case 4: // alphabetical high to low
                            // Sorts alphabetically
                            Collections.sort(recipeList, new Comparator<RecipeModel.Recipes>() {
                                @Override
                                public int compare(RecipeModel.Recipes s1, RecipeModel.Recipes s2) {
                                    return s2.getRecipeName().compareToIgnoreCase(s1.getRecipeName());
                                }
                            });
                            break;*/
                    }

                    expandableListAdapter = new CartExpandableListAdapter(getActivity(), recipesCart, listener);
                    expandableListView.setAdapter(expandableListAdapter);
                }
                else
                {
                    showToastAtCentre("Your cart is empty..",LENGTH_SHORT);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    private void showToastAtCentre(String message, int duration) {
        if (mToast != null) {
            mToast.cancel();
            mToast = null;
        }
        mToast = Toast.makeText(getActivity().getApplicationContext(), message, duration);
        mToast.setGravity(Gravity.CENTER, 0, 0);
        mToast.show();
    }

    public void pushOrder()
    {
        {
            UtilsClass.showBusyAnimation(getActivity(), "Loading..");
            Call<StatusModel> call = apiService.placeOrder(SharedPreferenceUtility.getUserId(getActivity()));

            call.enqueue(new Callback<StatusModel>() {
                @Override
                public void onResponse(Call<StatusModel> call, final Response<StatusModel> response) {
                    UtilsClass.hideBusyAnimation(getActivity());
                    if (response.body() != null) {
                        if (response.body().getStatus())
                        {
                            Intent intt = new Intent(getActivity(), OrderPlacedActivity.class);
                            startActivityForResult(intt, 101);
                        }
                        else {
                            showToastAtCentre("Oops!, Something went wrong.", LENGTH_SHORT);
                            Log.d(RecipeAdapter.class.getSimpleName(), "Response: " + response.body().getMessage());
                        }

                    } else {
                        showToastAtCentre("Oops!, Something went wrong.", LENGTH_SHORT);
                        Log.d(RecipeAdapter.class.getSimpleName(), "Response: " + response.raw().message());
                    }
                }

                @Override
                public void onFailure(Call<StatusModel> call, Throwable t) {
                    UtilsClass.hideBusyAnimation(getActivity());
                    // retain the button background
                    Log.d(RecipeAdapter.class.getSimpleName(), "Response: " + "Something went wrong!!!!");

                }
            });
        }
    }


    public void getItemsInCart()
    {
        UtilsClass.showBusyAnimation(getActivity(), "Loading..");
        Call<CartModel> call = apiService.getItemsInCart(SharedPreferenceUtility.getUserId(getActivity()));

        call.enqueue(new Callback<CartModel>() {
            @Override
            public void onResponse(Call<CartModel> call, final Response<CartModel> response) {
                UtilsClass.hideBusyAnimation(getActivity());
                if (response.body() != null) {
                    if (response.body().getStatus())
                    {
                        recipesInCart = response.body().getCartRecipesDetail();
                        productsInCart = response.body().getCartProductsDetail();
                        CartPaymentDetail = response.body().getCartPaymentDetail();
                        recipesCart = new ArrayList<>();

                        if (recipesInCart.size() == 0 && productsInCart.size() == 0)
                        {
                            cartLayout.setVisibility(View.GONE);
                            noItemsInCart.setVisibility(View.VISIBLE);
                            return;
                        }
                        else {
                            cartLayout.setVisibility(View.VISIBLE);
                            noItemsInCart.setVisibility(View.GONE);
                        }

                        CartModel.RecipesInCart dd = new CartModel.RecipesInCart();
                        dd.setIngredients(productsInCart);

                        recipesCart.add(0, dd);

                        for (int i = 0; i < recipesInCart.size();i++)
                        {
                            if (recipesInCart.get(i).getIngredients().size() > 0)
                            {
                                recipesCart.add(recipesInCart.get(i));
                            }
                        }
                        expandableListAdapter = new CartExpandableListAdapter(getActivity(), recipesCart, listener);
                        expandableListView.setAdapter(expandableListAdapter);

                        double charge = CartPaymentDetail.getExtraCharge() + CartPaymentDetail.getTaxAmount();
                        String tax = String.format("%.2f", charge);
                        deliveryCharges.setText("     ₹ " + CartPaymentDetail.getDeliveryCharge());
                        gstPrice.setText("     ₹ " + String.valueOf(tax));
                        basePrice.setText("     ₹ " + CartPaymentDetail.getSubTotalPrice());
                        discountPrice.setText("(-) ₹ " + CartPaymentDetail.getTotalDiscountPrice());
                        totalPrice.setText("     ₹ " + CartPaymentDetail.getTotalPrice());
                        grandPrice.setText("     ₹ " + String.valueOf(CartPaymentDetail.getGrandTotalPrice()));

                        for(int i=0; i < expandableListAdapter.getGroupCount(); i++) {
                            expandableListView.expandGroup(i);
                        }

                       /* productInCartAdapter = new ProductInCartAdapter(productsInCart, getContext());
                        productListView.setAdapter(productInCartAdapter);*/

                    }
                    else {
                        cartLayout.setVisibility(View.GONE);
                        noItemsInCart.setVisibility(View.VISIBLE);

                        Log.d(RecipeAdapter.class.getSimpleName(), "Response: " + response.body().getMessage());
                    }

                } else {
                    cartLayout.setVisibility(View.GONE);
                    noItemsInCart.setVisibility(View.VISIBLE);

                    Log.d(RecipeAdapter.class.getSimpleName(), "Response: " + response.raw().message());
                }
            }

            @Override
            public void onFailure(Call<CartModel> call, Throwable t) {
                UtilsClass.hideBusyAnimation(getActivity());
                // retain the button background
                Log.d(RecipeAdapter.class.getSimpleName(), "Response: " + "Something went wrong!!!!");

            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && requestCode == 101)
        {
            UtilsClass.setIsChangesMadeInRecipes(true);
            UtilsClass.setIsChangesMadeInProducts(true);
            ((HomeActivity)getActivity()).selectTab(0);

        }
    }

    @Override
    public void onQuantityChanged() {
        //getItemsInCart();
    }
}
