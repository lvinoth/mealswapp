package com.greeneru.mealswapp.Models;

import java.util.List;

public class CuisineModel {

    private boolean status;

    private String message;

    private String userId;

    private List<Cuisines> cuisines;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean stat) {
        this.status = stat;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public List<Cuisines> getCuisines() {
        return cuisines;
    }

    public void setCuisines(List<Cuisines> cuisines) {
        this.cuisines = cuisines;
    }

    public class Cuisines {
        private String CuisineID;
        private String CuisineName;
        private String Status;
        private String AddedBy;
        private boolean isSelected;

        public String getCuisineID() {
            return CuisineID;
        }

        public void setCuisineID(String cuisineID) {
            CuisineID = cuisineID;
        }

        public String getCuisineName() {
            return CuisineName;
        }

        public void setCuisineName(String cuisineName) {
            CuisineName = cuisineName;
        }

        public String getStatus() {
            return Status;
        }

        public String getAddedBy() {
            return AddedBy;
        }

        public void setAddedBy(String addedBy) {
            AddedBy = addedBy;
        }

        public boolean isSelected() {
            return isSelected;
        }

        public void setSelected(boolean selected) {
            isSelected = selected;
        }
    }
}
