package com.greeneru.mealswapp.Models;

import java.util.List;

public class RecipeDetailsModel {

    private boolean status;
    private String message;
    private IngredientNutrition ingrednutr;
    private Recipe recipe;


    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public IngredientNutrition getIngrednutr() {
        return ingrednutr;
    }

    public void setIngrednutr(IngredientNutrition ingrednutr) {
        this.ingrednutr = ingrednutr;
    }

    public Recipe getRecipe() {
        return recipe;
    }

    public void setRecipe(Recipe recipe) {
        this.recipe = recipe;
    }

    public class IngredientNutrition
    {
        private List<IngredientsDetail> list;
        private String totprice;


        public List<IngredientsDetail> getList() {
            return list;
        }

        public void setList(List<IngredientsDetail> list) {
            this.list = list;
        }

        public String getTotprice() {
            return totprice;
        }

        public void setTotprice(String totprice) {
            this.totprice = totprice;
        }
    }


    public class Recipe
    {

        private String RecipeID;
        private String RecipeName;
        private String CookDescription;
        private String UtensilsNeeded;
        private String DefaultServingsSize;
        private String TimeToCook;
        private String UnitOfTime;
        private String Calorieskcal;
        private String Fat;
        private String Protein;
        private String Carbs;
        private String Sugar;
        private String RecipeImage;
        private String FavID;
        private String cartId;
        private String cartqty;


        public String getRecipeID() {
            return RecipeID;
        }

        public void setRecipeID(String recipeID) {
            RecipeID = recipeID;
        }

        public String getRecipeName() {
            return RecipeName;
        }

        public void setRecipeName(String recipeName) {
            RecipeName = recipeName;
        }

        public String getCookDescription() {
            return CookDescription;
        }

        public void setCookDescription(String cookDescription) {
            CookDescription = cookDescription;
        }

        public String getUtensilsNeeded() {
            return UtensilsNeeded;
        }

        public void setUtensilsNeeded(String utensilsNeeded) {
            UtensilsNeeded = utensilsNeeded;
        }

        public String getDefaultServingsSize() {
            return DefaultServingsSize;
        }

        public void setDefaultServingsSize(String defaultServingsSize) {
            DefaultServingsSize = defaultServingsSize;
        }

        public String getTimeToCook() {
            return TimeToCook;
        }

        public void setTimeToCook(String timeToCook) {
            TimeToCook = timeToCook;
        }

        public String getUnitOfTime() {
            return UnitOfTime;
        }

        public void setUnitOfTime(String unitOfTime) {
            UnitOfTime = unitOfTime;
        }

        public String getRecipeImage() {
            return RecipeImage;
        }

        public void setRecipeImage(String recipeImage) {
            RecipeImage = recipeImage;
        }

        public String getCalorieskcal() {
            return Calorieskcal;
        }

        public void setCalorieskcal(String calorieskcal) {
            Calorieskcal = calorieskcal;
        }

        public String getFat() {
            return Fat;
        }

        public void setFat(String fat) {
            Fat = fat;
        }

        public String getProtein() {
            return Protein;
        }

        public void setProtein(String protein) {
            Protein = protein;
        }

        public String getCarbs() {
            return Carbs;
        }

        public void setCarbs(String carbs) {
            Carbs = carbs;
        }

        public String getSugar() {
            return Sugar;
        }

        public void setSugar(String sugar) {
            Sugar = sugar;
        }

        public String getFavID() {
            return FavID;
        }

        public void setFavID(String favID) {
            FavID = favID;
        }

        public String getCartId() {
            return cartId;
        }

        public void setCartId(String cartId) {
            this.cartId = cartId;
        }

        public String getCartqty() {
            return cartqty;
        }

        public void setCartqty(String cartqty) {

            this.cartqty = cartqty;
            if (cartqty == null)
            {
                this.cartqty = "0";
            }
        }
    }


    public class IngredientsDetail
    {
        private String Id;
        private String ItemName;
        private String Weight;
        private String UnitName;
        private String CaloriesKcal;
        private String Protein;
        private String Sugar;
        private String Fat;
        private String SatFat;
        private String CarbohyDrates;
        private String DietaryFiber;
        private String Cholestrol;
        private String AddedSugars;
        private String Price;
        private String DiscountPrice;
        private String DiscountValidity;
        private String SalePrice;
        private String Nutrition;
        private String GproteinPerKcal;
        private String FiberPerKcal;
        private String Sodium;
        private String SourceOfNutrition;
        private String recipes_quantity;
        private String ingredient_Id;
        private String ingredient_qty;
        private String orderQuantity;
        private boolean userRemoved;

        public String getItemName() {
            return ItemName;
        }

        public void setItemName(String itemName) {
            ItemName = itemName;
        }

        public String getWeight() {
            return Weight;
        }

        public void setWeight(String weight) {
            Weight = weight;
        }

        public String getCaloriesKcal() {
            return CaloriesKcal;
        }

        public void setCaloriesKcal(String caloriesKcal) {
            CaloriesKcal = caloriesKcal;
        }

        public String getProtein() {
            return Protein;
        }

        public void setProtein(String protein) {
            Protein = protein;
        }

        public String getSugar() {
            return Sugar;
        }

        public void setSugar(String sugar) {
            Sugar = sugar;
        }

        public String getFat() {
            return Fat;
        }

        public void setFat(String fat) {
            Fat = fat;
        }

        public String getSatFat() {
            return SatFat;
        }

        public void setSatFat(String satFat) {
            SatFat = satFat;
        }

        public String getCarbohyDrates() {
            return CarbohyDrates;
        }

        public void setCarbohyDrates(String carbohyDrates) {
            CarbohyDrates = carbohyDrates;
        }

        public String getDietaryFiber() {
            return DietaryFiber;
        }

        public void setDietaryFiber(String dietaryFiber) {
            DietaryFiber = dietaryFiber;
        }

        public String getCholestrol() {
            return Cholestrol;
        }

        public void setCholestrol(String cholestrol) {
            Cholestrol = cholestrol;
        }

        public String getAddedSugars() {
            return AddedSugars;
        }

        public void setAddedSugars(String addedSugars) {
            AddedSugars = addedSugars;
        }

        public String getPrice() {
            return Price;
        }

        public void setPrice(String price) {
            Price = price;
        }

        public String getDiscountPrice() {
            return DiscountPrice;
        }

        public void setDiscountPrice(String discountPrice) {
            DiscountPrice = discountPrice;
        }

        public String getDiscountValidity() {
            return DiscountValidity;
        }

        public void setDiscountValidity(String discountValidity) {
            DiscountValidity = discountValidity;
        }

        public String getSalePrice() {
            return SalePrice;
        }

        public void setSalePrice(String salePrice) {
            SalePrice = salePrice;
        }

        public String getNutrition() {
            return Nutrition;
        }

        public void setNutrition(String nutrition) {
            Nutrition = nutrition;
        }

        public String getGproteinPerKcal() {
            return GproteinPerKcal;
        }

        public void setGproteinPerKcal(String gproteinPerKcal) {
            GproteinPerKcal = gproteinPerKcal;
        }

        public String getFiberPerKcal() {
            return FiberPerKcal;
        }

        public void setFiberPerKcal(String fiberPerKcal) {
            FiberPerKcal = fiberPerKcal;
        }

        public String getSodium() {
            return Sodium;
        }

        public void setSodium(String sodium) {
            Sodium = sodium;
        }

        public String getSourceOfNutrition() {
            return SourceOfNutrition;
        }

        public void setSourceOfNutrition(String sourceOfNutrition) {
            SourceOfNutrition = sourceOfNutrition;
        }

        public String getRecipes_quantity() {
            return recipes_quantity;
        }

        public void setRecipes_quantity(String recipes_quantity) {
            this.recipes_quantity = recipes_quantity;
        }

        public String getUnitName() {
            return UnitName;
        }

        public void setUnitName(String unitName) {
            this.UnitName = unitName;
        }

        public String getId() {
            return Id;
        }

        public void setId(String id) {
            Id = id;
        }

        public String getOrderQuantity() {
            return orderQuantity;
        }

        public void setOrderQuantity(String orderQuantity) {
            this.orderQuantity = orderQuantity;
        }

        public String getIngredient_Id() {
            return ingredient_Id;
        }

        public void setIngredient_Id(String ingredient_Id) {
            this.ingredient_Id = ingredient_Id;
        }

        public String getIngredient_qty() {
            return ingredient_qty;
        }

        public void setIngredient_qty(String ingredient_qty) {
            this.ingredient_qty = ingredient_qty;
        }

        public boolean isUserRemoved() {
            return userRemoved;
        }

        public void setUserRemoved(boolean userRemoved) {
            this.userRemoved = userRemoved;
        }
    }


}
