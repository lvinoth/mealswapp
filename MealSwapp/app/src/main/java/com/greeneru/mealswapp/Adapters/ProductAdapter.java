package com.greeneru.mealswapp.Adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.greeneru.mealswapp.HomeActivity;
import com.greeneru.mealswapp.Models.ProductModel;
import com.greeneru.mealswapp.Models.StatusModel;
import com.greeneru.mealswapp.R;
import com.greeneru.mealswapp.Retrofit.ApiClient;
import com.greeneru.mealswapp.Retrofit.ApiInterface;
import com.greeneru.mealswapp.Utilities.SharedPreferenceUtility;
import com.greeneru.mealswapp.UtilsClass;
import com.squareup.picasso.Picasso;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ViewHolder> {

    private List<ProductModel.Products> mData;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    private boolean isDiscountPage;
    private Context mContext;
    private Activity baseActivity;
    private String selectedTab;
    private int red_color;
    private int blue_color;
    private int green_color;
    private ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

    // data is passed into the constructor
    public ProductAdapter(Context context, List<ProductModel.Products> data, boolean discountPage, String selectedTab) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        this.isDiscountPage = discountPage;
        this.selectedTab = selectedTab;
        this.mContext = context;
        this.baseActivity = (HomeActivity)context;
        red_color = this.mContext.getResources().getColor(R.color.app_red_color);
        blue_color = this.mContext.getResources().getColor(R.color.app_blue_color);
        green_color = this.mContext.getResources().getColor(R.color.app_green_color);
    }

    // inflates the row layout from xml when needed
    @NonNull
    @Override
    public ProductAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.recipe_item, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final ProductModel.Products products = mData.get(position);
        holder.productName.setText(products.getItemName());

        String price = products.getSalePrice();
        price = price == null ? "₹ 0" : ("₹ " + price);
        holder.productPrice.setText(price);

        String discountPrice = products.getDiscountPrice();
        discountPrice = discountPrice == null ? "₹ 0" : ("₹ " + discountPrice);

        if (selectedTab.equals("2")) {
            holder.productDiscountPrice.setVisibility(View.VISIBLE);
            holder.productDiscountPrice.setText(discountPrice);
            holder.productPrice.setPaintFlags(holder.productPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        } else {
            holder.productPrice.setPaintFlags(holder.productPrice.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
            holder.productDiscountPrice.setVisibility(View.GONE);
        }

        final String recipeImagePath = products.getProductImage();
        if (recipeImagePath == null || recipeImagePath.isEmpty())
        {
            Picasso.with(mContext).load(R.drawable.food_ph)
                    .placeholder(R.drawable.food_ph)
                    .error(R.drawable.food_ph)
                    .into(holder.productImageView);
        }
        else {

            Picasso.with(mContext).load(recipeImagePath)
                    .placeholder(R.drawable.food_ph)
                    .error(R.drawable.food_ph)
                    .into(holder.productImageView);
        }


        String cal = products.getCaloriesKcal() == null ? "0 Cal" : String.valueOf(products.getCaloriesKcal()) + " Cal";
        holder.productCalorie.setText(cal);


        if (products.getCartId() != null)
        {
            holder.addCartLayout.setVisibility(View.GONE);
            holder.addMoreToCartLayout.setVisibility(View.VISIBLE);
            int quantity = products.getCartqty();
            String quant = quantity > 1 ? quantity + " servings" : quantity + " serving";
            holder.recipeCartQuantity.setText(quant);
        }
        else {
            holder.addCartLayout.setVisibility(View.VISIBLE);
            holder.addMoreToCartLayout.setVisibility(View.GONE);
        }


        holder.addIngredients.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String cost = products.getSalePrice();
                if (selectedTab.equals("2")) {
                    cost = products.getDiscountPrice();
                }
                UtilsClass.showBusyAnimation(baseActivity, "");
                Call<StatusModel> call = apiService.addItemsToCart(SharedPreferenceUtility.getUserId(mContext),products.getPTID(),cost,"1","1","2","","","1");

                call.enqueue(new Callback<StatusModel>() {
                    @Override
                    public void onResponse(Call<StatusModel> call, final Response<StatusModel> response) {
                        UtilsClass.hideBusyAnimation(baseActivity);
                        if (response.body() != null) {
                            if (response.body().getStatus())
                            {
                                // change the button background
                                holder.addCartLayout.setVisibility(View.GONE);
                                holder.addMoreToCartLayout.setVisibility(View.VISIBLE);

                                String quantity = "1 serving";
                                products.setCartqty(1);
                                holder.recipeCartQuantity.setText(quantity);
                            }
                            else {
                                // retain the button background
                                holder.addCartLayout.setVisibility(View.VISIBLE);
                                holder.addMoreToCartLayout.setVisibility(View.GONE);
                                Log.d(ProductAdapter.class.getSimpleName(), "Response: " + response.body().getMessage());
                            }

                        } else {
                            // retain the button background
                            holder.addCartLayout.setVisibility(View.VISIBLE);
                            holder.addMoreToCartLayout.setVisibility(View.GONE);
                            Log.d(ProductAdapter.class.getSimpleName(), "Response: " + response.raw().message());
                        }
                    }

                    @Override
                    public void onFailure(Call<StatusModel> call, Throwable t) {
                        UtilsClass.hideBusyAnimation(baseActivity);
                        // retain the button background
                        holder.addCartLayout.setVisibility(View.VISIBLE);
                        holder.addMoreToCartLayout.setVisibility(View.GONE);
                        Log.d(ProductAdapter.class.getSimpleName(), "Response: " + "Something went wrong!!!!");

                    }
                });
            }
        });



        holder.decreaseCartQuantity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String cost = products.getSalePrice();
                if (selectedTab.equals("2")) {
                    cost = products.getDiscountPrice();
                }
                final int quantity = products.getCartqty() - 1;
                if (quantity != 0) {

                    String cartId = products.getCartId() == null ? "" : products.getCartId();
                    UtilsClass.showBusyAnimation(baseActivity,"");
                    Call<StatusModel> call = apiService.addItemsToCart(SharedPreferenceUtility.getUserId(mContext), products.getPTID(), cost, String.valueOf(quantity), "1", "2", "", cartId, "0");

                    call.enqueue(new Callback<StatusModel>() {
                        @Override
                        public void onResponse(Call<StatusModel> call, final Response<StatusModel> response) {
                            UtilsClass.hideBusyAnimation(baseActivity);
                            if (response.body() != null) {
                                if (response.body().getStatus()) {

                                    products.setCartqty(quantity);

                                    if (quantity == 0) {
                                        holder.addCartLayout.setVisibility(View.VISIBLE);
                                        holder.addMoreToCartLayout.setVisibility(View.GONE);
                                    } else {
                                        String quant = quantity > 1 ? quantity + " servings" : quantity + " serving";
                                        holder.recipeCartQuantity.setText(quant);
                                    }

                                } else {
                                    // retain the button background
                                    Log.d(ProductAdapter.class.getSimpleName(), "Response: " + response.body().getMessage());
                                }

                            } else {
                                // retain the button background
                                Log.d(ProductAdapter.class.getSimpleName(), "Response: " + response.raw().message());
                            }
                        }

                        @Override
                        public void onFailure(Call<StatusModel> call, Throwable t) {
                            UtilsClass.hideBusyAnimation(baseActivity);
                            // retain the button background
                            Log.d(ProductAdapter.class.getSimpleName(), "Response: " + "Something went wrong!!!!");

                        }
                    });
                }
            }
        });

        holder.increaseCartQuantity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String cost = products.getSalePrice();
                if (selectedTab.equals("2")) {
                    cost = products.getDiscountPrice();
                }
                final int quantity = products.getCartqty() + 1;

                String cartId = products.getCartId() == null ? "" : products.getCartId();
                UtilsClass.showBusyAnimation(baseActivity,"");
                Call<StatusModel> call = apiService.addItemsToCart(SharedPreferenceUtility.getUserId(mContext), products.getPTID(), cost, String.valueOf(quantity), "1", "2", "", cartId,"1");

                call.enqueue(new Callback<StatusModel>() {
                    @Override
                    public void onResponse(Call<StatusModel> call, final Response<StatusModel> response) {
                        UtilsClass.hideBusyAnimation(baseActivity);
                        if (response.body() != null) {
                            if (response.body().getStatus()) {

                                products.setCartqty(quantity);
                                String quant = quantity > 1 ? quantity + " servings" : quantity + " serving";
                                holder.recipeCartQuantity.setText(quant);

                            } else {
                                // retain the button background
                                Log.d(ProductAdapter.class.getSimpleName(), "Response: " + response.body().getMessage());
                            }

                        } else {
                            // retain the button background
                            Log.d(ProductAdapter.class.getSimpleName(), "Response: " + response.raw().message());
                        }
                    }

                    @Override
                    public void onFailure(Call<StatusModel> call, Throwable t) {
                        UtilsClass.hideBusyAnimation(baseActivity);
                        // retain the button background
                        Log.d(ProductAdapter.class.getSimpleName(), "Response: " + "Something went wrong!!!!");

                    }
                });
            }
        });

        if (products.getIsFavorite())
        {
            holder.favouriteBtn.setBackground(mContext.getResources().getDrawable(R.drawable.ic_fav_sel));
        }
        else
        {
            holder.favouriteBtn.setBackground(mContext.getResources().getDrawable(R.drawable.ic_fav_unsel));
        }

        //        TODO: get make favourites API
        holder.favouriteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /*
                 * 0 - unfav
                 * 1 - fav
                 * */

                String isFav = products.getIsFavorite()?"0":"1";

                /*if (products.getIsFavorite()) {
                    holder.favouriteBtn.setBackground(mContext.getResources().getDrawable(R.drawable.ic_fav_unsel));
                } else {
                    holder.favouriteBtn.setBackground(mContext.getResources().getDrawable(R.drawable.ic_fav_sel));
                }*/

                UtilsClass.showBusyAnimation(baseActivity,"");
                Call<StatusModel> call = apiService.makeFavorite(SharedPreferenceUtility.getUserId(mContext),products.getPTID(),"1", isFav);

                call.enqueue(new Callback<StatusModel>() {
                    @Override
                    public void onResponse(Call<StatusModel> call, final Response<StatusModel> response) {
                        UtilsClass.hideBusyAnimation(baseActivity);
                        if (response.body() != null) {
                            if (response.body().getStatus())
                            {
                                if (!products.getIsFavorite()){
                                    products.setIsFavorite(true);
                                    holder.favouriteBtn.setBackground(mContext.getResources().getDrawable(R.drawable.ic_fav_sel));
                                }
                                else {
                                    products.setIsFavorite(false);
                                    holder.favouriteBtn.setBackground(mContext.getResources().getDrawable(R.drawable.ic_fav_unsel));
                                }
                            }
                            else {
                                if (!products.getIsFavorite()){
                                    products.setIsFavorite(false);
                                    holder.favouriteBtn.setBackground(mContext.getResources().getDrawable(R.drawable.ic_fav_unsel));
                                }
                                else {
                                    products.setIsFavorite(true);
                                    holder.favouriteBtn.setBackground(mContext.getResources().getDrawable(R.drawable.ic_fav_sel));
                                }

                                Log.d(ProductAdapter.class.getSimpleName(), "Response: " + response.body().getMessage());
                            }

                        } else {
                            if (!products.getIsFavorite()){
                                products.setIsFavorite(false);
                                holder.favouriteBtn.setBackground(mContext.getResources().getDrawable(R.drawable.ic_fav_unsel));
                            }
                            else {
                                products.setIsFavorite(true);
                                holder.favouriteBtn.setBackground(mContext.getResources().getDrawable(R.drawable.ic_fav_sel));
                            }
                            Log.d(ProductAdapter.class.getSimpleName(), "Response: " + response.raw().message());
                        }
                    }

                    @Override
                    public void onFailure(Call<StatusModel> call, Throwable t) {
                        UtilsClass.hideBusyAnimation(baseActivity);
                        if (!products.getIsFavorite()){
                            products.setIsFavorite(false);
                            holder.favouriteBtn.setBackground(mContext.getResources().getDrawable(R.drawable.ic_fav_unsel));
                        }
                        else {
                            products.setIsFavorite(true);
                            holder.favouriteBtn.setBackground(mContext.getResources().getDrawable(R.drawable.ic_fav_sel));
                        }
                        Log.d(ProductAdapter.class.getSimpleName(), "Response: " + "Something went wrong!!!!");

                    }
                });

            }
        });
    }

    // total number of rows
    @Override
    public int getItemCount() {
        return mData.size();
    }

    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView productImageView;
        Button favouriteBtn;
        TextView productName;
        TextView productCalorie;
        TextView productDiscountPrice;
        TextView productPrice;
        LinearLayout addToCart;
        RelativeLayout productLayout;
        Button addIngredients;
        TextView seperatorView;
        LinearLayout addCartLayout;
        LinearLayout addMoreToCartLayout;
        TextView recipeCartQuantity;
        TextView increaseCartQuantity;
        TextView decreaseCartQuantity;

        ViewHolder(View itemView) {
            super(itemView);
            productImageView = itemView.findViewById(R.id.recipe_imageView);
            favouriteBtn= itemView.findViewById(R.id.favouriteBtn);
            productName = itemView.findViewById(R.id.recipe_nameView);
            productCalorie = itemView.findViewById(R.id.recipe_calorieView);
            productDiscountPrice = itemView.findViewById(R.id.recipe_discountcostView);
            productPrice = itemView.findViewById(R.id.recipe_costView);
            addToCart = itemView.findViewById(R.id.addCartLayout);
            productLayout = itemView.findViewById(R.id.recipeLayout);
            addIngredients = itemView.findViewById(R.id.addIngredientsBtn);
            seperatorView= itemView.findViewById(R.id.recipe_seperatorView);
            addCartLayout = itemView.findViewById(R.id.addCartLayout);
            addMoreToCartLayout = itemView.findViewById(R.id.addMoreToCartLayout);
            recipeCartQuantity = itemView.findViewById(R.id.recipeCartQuantity);
            decreaseCartQuantity = itemView.findViewById(R.id.decrementRecipeOrder);
            increaseCartQuantity = itemView.findViewById(R.id.incrementRecipeOrder);

            int setColor = blue_color;

            if (selectedTab.equals("0") || selectedTab.equals("-1"))
            {
                setColor = blue_color;
                addIngredients.setBackground(mContext.getDrawable(R.drawable.add_cart_blue));
                addMoreToCartLayout.setBackground(mContext.getDrawable(R.drawable.add_cart_blue));
                // blue color
            }
            else if (selectedTab.equals("1"))
            {
                // green color
                setColor = green_color;
                addIngredients.setBackground(mContext.getDrawable(R.drawable.add_cart_green));
                addMoreToCartLayout.setBackground(mContext.getDrawable(R.drawable.add_cart_green));
            }
            else {
                // red color
                setColor = red_color;
                addIngredients.setBackground(mContext.getDrawable(R.drawable.add_cart_red));
                addMoreToCartLayout.setBackground(mContext.getDrawable(R.drawable.add_cart_red));
            }
            productCalorie.setTextColor(setColor);
            productPrice.setTextColor(setColor);
            productDiscountPrice.setTextColor(setColor);
            seperatorView.setTextColor(setColor);


            productImageView.setClipToOutline(true);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    // convenience method for getting data at click position
    ProductModel.Products getItem(int id) {
        return mData.get(id);
    }

    // allows clicks events to be caught
    void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}
