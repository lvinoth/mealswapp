package com.greeneru.mealswapp;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.greeneru.mealswapp.Utilities.SharedPreferenceUtility;

public class ProfileActivity extends AppCompatActivity {

    private TextView logOut;
    private Toolbar toolbar;
    private ActionBar actionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        UtilsClass.makeStatusBarTransparent(getWindow(), this);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final TextView view = findViewById(R.id.toolbarTitleView);
        view.setText("MY PROFILE");

        actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);

        logOut = findViewById(R.id.logoutBtn);
        logOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferenceUtility.savePreferencesForLogout(getApplicationContext());
                startActivity(new Intent(getApplicationContext(), MainActivity.class));
                Intent intent = getIntent();
                setResult(RESULT_OK, intent);
                finish();
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.e(ProfileActivity.class.getSimpleName(),"Profile Activity Destroyed");
    }
}
