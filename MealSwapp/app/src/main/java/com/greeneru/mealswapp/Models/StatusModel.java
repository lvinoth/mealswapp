package com.greeneru.mealswapp.Models;

import java.util.List;

/**
 * Created by shastamobiledev on 03/04/18.
 */

public class StatusModel {


    private boolean success;

    private boolean status;

    private String message;

    private String userId;

    private String goalId;


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean getSuccess() {
        return success;
    }

    public void setSuccess(boolean success)
    {
        this.success = success;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean stat)
    {
        this.status = stat;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getGoalId() {
        return goalId;
    }

    public void setGoalId(String goalId) {
        this.goalId = goalId;
    }
}
