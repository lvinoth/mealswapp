package com.greeneru.mealswapp.Models;

import java.util.List;

public class CartModel {

    private boolean status;
    private String message;
    private List<RecipesInCart> CartRecipesDetail;
    private List<ProductsInCart> CartProductsDetail;
    private PaymentDetails CartPaymentDetail;

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<RecipesInCart> getCartRecipesDetail() {
        return CartRecipesDetail;
    }

    public void setCartRecipesDetail(List<RecipesInCart> cartRecipesDetail) {
        CartRecipesDetail = cartRecipesDetail;
    }

    public List<ProductsInCart> getCartProductsDetail() {
        return CartProductsDetail;
    }

    public void setCartProductsDetail(List<ProductsInCart> cartProductsDetail) {
        CartProductsDetail = cartProductsDetail;
    }

    public PaymentDetails getCartPaymentDetail() {
        return CartPaymentDetail;
    }

    public void setCartPaymentDetail(PaymentDetails cartPaymentDetail) {
        CartPaymentDetail = cartPaymentDetail;
    }

    public class PaymentDetails
    {
        private String CustID;
        private String STRID;
        private String TotalPrice;
        private String TotalDiscountPrice;
        private String SubTotalPrice;
        private String TaxCgstSgstPercentage;
        private String deliveryCharge;
        private int extraChargePercentage;
        private double extraCharge;
        private double taxAmount;
        private int GrandTotalPrice;

        public String getCustID() {
            return CustID;
        }

        public void setCustID(String custID) {
            CustID = custID;
        }

        public String getSTRID() {
            return STRID;
        }

        public void setSTRID(String STRID) {
            this.STRID = STRID;
        }

        public String getTotalPrice() {
            return TotalPrice;
        }

        public void setTotalPrice(String totalPrice) {
            TotalPrice = totalPrice;
        }

        public String getTotalDiscountPrice() {
            return TotalDiscountPrice;
        }

        public void setTotalDiscountPrice(String totalDiscountPrice) {
            TotalDiscountPrice = totalDiscountPrice;
        }

        public String getSubTotalPrice() {
            return SubTotalPrice;
        }

        public void setSubTotalPrice(String subTotalPrice) {
            SubTotalPrice = subTotalPrice;
        }

        public String getTaxCgstSgstPercentage() {
            return TaxCgstSgstPercentage;
        }

        public void setTaxCgstSgstPercentage(String taxCgstSgstPercentage) {
            TaxCgstSgstPercentage = taxCgstSgstPercentage;
        }

        public String getDeliveryCharge() {
            return deliveryCharge;
        }

        public void setDeliveryCharge(String deliveryCharge) {
            this.deliveryCharge = deliveryCharge;
        }

        public int getExtraChargePercentage() {
            return extraChargePercentage;
        }

        public void setExtraChargePercentage(int extraChargePercentage) {
            this.extraChargePercentage = extraChargePercentage;
        }

        public double getExtraCharge() {
            return extraCharge;
        }

        public void setExtraCharge(double extraCharge) {
            this.extraCharge = extraCharge;
        }

        public double getTaxAmount() {
            return taxAmount;
        }

        public void setTaxAmount(double taxAmount) {
            this.taxAmount = taxAmount;
        }

        public int getGrandTotalPrice() {
            return GrandTotalPrice;
        }

        public void setGrandTotalPrice(int grandTotalPrice) {
            GrandTotalPrice = grandTotalPrice;
        }
    }


    public static class RecipesInCart
    {
        private String userId;
        private String cartId;
        private String recipeId;
        private String recipeName;
        private String recipePrice;
        private String recipeQty;
        private String TotPrice;
        private String recipeCalories;
        private String recipeImage;
        private List<ProductsInCart> Ingredients;
        private boolean isChecked;


        public List<ProductsInCart> getIngredients() {
            return Ingredients;
        }

        public void setIngredients(List<ProductsInCart> ingredients) {
            Ingredients = ingredients;
        }

        public String getRecipeImage() {
            return recipeImage;
        }

        public void setRecipeImage(String recipeImage) {
            this.recipeImage = recipeImage;
        }

        public String getRecipeCalories() {
            return recipeCalories;
        }

        public void setRecipeCalories(String recipeCalories) {
            this.recipeCalories = recipeCalories;
        }

        public String getTotPrice() {
            return TotPrice;
        }

        public void setTotPrice(String totPrice) {
            TotPrice = totPrice;
        }

        public String getRecipePrice() {
            return recipePrice;
        }

        public void setRecipePrice(String recipePrice) {
            this.recipePrice = recipePrice;
        }

        public String getRecipeName() {
            return recipeName;
        }

        public void setRecipeName(String recipeName) {
            this.recipeName = recipeName;
        }

        public String getRecipeId() {
            return recipeId;
        }

        public void setRecipeId(String recipeId) {
            this.recipeId = recipeId;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getCartId() {
            return cartId;
        }

        public void setCartId(String cartId) {
            this.cartId = cartId;
        }

        public String getRecipeQty() {
            return recipeQty;
        }

        public void setRecipeQty(String recipeQty) {
            this.recipeQty = recipeQty;
        }

        public boolean isChecked() {
            return isChecked;
        }

        public void setChecked(boolean checked) {
            isChecked = checked;
        }
    }


    public class ProductsInCart
    {
        private String Id;
        private String cartId;
        private String userId;
        private String recipeId;
        private String productId;
        private String productName;
        private String productPrice;
        private String productQty;
        private String sumPrice;
        private String CaloriesKcal;
        private String productImage;
        private boolean isChecked;


        public String getId() {
            return Id;
        }

        public void setId(String id) {
            Id = id;
        }

        public String getCartId() {
            return cartId;
        }

        public void setCartId(String cartId) {
            this.cartId = cartId;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getProductId() {
            return productId;
        }

        public void setProductId(String productId) {
            this.productId = productId;
        }

        public String getProductName() {
            return productName;
        }

        public void setProductName(String productName) {
            this.productName = productName;
        }

        public String getProductPrice() {
            return productPrice;
        }

        public void setProductPrice(String productPrice) {
            this.productPrice = productPrice;
        }

        public String getProductQty() {
            return productQty;
        }

        public void setProductQty(String productQty) {
            this.productQty = productQty;
        }

        public String getSumPrice() {
            return sumPrice;
        }

        public void setSumPrice(String sumPrice) {
            this.sumPrice = sumPrice;
        }

        public String getCaloriesKcal() {
            return CaloriesKcal;
        }

        public void setCaloriesKcal(String caloriesKcal) {
            CaloriesKcal = caloriesKcal;
        }

        public String getProductImage() {
            return productImage;
        }

        public void setProductImage(String productImage) {
            this.productImage = productImage;
        }

        public boolean isChecked() {
            return isChecked;
        }

        public void setChecked(boolean checked) {
            isChecked = checked;
        }

        public String getRecipeId() {
            return recipeId;
        }

        public void setRecipeId(String recipeId) {
            this.recipeId = recipeId;
        }
    }




}
