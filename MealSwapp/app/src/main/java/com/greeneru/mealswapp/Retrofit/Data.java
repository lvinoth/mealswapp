package com.greeneru.mealswapp.Retrofit;

import com.google.gson.annotations.Expose;

public class Data
{
    @Expose
    private String ingredientId;
    @Expose
    private String ingredientName;
    @Expose
    private String qty;

    public String getIngredientId() {
        return ingredientId;
    }

    public void setIngredientId(String ingredientId) {
        this.ingredientId = ingredientId;
    }

    public String getIngredientName() {
        return ingredientName;
    }

    public void setIngredientName(String ingredientName) {
        this.ingredientName = ingredientName;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }
}
