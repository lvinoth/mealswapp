package com.greeneru.mealswapp;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.greeneru.mealswapp.Models.StatusModel;
import com.greeneru.mealswapp.Retrofit.ApiClient;
import com.greeneru.mealswapp.Retrofit.ApiInterface;
import com.greeneru.mealswapp.Utilities.SharedPreferenceUtility;

import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.widget.Toast.LENGTH_LONG;

public class SignUpActivity extends AppCompatActivity {

    private ActionBar actionBar;
    private Toolbar toolbar;
    private LinearLayout signUpLayout;
    private Button emailSignUpButton;
    private ApiInterface apiService;
    private EditText firstName;
    private EditText lastName;
    private EditText email;
    private EditText password;
    private Activity self;

    private static final String TAG = SignUpActivity.class.getSimpleName();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        UtilsClass.makeStatusBarTransparent(getWindow(), this);
        toolbar = findViewById(R.id.toolbar_signup);
        setSupportActionBar(toolbar);
        final TextView view = findViewById(R.id.toolbarTitleView);
        view.setText("CREATE AN ACCOUNT");
        self = this;

        actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);

        firstName = findViewById(R.id.firstNameText);
        lastName = findViewById(R.id.lastNameText);
        email = findViewById(R.id.emailText);
        password = findViewById(R.id.passwordText);

        signUpLayout = findViewById(R.id.signUpLayout);
        emailSignUpButton = findViewById(R.id.emailSignUp);

        apiService = ApiClient.getClient().create(ApiInterface.class);

        emailSignUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (signUpLayout.getVisibility() == View.GONE)
                {
                    ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) v.getLayoutParams();
                    final float scale = getBaseContext().getResources().getDisplayMetrics().density;
                    // convert the DP into pixel
                    int l =  (int)(40 * scale + 0.5f);
                    int r =  (int)(40 * scale + 0.5f);
                    int t =  (int)(30 * scale + 0.5f);
                    int b =  (int)(40 * scale + 0.5f);
                    params.setMargins(l,t,r,b);
                    v.requestLayout();
                    emailSignUpButton.setLayoutParams(params);
                    signUpLayout.setVisibility(View.VISIBLE);
                }
                else
                {
                    // validate username/ password
                    String firstNameText = firstName.getText().toString();
                    String lastNameText = lastName.getText().toString();
                    String emailText = email.getText().toString();
                    String passwordText = password.getText().toString();


                    if (TextUtils.isEmpty(firstNameText))
                    {
                        showToastAtCentre("Please enter your first name", LENGTH_LONG);
                        return;
                    }
                    if (TextUtils.isEmpty(lastNameText))
                    {
                        showToastAtCentre("Please enter your last name", LENGTH_LONG);
                        return;
                    }
                    if (TextUtils.isEmpty(emailText))
                    {
                        showToastAtCentre("Please enter your email", LENGTH_LONG);
                        return;
                    }
                    if (TextUtils.isEmpty(passwordText))
                    {
                        showToastAtCentre("Please enter password", LENGTH_LONG);
                        return;
                    }
                    if (!UtilsClass.isValidEmail(emailText))
                    {
                        showToastAtCentre("Please enter a valid email", LENGTH_LONG);
                        return;
                    }

                    doSignUp(firstNameText, lastNameText, emailText, passwordText, "email");
                    //doSignIn(emailText, passwordText);
                }
            }
        });

    }

    private Toast mToast;
    private void showToastAtCentre(String message, int duration)
    {
        if (mToast != null)
        {
            mToast.cancel();
            mToast = null;
        }
        mToast = Toast.makeText(getApplicationContext(), message, duration);
        mToast.setGravity(Gravity.CENTER, 0, 0);
        mToast.show();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /*public void doSignUp(String firstName, String lastName, String email, String password, String source)
    {
        Call<StatusModel> call = apiService.doSignUpEmail("","",email, password, source);

        call.enqueue(new Callback<StatusModel>() {
            @Override
            public void onResponse(Call<StatusModel> call, Response<StatusModel> response) {

                String mess = response.body().getMessage();
                Log.d(TAG, "Response: " + mess);

            }

            @Override
            public void onFailure(Call<StatusModel> call, Throwable t) {
                Log.d(TAG, "Response: " + "Something went wrong!!!!");
            }
        });
    }*/


    public void doSignUp(String fName, String lName, String email, String password, String source)
    {
        UtilsClass.showBusyAnimation(self,"Signing up..");
        String time = UtilsClass.getSimpleDateTimeFormat().format(new Date());
        Log.d(TAG,"time: " + time);
        String token = UtilsClass.getBase64HashedString(time);
        String goalId = SharedPreferenceUtility.getUserGoal(getApplicationContext());

        Call<StatusModel> call = apiService.doSignUpEmail(fName, lName, email, password, source, time.trim(), token.trim(), goalId);

        call.enqueue(new Callback<StatusModel>() {
            @Override
            public void onResponse(Call<StatusModel> call, Response<StatusModel> response) {

                UtilsClass.hideBusyAnimation(self);
                if (response.body().getStatus())
                {
                    String userId = response.body().getUserId();
                    SharedPreferenceUtility.saveUserLoginDetails(getApplicationContext(),userId ,"");
                    Intent intent = getIntent();
                    setResult(RESULT_OK, intent);
                    finish();
                }
                else // Handle failure cases here
                {
                    String mess = response.body().getMessage();
                    Log.d(TAG, "Response: " + mess);
                    showToastAtCentre("Response: " + mess, LENGTH_LONG);
                }
            }

            @Override
            public void onFailure(Call<StatusModel> call, Throwable t) {
                UtilsClass.hideBusyAnimation(self);
                Log.d(TAG, "Response: " + t.getMessage());
                showToastAtCentre("Response: " + t.getMessage(), LENGTH_LONG);

            }
        });
    }

}
