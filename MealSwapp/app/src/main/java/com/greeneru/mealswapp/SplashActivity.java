package com.greeneru.mealswapp;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.greeneru.mealswapp.Utilities.SharedPreferenceUtility;

public class SplashActivity extends AppCompatActivity {

    private Handler handler = new Handler();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        UtilsClass.makeStatusBarTransparent(getWindow(), this);
        showMainScreen();

    }

    private void showMainScreen()
    {

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                // Launch Main Activity here

                if (SharedPreferenceUtility.getUserId(getApplicationContext()).equals("EMPTY"))
                {
                    Intent i = new Intent(getBaseContext(),MainActivity.class);
                    startActivity(i);
                }
                else {
                    Intent i = new Intent(getBaseContext(),HomeActivity.class);
                    startActivity(i);
                }

                finish();
            }
        }, 2000);
    }

}
