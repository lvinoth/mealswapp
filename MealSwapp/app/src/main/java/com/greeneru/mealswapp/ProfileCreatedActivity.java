package com.greeneru.mealswapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.TextView;

public class ProfileCreatedActivity extends AppCompatActivity {

    private TextView profileCreatedText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_created);
        UtilsClass.makeStatusBarTransparent(getWindow(), this);
        TextView goToProfileView = findViewById(R.id.goToHomeScreenView);
        profileCreatedText = findViewById(R.id.profileCreatedText);
        goToProfileView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(),HomeActivity.class));
                finish();
            }
        });
    }
}
