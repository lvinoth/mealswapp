package com.greeneru.mealswapp;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.greeneru.mealswapp.Utilities.SharedPreferenceUtility;

import java.util.ArrayList;
import java.util.List;

import static com.greeneru.mealswapp.TourPageView.GOALS.BUILD_MUSCLE;
import static com.greeneru.mealswapp.TourPageView.GOALS.GENERAL_HEALTH;
import static com.greeneru.mealswapp.TourPageView.GOALS.LOSE_WEIGHT;
import static com.greeneru.mealswapp.TourPageView.GOALS.LOWER_CHOLESTEROL;
import static com.greeneru.mealswapp.TourPageView.GOALS.LOWER_DIABETES;
import static com.greeneru.mealswapp.TourPageView.GOALS.REDUCE_BLOOD_PRESSURE;

public class MainActivity extends AppCompatActivity {

    private ViewPager viewPager;
    private MyViewPagerAdapter myViewPagerAdapter;
    private TextView[] dots;
    private int[] layouts;
    private TextView loginView;
    private TextView signUpView;
    private TextView titleText;
    private static final int SIGNUP_REQUEST_CODE = 101;
    private static final int SIGNIN_REQUEST_CODE = 102;


    List<TourPageView.GOALS> selectedGoal = new ArrayList<>(6);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        UtilsClass.makeStatusBarTransparent(getWindow(), this);
        TextView goal = findViewById(R.id.goal);
        TextView tour1 = findViewById(R.id.tour1);
        TextView tour2 = findViewById(R.id.tour2);
        TextView tour3 = findViewById(R.id.tour3);
        titleText = findViewById(R.id.titleText);

        dots = new TextView[]{goal,tour1,tour2, tour3};

        viewPager =  findViewById(R.id.view_pager);

        signUpView = findViewById(R.id.beginJourneyView);
        signUpView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int pageNo = viewPager.getCurrentItem();
                if (pageNo == 0)
                {
                    viewPager.setCurrentItem(1,true);
                }
                else {
                    startActivityForResult(new Intent(getApplicationContext(), SignUpActivity.class), SIGNUP_REQUEST_CODE);
                }

            }
        });

        loginView = findViewById(R.id.loginBtn);
        loginView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivityForResult(new Intent(getApplicationContext(), LoginActivity.class), SIGNIN_REQUEST_CODE);
            }
        });

        layouts = new int[]{
                R.layout.fragment_goals,
                R.layout.welcome_slide1,
                R.layout.welcome_slide2,
                R.layout.welcome_slide3};

        // adding bottom dots
        addBottomDots(0);

        myViewPagerAdapter = new MyViewPagerAdapter();
        viewPager.setAdapter(myViewPagerAdapter);
        viewPager.addOnPageChangeListener(viewPagerPageChangeListener);
        viewPager.setOffscreenPageLimit(3);
    }

    private void setTitleText()
    {
        String text = "<font color=#EC9893>MEAL</font><font color=#79B1E0>SWAPP TOUR</font>";
        titleText.setText(Html.fromHtml(text));
    }


    private void selectGoals(List<RelativeLayout> goalArray, int index)
    {
        int goalId = index + 1;
        for (RelativeLayout goal:goalArray) {
            goal.setBackground(getResources().getDrawable(R.drawable.goal_unselect));
        }
        goalArray.get(index).setBackground(getResources().getDrawable(R.drawable.goal_select));
        signUpView.setEnabled(true);
        signUpView.setAlpha(1f);
        SharedPreferenceUtility.saveUserGoal(getApplicationContext(),String.valueOf(goalId));

    }

    private void addBottomDots(int currentPage) {
        for (TextView dot : dots) {
            dot.setBackground(getResources().getDrawable(R.drawable.dot_unselected));
        }

        if (dots.length > 0)
            dots[currentPage].setBackground(getResources().getDrawable(R.drawable.dot_selected));
    }

    //  viewpager change listener
    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageSelected(int position) {
            addBottomDots(position);
            if (position == 0)
            {
                titleText.setText("PICK YOUR HEALTH GOAL");
            }
            else {
                setTitleText();
            }
            if (position == 0)
            {
                signUpView.setText("Start Tour");
            }else if(position != 3)
            {
                signUpView.setText("Skip Tour");
            }
            else {
                signUpView.setText("Begin Your Journey");
            }
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {

        }

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }
    };


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode)
        {
            case SIGNUP_REQUEST_CODE:
                if (resultCode == Activity.RESULT_OK)
                {
                    startActivity(new Intent(getApplicationContext(),ProfileCreatedActivity.class));
                    finish();
                }
                break;

            case SIGNIN_REQUEST_CODE:
                if (resultCode == Activity.RESULT_OK)
                {
                    startActivity(new Intent(getApplicationContext(),HomeActivity.class));
                    finish();
                }
                break;
        }

    }

    /**
     * View pager adapter
     */
    public class MyViewPagerAdapter extends PagerAdapter {
        private LayoutInflater layoutInflater;

        public MyViewPagerAdapter() {
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View view = layoutInflater.inflate(layouts[position], container, false);
            if (layouts[0] == layouts[position]) {

                final RelativeLayout loseWeightView = view.findViewById(R.id.loseWeightView);
                final RelativeLayout buildMuscleView = view.findViewById(R.id.buildMuscleView);
                final RelativeLayout reduceBloodPressureView = view.findViewById(R.id.reduceBloodPressureView);
                final RelativeLayout lowerDiabetesView = view.findViewById(R.id.lowerDiabetesView);
                final RelativeLayout lowerCholesterolView = view.findViewById(R.id.lowerCholesterolView);
                final RelativeLayout generalHealthView = view.findViewById(R.id.generalHealthView);

                // Vinoth: Do not change this order as it will mislead the goal selection.
                final List<RelativeLayout> goals = new ArrayList<>();
                goals.add(loseWeightView);
                goals.add(buildMuscleView);
                goals.add(lowerDiabetesView);
                goals.add(lowerCholesterolView);
                goals.add(reduceBloodPressureView);
                goals.add(generalHealthView);

                loseWeightView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        selectGoals(goals, 0);
                    }
                });

                buildMuscleView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        selectGoals(goals, 1);
                    }
                });

                lowerDiabetesView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        selectGoals(goals, 2);
                    }
                });

                lowerCholesterolView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        selectGoals(goals, 3);
                    }
                });

                reduceBloodPressureView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        selectGoals(goals, 4);
                    }
                });

                generalHealthView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        selectGoals(goals, 5);
                    }
                });

            }
            container.addView(view);

            return view;
        }

        @Override
        public int getCount() {
            return layouts.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object obj) {
            return view == obj;
        }


        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            View view = (View) object;
            container.removeView(view);
        }
    }
}
