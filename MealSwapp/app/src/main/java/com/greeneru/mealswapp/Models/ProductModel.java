package com.greeneru.mealswapp.Models;

import java.util.List;

public class ProductModel {

    private boolean status;
    private String message;
    private List<Products> products;

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Products> getProducts() {
        return products;
    }

    public void setProducts(List<Products> products) {
        this.products = products;
    }


    public class Products {
        private String ProductID;
        private String CatID;
        private String CategoryName;
        private String SubCatID;
        private String SubCatName;
        private String ProductName;
        private String STRID;
        private String StoreName;
        private String BRID;
        private String BrandName;
        private String PTID;
        private String ItemCode;
        private String ItemName;
        private String ProductImage;
        private String Weight;
        private String CaloriesKJ;
        private String CaloriesKcal;
        private String Fat;
        private String Protein;
        private String Carbs;
        private String Sugar;
        private String SatFat;
        private String CarbohyDrates;
        private String Cholestrol;
        private String AddedSugars;
        private String Price;
        private String DiscountPrice;
        private String DiscountValidity;
        private String SalePrice;
        private String Nutrition;
        private String GproteinPerKcal;
        private String FiberPerKcal;
        private String Sodium;
        private String SourceOfNutrition;
        private String Points;
        private String Status;
        private String AddedBy;
        private String CustID;
        private String cartId;
        private int cartqty;
        private boolean isFavorite;

        public String getProductID() {
            return ProductID;
        }

        public void setProductID(String productID) {
            ProductID = productID;
        }

        public String getCatID() {
            return CatID;
        }

        public void setCatID(String catID) {
            CatID = catID;
        }

        public String getCategoryName() {
            return CategoryName;
        }

        public void setCategoryName(String categoryName) {
            CategoryName = categoryName;
        }

        public String getSubCatID() {
            return SubCatID;
        }

        public void setSubCatID(String subCatID) {
            SubCatID = subCatID;
        }

        public String getSubCatName() {
            return SubCatName;
        }

        public void setSubCatName(String subCatName) {
            SubCatName = subCatName;
        }

        public String getProductName() {
            return ProductName;
        }

        public void setProductName(String productName) {
            ProductName = productName;
        }

        public String getSTRID() {
            return STRID;
        }

        public void setSTRID(String STRID) {
            this.STRID = STRID;
        }

        public String getStoreName() {
            return StoreName;
        }

        public void setStoreName(String storeName) {
            StoreName = storeName;
        }

        public String getBRID() {
            return BRID;
        }

        public void setBRID(String BRID) {
            this.BRID = BRID;
        }

        public String getBrandName() {
            return BrandName;
        }

        public void setBrandName(String brandName) {
            BrandName = brandName;
        }

        public String getPTID() {
            return PTID;
        }

        public void setPTID(String PTID) {
            this.PTID = PTID;
        }

        public String getItemCode() {
            return ItemCode;
        }

        public void setItemCode(String itemCode) {
            ItemCode = itemCode;
        }

        public String getItemName() {
            return ItemName;
        }

        public void setItemName(String itemName) {
            ItemName = itemName;
        }

        public String getWeight() {
            return Weight;
        }

        public void setWeight(String weight) {
            Weight = weight;
        }

        public String getCaloriesKJ() {
            return CaloriesKJ;
        }

        public void setCaloriesKJ(String caloriesKJ) {
            CaloriesKJ = caloriesKJ;
        }

        public String getCaloriesKcal() {
            return CaloriesKcal;
        }

        public void setCaloriesKcal(String caloriesKcal) {
            CaloriesKcal = caloriesKcal;
        }

        public String getFat() {
            return Fat;
        }

        public void setFat(String fat) {
            Fat = fat;
        }

        public String getProtein() {
            return Protein;
        }

        public void setProtein(String protein) {
            Protein = protein;
        }

        public String getCarbs() {
            return Carbs;
        }

        public void setCarbs(String carbs) {
            Carbs = carbs;
        }

        public String getSugar() {
            return Sugar;
        }

        public void setSugar(String sugar) {
            Sugar = sugar;
        }

        public String getSatFat() {
            return SatFat;
        }

        public void setSatFat(String satFat) {
            SatFat = satFat;
        }

        public String getCarbohyDrates() {
            return CarbohyDrates;
        }

        public void setCarbohyDrates(String carbohyDrates) {
            CarbohyDrates = carbohyDrates;
        }

        public String getCholestrol() {
            return Cholestrol;
        }

        public void setCholestrol(String cholestrol) {
            Cholestrol = cholestrol;
        }

        public String getAddedSugars() {
            return AddedSugars;
        }

        public void setAddedSugars(String addedSugars) {
            AddedSugars = addedSugars;
        }

        public String getPrice() {
            return Price;
        }

        public void setPrice(String price) {
            Price = price;
        }

        public String getDiscountPrice() {
            return DiscountPrice;
        }

        public void setDiscountPrice(String discountPrice) {
            DiscountPrice = discountPrice;
        }

        public String getDiscountValidity() {
            return DiscountValidity;
        }

        public void setDiscountValidity(String discountValidity) {
            DiscountValidity = discountValidity;
        }

        public String getSalePrice() {
            return SalePrice;
        }

        public void setSalePrice(String salePrice) {
            SalePrice = salePrice;
        }

        public String getNutrition() {
            return Nutrition;
        }

        public void setNutrition(String nutrition) {
            Nutrition = nutrition;
        }

        public String getGproteinPerKcal() {
            return GproteinPerKcal;
        }

        public void setGproteinPerKcal(String gproteinPerKcal) {
            GproteinPerKcal = gproteinPerKcal;
        }

        public String getFiberPerKcal() {
            return FiberPerKcal;
        }

        public void setFiberPerKcal(String fiberPerKcal) {
            FiberPerKcal = fiberPerKcal;
        }

        public String getSodium() {
            return Sodium;
        }

        public void setSodium(String sodium) {
            Sodium = sodium;
        }

        public String getSourceOfNutrition() {
            return SourceOfNutrition;
        }

        public void setSourceOfNutrition(String sourceOfNutrition) {
            SourceOfNutrition = sourceOfNutrition;
        }

        public String getPoints() {
            return Points;
        }

        public void setPoints(String points) {
            Points = points;
        }

        public String getStatus() {
            return Status;
        }

        public void setStatus(String status) {
            Status = status;
        }

        public String getAddedBy() {
            return AddedBy;
        }

        public void setAddedBy(String addedBy) {
            AddedBy = addedBy;
        }

        public String getProductImage() {
            return ProductImage;
        }

        public void setProductImage(String productImage) {
            ProductImage = productImage;
        }

        public String getCustID() {
            return CustID;
        }

        public void setCustID(String custID) {
            CustID = custID;
        }

        public boolean getIsFavorite() {
            return isFavorite;
        }

        public void setIsFavorite(boolean isFavorite) {
            this.isFavorite = isFavorite;
        }

        public String getCartId() {
            return cartId;
        }

        public void setCartId(String cartId) {
            this.cartId = cartId;
        }

        public int getCartqty() {
            return cartqty;
        }

        public void setCartqty(int cartqty) {
            this.cartqty = cartqty;
        }
    }

}
