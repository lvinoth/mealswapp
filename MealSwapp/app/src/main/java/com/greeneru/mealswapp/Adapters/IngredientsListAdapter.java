package com.greeneru.mealswapp.Adapters;

import android.content.Context;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.greeneru.mealswapp.Models.RecipeDetailsModel;
import com.greeneru.mealswapp.R;

import java.util.ArrayList;

public class IngredientsListAdapter extends ArrayAdapter<RecipeDetailsModel> implements View.OnClickListener {

    private ArrayList<RecipeDetailsModel> dataSet;
    Context mContext;


    private static class ViewHolder {
        TextView txtName;
        TextView txtType;
        TextView txtVersion;
        ImageView info;
    }

    public IngredientsListAdapter(ArrayList<RecipeDetailsModel> data, Context context) {
        super(context, R.layout.ingredient_item, data);
        this.dataSet = data;
        this.mContext=context;

    }






























    @Override
    public void onClick(View v) {

    }
}
