package com.greeneru.mealswapp.Adapters;


import android.content.Context;
import android.graphics.Typeface;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.greeneru.mealswapp.Models.CartModel;
import com.greeneru.mealswapp.Models.DataModel;
import com.greeneru.mealswapp.Models.RecipeDetailsModel;
import com.greeneru.mealswapp.R;

import java.util.List;

public class ProductInCartAdapter extends ArrayAdapter<CartModel.ProductsInCart> implements View.OnClickListener{

    private List<CartModel.ProductsInCart> dataSet;
    Context mContext;

    // View lookup cache
    private static class ViewHolder {
        TextView ingredientName;
        /*TextView ingredientPrice;
        TextView ingredientQuantity;
        TextView ingredientRequiredQuantity;
        TextView ingredientUnit;
        Button incrementView;
        Button decrementView;*/

    }

    public ProductInCartAdapter(List<CartModel.ProductsInCart> data, Context context) {
        super(context, R.layout.cart_item, data);
        this.dataSet = data;
        this.mContext = context;
    }

    @Override
    public void onClick(View v) {

        int position=(Integer) v.getTag();
        Object object= getItem(position);
        DataModel dataModel=(DataModel)object;

        switch (v.getId())
        {
            case R.id.incrementView:
                Snackbar.make(v, "Quantity incremented", Snackbar.LENGTH_LONG)
                        .setAction("No action", null).show();
                break;

                case R.id.decrementView:
                Snackbar.make(v, "Quantity decremented", Snackbar.LENGTH_LONG)
                        .setAction("No action", null).show();
                break;
        }
    }

    private int lastPosition = -1;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        final CartModel.ProductsInCart dataModel = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        final ViewHolder viewHolder; // view lookup cache stored in tag

        final View result;

        if (convertView == null) {

            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.cart_item, parent, false);
            final Typeface mediumFont = Typeface.createFromAsset(mContext.getAssets(), "fonts/gotham_rounded_medium.otf");
            final Typeface lightFont = Typeface.createFromAsset(mContext.getAssets(), "fonts/gotham_rounded_light.otf");

            viewHolder.ingredientName = convertView.findViewById(R.id.cartItemTitleView);
            /*viewHolder.ingredientPrice =  convertView.findViewById(R.id.ingredientPriceView);
            viewHolder.ingredientQuantity = convertView.findViewById(R.id.quantityView);
            viewHolder.ingredientRequiredQuantity =  convertView.findViewById(R.id.ingredientRequiredView);
            viewHolder.ingredientUnit =  convertView.findViewById(R.id.quantityUnitView);
            viewHolder.incrementView  =convertView.findViewById(R.id.incrementView);
            viewHolder.decrementView  =convertView.findViewById(R.id.decrementView);*/

            viewHolder.ingredientName.setTypeface(mediumFont);
            /*viewHolder.ingredientPrice.setTypeface(mediumFont);
            viewHolder.ingredientQuantity.setTypeface(lightFont);
            viewHolder.ingredientRequiredQuantity.setTypeface(lightFont);
            viewHolder.ingredientUnit.setTypeface(lightFont);
            viewHolder.incrementView.setTypeface(lightFont);
            viewHolder.decrementView.setTypeface(lightFont);*/


            result=convertView;

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            result=convertView;
        }

        lastPosition = position;

        viewHolder.ingredientName.setText(dataModel.getProductName());
        /*viewHolder.ingredientPrice.setText("₹ " +dataModel.getSalePrice());
        viewHolder.ingredientQuantity.setText(dataModel.getWeight());
        viewHolder.ingredientRequiredQuantity.setText(dataModel.getRecipes_quantity());
        viewHolder.ingredientUnit.setText(dataModel.getUnitName());*/

        // Return the completed view to render on screen
        return convertView;
    }

    public int getCount() {
        return dataSet.size();
    }

    public CartModel.ProductsInCart getItem(int position) {
        return dataSet.get(position);
    }

    public long getItemId(int position) {
        return position;
    }
}

