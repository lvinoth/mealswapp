package com.greeneru.mealswapp;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import static android.widget.Toast.LENGTH_LONG;

public class ForgotPasswordActivity extends AppCompatActivity {

    private ActionBar actionBar;
    private Toolbar toolbar;
    private TextView sendPassword;
    private EditText emailForgot;
    private Toast mToast;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        UtilsClass.makeStatusBarTransparent(getWindow(), this);

        toolbar = findViewById(R.id.toolbar_signup);
        setSupportActionBar(toolbar);
        final TextView view = findViewById(R.id.toolbarTitleView);
        view.setText("FORGOT PASSWORD");

        actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        emailForgot = findViewById(R.id.emailTextForgot);
        sendPassword = findViewById(R.id.sendPassword);
        sendPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String emailText = emailForgot.getText().toString();

                if (TextUtils.isEmpty(emailText)) {
                    showToastAtCentre("Please enter your email", LENGTH_LONG);
                    return;
                }
                if (!UtilsClass.isValidEmail(emailText))
                {
                    showToastAtCentre("Please enter a valid email", LENGTH_LONG);
                    return;
                }
                sendPasswordLink(emailText);
            }
        });
    }

    private void sendPasswordLink(String email)
    {

    }


    private void showToastAtCentre(String message, int duration) {
        if (mToast != null) {
            mToast.cancel();
            mToast = null;
        }
        mToast = Toast.makeText(getApplicationContext(), message, duration);
        mToast.setGravity(Gravity.CENTER, 0, 0);
        mToast.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
