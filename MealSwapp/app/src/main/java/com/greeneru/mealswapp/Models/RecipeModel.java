package com.greeneru.mealswapp.Models;

import java.util.List;

public class RecipeModel {

    private boolean success;

    private boolean status;

    private String message;

    private String userId;

    private List<Recipes> recipes;

    private List<ApiRecipes> api_recipes;


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean getSuccess() {
        return success;
    }

    public void setSuccess(boolean success)
    {
        this.success = success;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean stat)
    {
        this.status = stat;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public List<Recipes> getRecipes() {
        return recipes;
    }

    public void setRecipes(List<Recipes> recipes) {
        this.recipes = recipes;
    }

    public List<ApiRecipes> getApi_recipes() {
        return api_recipes;
    }

    public void setApi_recipes(List<ApiRecipes> api_recipes) {
        this.api_recipes = api_recipes;
    }

    public class ApiRecipes{

        private  String ID;
        private String recipeName;
        private String recipeImage;
        private String recipeCalories;
        private String FavID;
        private String price;
        private String cartId;
        private int cartqty;
        private boolean isFavorite;

        public String getID() {
            return ID;
        }

        public void setID(String ID) {
            this.ID = ID;
        }

        public String getRecipeName() {
            return recipeName;
        }

        public void setRecipeName(String recipeName) {
            this.recipeName = recipeName;
        }

        public String getRecipeImage() {
            return recipeImage;
        }

        public void setRecipeImage(String recipeImage) {
            this.recipeImage = recipeImage;
        }

        public String getRecipeCalories() {
            return recipeCalories;
        }

        public void setRecipeCalories(String recipeCalories) {
            this.recipeCalories = recipeCalories;
        }

        public String getFavID() {
            return FavID;
        }

        public void setFavID(String favID) {
            FavID = favID;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public boolean isFavorite() {
            return isFavorite;
        }

        public void setFavorite(boolean favorite) {
            isFavorite = favorite;
        }

        public String getCartId() {
            return cartId;
        }

        public void setCartId(String cartId) {
            this.cartId = cartId;
        }

        public int getCartqty() {
            return cartqty;
        }

        public void setCartqty(int cartqty) {
            this.cartqty = cartqty;
        }
    }

    public class Recipes {

        private  String RecipeID;
        private String RecipeName;
        private String RecipeImage;
        private String Calorieskcal;
        private String CaloriesKJ;
        private String Fat;
        private String Protein;
        private String Carbs;
        private String Sugar;
        private String FavID;
        private String SalePrice;
        private String DiscountPrice;
        private String cartId;
        private int cartqty;
        private boolean isFavorite;

        public String getRecipeID() {
            return RecipeID;
        }

        public void setRecipeID(String recipeID) {
            RecipeID = recipeID;
        }

        public String getRecipeName() {
            return RecipeName;
        }

        public void setRecipeName(String recipeName) {
            RecipeName = recipeName;
        }

        public String getRecipeImage() {
            return RecipeImage;
        }

        public void setRecipeImage(String recipeImage) {
            RecipeImage = recipeImage;
        }

        public String getCalorieskcal() {
            return Calorieskcal;
        }

        public void setCalorieskcal(String calorieskcal) {
            Calorieskcal = calorieskcal;
        }

        public String getFavID() {
            return FavID;
        }

        public void setFavID(String favID) {
            FavID = favID;
        }

        public String getCaloriesKJ() {
            return CaloriesKJ;
        }

        public void setCaloriesKJ(String caloriesKJ) {
            CaloriesKJ = caloriesKJ;
        }

        public String getFat() {
            return Fat;
        }

        public void setFat(String fat) {
            Fat = fat;
        }

        public String getProtein() {
            return Protein;
        }

        public void setProtein(String protein) {
            Protein = protein;
        }

        public String getCarbs() {
            return Carbs;
        }

        public void setCarbs(String carbs) {
            Carbs = carbs;
        }

        public String getSugar() {
            return Sugar;
        }

        public void setSugar(String sugar) {
            Sugar = sugar;
        }

        public String getSalePrice() {
            return SalePrice;
        }

        public void setSalePrice(String salePrice) {
            SalePrice = salePrice;
        }

        public String getDiscountPrice() {
            return DiscountPrice;
        }

        public void setDiscountPrice(String discountPrice) {
            DiscountPrice = discountPrice;
        }

        public boolean isFavorite() {
            return isFavorite;
        }

        public void setFavorite(boolean favorite) {
            isFavorite = favorite;
        }

        public String getCartId() {
            return cartId;
        }

        public void setCartId(String cartId) {
            this.cartId = cartId;
        }

        public int getCartqty() {
            return cartqty;
        }

        public void setCartqty(int cartqty) {
            this.cartqty = cartqty;
        }
    }

}
