package com.greeneru.mealswapp.Adapters;


import android.content.Context;
import android.graphics.Typeface;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.greeneru.mealswapp.Models.CategoryModel;
import com.greeneru.mealswapp.Models.CuisineModel;
import com.greeneru.mealswapp.R;

import java.util.List;

public class CategoriesAdapter extends ArrayAdapter<CategoryModel.Category> implements View.OnClickListener{

    private List<CategoryModel.Category> dataSet;
    Context mContext;

    // View lookup cache
    private static class ViewHolder {
        TextView cuisinesName;
        LinearLayout cuisineLayout;
        CheckBox cuisineCheck;

    }

    public CategoriesAdapter(List<CategoryModel.Category> data, Context context) {
        super(context, R.layout.cuisine_item, data);
        this.dataSet = data;
        this.mContext=context;

    }

    @Override
    public void onClick(View v) {

        switch (v.getId())
        {
            case R.id.cuisineItemLayout:

                Snackbar.make(v, "Quantity incremented", Snackbar.LENGTH_LONG)
                        .setAction("No action", null).show();
                break;

                case R.id.decrementView:
                Snackbar.make(v, "Quantity decremented", Snackbar.LENGTH_LONG)
                        .setAction("No action", null).show();
                break;
        }
    }

    private int lastPosition = -1;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        final CategoryModel.Category dataModel = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        final ViewHolder viewHolder; // view lookup cache stored in tag

        final View result;

        if (convertView == null) {

            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.cuisine_item, parent, false);
            final Typeface mediumFont = Typeface.createFromAsset(mContext.getAssets(), "fonts/gotham_rounded_medium.otf");

            viewHolder.cuisinesName = convertView.findViewById(R.id.cuisineName);
            viewHolder.cuisineLayout = convertView.findViewById(R.id.cuisineItemLayout);
            viewHolder.cuisineCheck = convertView.findViewById(R.id.cuisineCheck);
            viewHolder.cuisinesName.setTypeface(mediumFont);
            viewHolder.cuisineLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    viewHolder.cuisineCheck.setChecked(!viewHolder.cuisineCheck.isChecked());
                    dataModel.setSelected(viewHolder.cuisineCheck.isChecked());
                }
            });
            result=convertView;

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            result=convertView;
        }

        lastPosition = position;

        viewHolder.cuisinesName.setText(dataModel.getCategoryName().trim());

        // Return the completed view to render on screen
        return convertView;
    }

    public int getCount() {
        return dataSet.size();
    }

    public CategoryModel.Category getItem(int position) {
        return dataSet.get(position);
    }

    public long getItemId(int position) {
        return position;
    }
}

