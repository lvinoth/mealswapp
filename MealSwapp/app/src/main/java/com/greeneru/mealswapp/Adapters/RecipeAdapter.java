package com.greeneru.mealswapp.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.greeneru.mealswapp.HomeActivity;
import com.greeneru.mealswapp.Interfaces.RecipeDataChangeInterface;
import com.greeneru.mealswapp.Models.RecipeModel;
import com.greeneru.mealswapp.Models.StatusModel;
import com.greeneru.mealswapp.R;
import com.greeneru.mealswapp.RecipeDetailedActivity;
import com.greeneru.mealswapp.Retrofit.ApiClient;
import com.greeneru.mealswapp.Retrofit.ApiInterface;
import com.greeneru.mealswapp.Utilities.SharedPreferenceUtility;
import com.greeneru.mealswapp.UtilsClass;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RecipeAdapter extends RecyclerView.Adapter<RecipeAdapter.ViewHolder>  {

    private List<RecipeModel.Recipes> mData;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    private boolean isDiscountPage;
    private Context mContext;
    private Activity baseActivity;
    private String selectedTab;
    private int red_color;
    private int blue_color;
    private int green_color;
    private ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

    // data is passed into the constructor
    public RecipeAdapter(Context context, List<RecipeModel.Recipes> data, boolean discountPage, String selectedTab) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        this.isDiscountPage = discountPage;
        this.selectedTab = selectedTab;
        this.mContext = context;
        this.baseActivity = (HomeActivity)context;
        red_color = this.mContext.getResources().getColor(R.color.app_red_color);
        blue_color = this.mContext.getResources().getColor(R.color.app_blue_color);
        green_color = this.mContext.getResources().getColor(R.color.app_green_color);
    }

    // inflates the row layout from xml when needed
    @NonNull
    @Override
    public RecipeAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.recipe_item, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final RecipeModel.Recipes recipe = mData.get(position);
        holder.recipe_name.setText(recipe.getRecipeName());
        String price = recipe.getSalePrice();
        price = price == null ? "₹ 0" : ("₹ " + price);
        holder.recipe_price.setText(price);

        String discountPrice = recipe.getDiscountPrice();
        discountPrice = discountPrice == null ? "₹ 0" : ("₹ " + discountPrice);

        if (selectedTab.equals("2")) {
            holder.recipe_discountPrice.setVisibility(View.VISIBLE);
            holder.recipe_discountPrice.setText(discountPrice);
            holder.recipe_price.setPaintFlags(holder.recipe_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        } else {
            holder.recipe_price.setPaintFlags(holder.recipe_price.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
            holder.recipe_discountPrice.setVisibility(View.GONE);
        }

        final String recipeImagePath = recipe.getRecipeImage();

        Picasso.with(mContext).load(recipeImagePath)
                .placeholder(R.drawable.food_ph)
                .error(R.drawable.food_ph)
                .into(holder.recipe_imageView);
        String cal = recipe.getCalorieskcal() == null ? "0 Cal" : String.valueOf(recipe.getCalorieskcal()) + " Cal";
        holder.recipe_calorie.setText(cal);

        if (recipe.getCartId() != null)
        {
            holder.addCartLayout.setVisibility(View.GONE);
            holder.addMoreToCartLayout.setVisibility(View.VISIBLE);
            int quantity = recipe.getCartqty();
            String quant = quantity > 1 ? quantity + " servings" : quantity + " serving";
            holder.recipeCartQuantity.setText(quant);
        }
        else {
            holder.addCartLayout.setVisibility(View.VISIBLE);
            holder.addMoreToCartLayout.setVisibility(View.GONE);
        }

        holder.recipeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent recipeDetail = new Intent(mContext.getApplicationContext(), RecipeDetailedActivity.class);
                recipeDetail.putExtra("recipeID", recipe.getRecipeID().trim());
                recipeDetail.putExtra("tab", selectedTab.trim());
                mContext.startActivity(recipeDetail);
            }
        });
        holder.addIngredients.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String cost = recipe.getSalePrice();
                if (selectedTab.equals("2")) {
                    cost = recipe.getDiscountPrice();
                }
                UtilsClass.showBusyAnimation(baseActivity, "");
                Call<StatusModel> call = apiService.addItemsToCart(SharedPreferenceUtility.getUserId(mContext), recipe.getRecipeID(), cost, "1", "2", "2", "","","1");

                call.enqueue(new Callback<StatusModel>() {
                    @Override
                    public void onResponse(Call<StatusModel> call, final Response<StatusModel> response) {
                        UtilsClass.hideBusyAnimation(baseActivity);
                        if (response.body() != null) {
                            if (response.body().getStatus()) {
                                // change the button background
                                /*holder.addIngredients.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                                holder.addIngredients.setText("");
                                holder.addIngredients.setBackground(mContext.getResources().getDrawable(R.drawable.cart_added));*/

                                holder.addCartLayout.setVisibility(View.GONE);
                                holder.addMoreToCartLayout.setVisibility(View.VISIBLE);

                                String quantity = "1 serving";
                                recipe.setCartqty(1);
                                holder.recipeCartQuantity.setText(quantity);

                            } else {
                                // retain the button background
                                holder.addCartLayout.setVisibility(View.VISIBLE);
                                holder.addMoreToCartLayout.setVisibility(View.GONE);
                                Log.d(RecipeAdapter.class.getSimpleName(), "Response: " + response.body().getMessage());
                            }

                        } else {
                            // retain the button background
                            holder.addCartLayout.setVisibility(View.VISIBLE);
                            holder.addMoreToCartLayout.setVisibility(View.GONE);
                            Log.d(RecipeAdapter.class.getSimpleName(), "Response: " + response.raw().message());
                        }
                    }

                    @Override
                    public void onFailure(Call<StatusModel> call, Throwable t) {
                        UtilsClass.hideBusyAnimation(baseActivity);
                        // retain the button background
                        holder.addCartLayout.setVisibility(View.VISIBLE);
                        holder.addMoreToCartLayout.setVisibility(View.GONE);
                        Log.d(RecipeAdapter.class.getSimpleName(), "Response: " + "Something went wrong!!!!");

                    }
                });
            }
        });

        holder.decreaseCartQuantity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String cost = recipe.getSalePrice();
                if (selectedTab.equals("2")) {
                    cost = recipe.getDiscountPrice();
                }
                final int quantity = recipe.getCartqty() - 1;

                if (quantity != 0) {

                    String cartId = recipe.getCartId() == null ? "" : recipe.getCartId();
                    UtilsClass.showBusyAnimation(baseActivity, "");
                    Call<StatusModel> call = apiService.addItemsToCart(SharedPreferenceUtility.getUserId(mContext), recipe.getRecipeID(), cost, String.valueOf(quantity), "2", "2", "", cartId, "0");

                    call.enqueue(new Callback<StatusModel>() {
                        @Override
                        public void onResponse(Call<StatusModel> call, final Response<StatusModel> response) {
                            UtilsClass.hideBusyAnimation(baseActivity);
                            if (response.body() != null) {
                                if (response.body().getStatus()) {

                                    recipe.setCartqty(quantity);

                                    if (quantity == 0) {
                                        holder.addCartLayout.setVisibility(View.VISIBLE);
                                        holder.addMoreToCartLayout.setVisibility(View.GONE);
                                    } else {
                                        String quant = quantity > 1 ? quantity + " servings" : quantity + " serving";
                                        holder.recipeCartQuantity.setText(quant);
                                    }

                                } else {
                                    // retain the button background
                                    Log.d(RecipeAdapter.class.getSimpleName(), "Response: " + response.body().getMessage());
                                }

                            } else {
                                // retain the button background
                                Log.d(RecipeAdapter.class.getSimpleName(), "Response: " + response.raw().message());
                            }
                        }

                        @Override
                        public void onFailure(Call<StatusModel> call, Throwable t) {
                            UtilsClass.hideBusyAnimation(baseActivity);
                            // retain the button background
                            Log.d(RecipeAdapter.class.getSimpleName(), "Response: " + "Something went wrong!!!!");

                        }
                    });
                }
            }
        });

        holder.increaseCartQuantity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String cost = recipe.getSalePrice();
                if (selectedTab.equals("2")) {
                    cost = recipe.getDiscountPrice();
                }
                final int quantity = recipe.getCartqty() + 1;

                String cartId = recipe.getCartId() == null ? "" : recipe.getCartId();
                UtilsClass.showBusyAnimation(baseActivity,"");
                Call<StatusModel> call = apiService.addItemsToCart(SharedPreferenceUtility.getUserId(mContext), recipe.getRecipeID(), cost, String.valueOf(quantity), "2", "2", "", cartId,"1");

                call.enqueue(new Callback<StatusModel>() {
                    @Override
                    public void onResponse(Call<StatusModel> call, final Response<StatusModel> response) {
                        UtilsClass.hideBusyAnimation(baseActivity);
                        if (response.body() != null) {
                            if (response.body().getStatus()) {

                                recipe.setCartqty(quantity);
                                String quant = quantity > 1 ? quantity + " servings" : quantity + " serving";
                                holder.recipeCartQuantity.setText(quant);

                            } else {
                                // retain the button background
                                Log.d(RecipeAdapter.class.getSimpleName(), "Response: " + response.body().getMessage());
                            }

                        } else {
                            // retain the button background
                            Log.d(RecipeAdapter.class.getSimpleName(), "Response: " + response.raw().message());
                        }
                    }

                    @Override
                    public void onFailure(Call<StatusModel> call, Throwable t) {
                        UtilsClass.hideBusyAnimation(baseActivity);
                        // retain the button background
                        Log.d(RecipeAdapter.class.getSimpleName(), "Response: " + "Something went wrong!!!!");

                    }
                });
            }
        });

        if (recipe.getFavID() != null) {
            holder.favouriteBtn.setBackground(mContext.getResources().getDrawable(R.drawable.ic_fav_sel));
        } else {
            holder.favouriteBtn.setBackground(mContext.getResources().getDrawable(R.drawable.ic_fav_unsel));
        }

        holder.favouriteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /*
                 * 0 - unfav
                 * 1 - fav
                 * */

                String isFav = recipe.getFavID() != null ? "0" : "1";

                /*if (recipe.getFavID()!= null) {
                    holder.favouriteBtn.setBackground(mContext.getResources().getDrawable(R.drawable.ic_fav_unsel));
                } else {
                    holder.favouriteBtn.setBackground(mContext.getResources().getDrawable(R.drawable.ic_fav_sel));
                }*/
                UtilsClass.showBusyAnimation(baseActivity,"");
                Call<StatusModel> call = apiService.makeFavorite(SharedPreferenceUtility.getUserId(mContext), recipe.getRecipeID(), "2", isFav);

                call.enqueue(new Callback<StatusModel>() {
                    @Override
                    public void onResponse(Call<StatusModel> call, final Response<StatusModel> response) {
                        UtilsClass.hideBusyAnimation(baseActivity);
                        if (response.body() != null) {
                            if (response.body().getStatus()) {
                                // change the button background
                                if (recipe.getFavID() == null) {
                                    recipe.setFavID("2");
                                    holder.favouriteBtn.setBackground(mContext.getResources().getDrawable(R.drawable.ic_fav_sel));
                                } else {
                                    recipe.setFavID(null);
                                    holder.favouriteBtn.setBackground(mContext.getResources().getDrawable(R.drawable.ic_fav_unsel));
                                }

                            } else {
                                if (recipe.getFavID() == null) {
                                    holder.favouriteBtn.setBackground(mContext.getResources().getDrawable(R.drawable.ic_fav_unsel));
                                } else {
                                    holder.favouriteBtn.setBackground(mContext.getResources().getDrawable(R.drawable.ic_fav_sel));
                                }
                                Log.d(RecipeAdapter.class.getSimpleName(), "Response: " + response.body().getMessage());

                            }

                        } else {
                            if (recipe.getFavID() == null) {
                                holder.favouriteBtn.setBackground(mContext.getResources().getDrawable(R.drawable.ic_fav_unsel));
                            } else {
                                holder.favouriteBtn.setBackground(mContext.getResources().getDrawable(R.drawable.ic_fav_sel));
                            }

                            Log.d(RecipeAdapter.class.getSimpleName(), "Response: " + response.raw().message());
                        }
                    }

                    @Override
                    public void onFailure(Call<StatusModel> call, Throwable t) {
                        UtilsClass.hideBusyAnimation(baseActivity);
                        if (recipe.getFavID() == null) {
                            holder.favouriteBtn.setBackground(mContext.getResources().getDrawable(R.drawable.ic_fav_unsel));
                        } else {
                            holder.favouriteBtn.setBackground(mContext.getResources().getDrawable(R.drawable.ic_fav_sel));
                        }
                        Log.d(RecipeAdapter.class.getSimpleName(), "Response: " + "Something went wrong!!!!");

                    }
                });
            }
        });


    }


    // total number of rows
    @Override
    public int getItemCount() {
        return mData.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView recipe_imageView;
        Button favouriteBtn;
        TextView recipe_name;
        TextView recipe_calorie;
        TextView recipe_discountPrice;
        TextView recipe_price;
        Button addIngredients;
        TextView recipe_seperatorView;
        LinearLayout addCartLayout;
        LinearLayout addMoreToCartLayout;
        RelativeLayout recipeLayout;
        TextView recipeCartQuantity;
        TextView increaseCartQuantity;
        TextView decreaseCartQuantity;

        ViewHolder(View itemView) {
            super(itemView);
            recipe_imageView = itemView.findViewById(R.id.recipe_imageView);
            favouriteBtn = itemView.findViewById(R.id.favouriteBtn);
            recipe_name = itemView.findViewById(R.id.recipe_nameView);
            recipe_calorie = itemView.findViewById(R.id.recipe_calorieView);
            recipe_discountPrice = itemView.findViewById(R.id.recipe_discountcostView);
            recipe_price = itemView.findViewById(R.id.recipe_costView);
            recipe_seperatorView = itemView.findViewById(R.id.recipe_seperatorView);
            addIngredients = itemView.findViewById(R.id.addIngredientsBtn);
            addCartLayout = itemView.findViewById(R.id.addCartLayout);
            addMoreToCartLayout = itemView.findViewById(R.id.addMoreToCartLayout);
            recipeCartQuantity = itemView.findViewById(R.id.recipeCartQuantity);
            decreaseCartQuantity = itemView.findViewById(R.id.decrementRecipeOrder);
            increaseCartQuantity = itemView.findViewById(R.id.incrementRecipeOrder);

            int setColor = blue_color;

            if (selectedTab.equals("0") || selectedTab.equals("-1")) {
                setColor = blue_color;
                addIngredients.setBackground(mContext.getDrawable(R.drawable.add_cart_blue));
                addMoreToCartLayout.setBackground(mContext.getDrawable(R.drawable.add_cart_blue));
                // blue color
            } else if (selectedTab.equals("1")) {
                // green color
                setColor = green_color;
                addIngredients.setBackground(mContext.getDrawable(R.drawable.add_cart_green));
                addMoreToCartLayout.setBackground(mContext.getDrawable(R.drawable.add_cart_green));
            } else {
                // red color
                setColor = red_color;
                addIngredients.setBackground(mContext.getDrawable(R.drawable.add_cart_red));
                addMoreToCartLayout.setBackground(mContext.getDrawable(R.drawable.add_cart_red));
            }

            recipe_calorie.setTextColor(setColor);
            recipe_price.setTextColor(setColor);
            recipe_discountPrice.setTextColor(setColor);
            recipe_seperatorView.setTextColor(setColor);

            recipeLayout = itemView.findViewById(R.id.recipeLayout);
            recipe_imageView.setClipToOutline(true);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    // convenience method for getting data at click position
    RecipeModel.Recipes getItem(int id) {
        return mData.get(id);
    }

    // allows clicks events to be caught
    void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}
