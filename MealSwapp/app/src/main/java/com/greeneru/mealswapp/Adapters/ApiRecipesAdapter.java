package com.greeneru.mealswapp.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.greeneru.mealswapp.HomeActivity;
import com.greeneru.mealswapp.Models.RecipeModel;
import com.greeneru.mealswapp.Models.StatusModel;
import com.greeneru.mealswapp.R;
import com.greeneru.mealswapp.RecipeDetailedActivity;
import com.greeneru.mealswapp.Retrofit.ApiClient;
import com.greeneru.mealswapp.Retrofit.ApiInterface;
import com.greeneru.mealswapp.Utilities.SharedPreferenceUtility;
import com.greeneru.mealswapp.UtilsClass;
import com.squareup.picasso.Picasso;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ApiRecipesAdapter extends RecyclerView.Adapter<ApiRecipesAdapter.ViewHolder> {

    private List<RecipeModel.ApiRecipes> mData;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    private boolean isDiscountPage;
    private Context mContext;
    private Activity baseActivity;
    private ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

    // data is passed into the constructor
    public ApiRecipesAdapter(Context context, List<RecipeModel.ApiRecipes> data, boolean discountPage) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        this.isDiscountPage = discountPage;
        this.mContext = context;
        this.baseActivity = (HomeActivity)context;
    }

    // inflates the row layout from xml when needed
    @NonNull
    @Override
    public ApiRecipesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.search_recipe_item, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final RecipeModel.ApiRecipes recipe = mData.get(position);
        holder.recipe_name.setText(recipe.getRecipeName());
        String price = "₹ " + String.valueOf(recipe.getPrice());
        holder.recipe_price.setText(price);

        if (isDiscountPage)
        {
            holder.recipe_discountPrice.setVisibility(View.VISIBLE);
            holder.recipe_discountPrice.setText(String.valueOf(10));
        }else {
            holder.recipe_discountPrice.setVisibility(View.GONE);
        }

        final String recipeImagePath = recipe.getRecipeImage();

        Picasso.with(mContext).load(recipeImagePath)
                .placeholder(R.drawable.food_ph)
                .error(R.drawable.food_ph)
                .into(holder.recipe_imageView);
        String cal = recipe.getRecipeCalories() == null ? "0 Cal" : String.valueOf(recipe.getRecipeCalories()) + " Cal";
        holder.recipe_calorie.setText(cal);

        if (recipe.getCartId() != null)
        {
            holder.addCartLayout.setVisibility(View.GONE);
            holder.addMoreToCartLayout.setVisibility(View.VISIBLE);
            int quantity = recipe.getCartqty();
            String quant = quantity > 1 ? quantity + " servings" : quantity + " serving";
            holder.recipeCartQuantity.setText(quant);
        }
        else {
            holder.addCartLayout.setVisibility(View.VISIBLE);
            holder.addMoreToCartLayout.setVisibility(View.GONE);
        }


        holder.recipeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent recipeDetail = new Intent(mContext.getApplicationContext(), RecipeDetailedActivity.class);
                recipeDetail.putExtra("recipeID",recipe.getID().trim());
                recipeDetail.putExtra("source","api");
                recipeDetail.putExtra("tab","0");
                mContext.startActivity(recipeDetail);
            }
        });
        if (recipe.getFavID()!=null)
        {
            holder.favouriteBtn.setBackground(mContext.getResources().getDrawable(R.drawable.ic_fav_sel));
        }
        else
        {
            holder.favouriteBtn.setBackground(mContext.getResources().getDrawable(R.drawable.ic_fav_unsel));
        }

        holder. addIngredientsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                UtilsClass.showBusyAnimation(baseActivity,"");
                Call<StatusModel> call = apiService.addItemsToCart(SharedPreferenceUtility.getUserId(mContext),recipe.getID(),recipe.getPrice(),"1","3","2","","","1");

                call.enqueue(new Callback<StatusModel>() {
                    @Override
                    public void onResponse(Call<StatusModel> call, final Response<StatusModel> response) {
                        UtilsClass.hideBusyAnimation(baseActivity);
                        if (response.body() != null) {
                            if (response.body().getStatus())
                            {
                                // change the button background
                                holder.addCartLayout.setVisibility(View.GONE);
                                holder.addMoreToCartLayout.setVisibility(View.VISIBLE);

                                String quantity = "1 serving";
                                recipe.setCartqty(1);
                                holder.recipeCartQuantity.setText(quantity);
                            }
                            else {
                                // retain the button background
                                holder.addCartLayout.setVisibility(View.VISIBLE);
                                holder.addMoreToCartLayout.setVisibility(View.GONE);
                                Log.d(RecipeAdapter.class.getSimpleName(), "Response: " + response.body().getMessage());
                            }

                        } else {
                            // retain the button background
                            holder.addCartLayout.setVisibility(View.VISIBLE);
                            holder.addMoreToCartLayout.setVisibility(View.GONE);
                            Log.d(RecipeAdapter.class.getSimpleName(), "Response: " + response.raw().message());
                        }
                    }

                    @Override
                    public void onFailure(Call<StatusModel> call, Throwable t) {
                        UtilsClass.hideBusyAnimation(baseActivity);
                        // retain the button background
                        holder.addCartLayout.setVisibility(View.VISIBLE);
                        holder.addMoreToCartLayout.setVisibility(View.GONE);
                        Log.d(RecipeAdapter.class.getSimpleName(), "Response: " + "Something went wrong!!!!");

                    }
                });

            }
        });


        holder.decreaseCartQuantity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final int quantity = recipe.getCartqty() - 1;

                if (quantity != 0) {
                    String cartId = recipe.getCartId() == null ? "" : recipe.getCartId();
                    UtilsClass.showBusyAnimation(baseActivity,"");
                    Call<StatusModel> call = apiService.addItemsToCart(SharedPreferenceUtility.getUserId(mContext), recipe.getID(), recipe.getPrice(), String.valueOf(quantity), "3", "2", "", cartId, "0");

                    call.enqueue(new Callback<StatusModel>() {
                        @Override
                        public void onResponse(Call<StatusModel> call, final Response<StatusModel> response) {
                            UtilsClass.hideBusyAnimation(baseActivity);
                            if (response.body() != null) {
                                if (response.body().getStatus()) {

                                    recipe.setCartqty(quantity);

                                    if (quantity == 0) {
                                        holder.addCartLayout.setVisibility(View.VISIBLE);
                                        holder.addMoreToCartLayout.setVisibility(View.GONE);
                                    } else {
                                        String quant = quantity > 1 ? quantity + " servings" : quantity + " serving";
                                        holder.recipeCartQuantity.setText(quant);
                                    }

                                } else {
                                    // retain the button background
                                    Log.d(RecipeAdapter.class.getSimpleName(), "Response: " + response.body().getMessage());
                                }

                            } else {
                                // retain the button background
                                Log.d(RecipeAdapter.class.getSimpleName(), "Response: " + response.raw().message());
                            }
                        }

                        @Override
                        public void onFailure(Call<StatusModel> call, Throwable t) {
                            UtilsClass.hideBusyAnimation(baseActivity);
                            // retain the button background
                            Log.d(RecipeAdapter.class.getSimpleName(), "Response: " + "Something went wrong!!!!");

                        }
                    });
                }
            }
        });

        holder.increaseCartQuantity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final int quantity = recipe.getCartqty() + 1;

                String cartId = recipe.getCartId() == null ? "" : recipe.getCartId();
                UtilsClass.showBusyAnimation(baseActivity,"");
                Call<StatusModel> call = apiService.addItemsToCart(SharedPreferenceUtility.getUserId(mContext), recipe.getID(), recipe.getPrice(), String.valueOf(quantity), "3", "2", "", cartId,"1");

                call.enqueue(new Callback<StatusModel>() {
                    @Override
                    public void onResponse(Call<StatusModel> call, final Response<StatusModel> response) {
                        UtilsClass.hideBusyAnimation(baseActivity);
                        if (response.body() != null) {
                            if (response.body().getStatus()) {

                                recipe.setCartqty(quantity);
                                String quant = quantity > 1 ? quantity + " servings" : quantity + " serving";
                                holder.recipeCartQuantity.setText(quant);

                            } else {
                                // retain the button background
                                Log.d(RecipeAdapter.class.getSimpleName(), "Response: " + response.body().getMessage());
                            }

                        } else {
                            // retain the button background
                            Log.d(RecipeAdapter.class.getSimpleName(), "Response: " + response.raw().message());
                        }
                    }

                    @Override
                    public void onFailure(Call<StatusModel> call, Throwable t) {
                        UtilsClass.hideBusyAnimation(baseActivity);
                        // retain the button background
                        Log.d(RecipeAdapter.class.getSimpleName(), "Response: " + "Something went wrong!!!!");

                    }
                });
            }
        });

        holder.favouriteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*
                 * 0 - unfav
                 * 1 - fav
                 * */

                String isFav = recipe.getFavID()!=null?"0":"1";

                /*if (recipe.getFavID()!= null) {
                    holder.favouriteBtn.setBackground(mContext.getResources().getDrawable(R.drawable.ic_fav_unsel));
                } else {
                    holder.favouriteBtn.setBackground(mContext.getResources().getDrawable(R.drawable.ic_fav_sel));
                }*/
                UtilsClass.showBusyAnimation(baseActivity,"");
                Call<StatusModel> call = apiService.makeFavorite(SharedPreferenceUtility.getUserId(mContext),recipe.getID(),"3", isFav);

                call.enqueue(new Callback<StatusModel>() {
                    @Override
                    public void onResponse(Call<StatusModel> call, final Response<StatusModel> response) {
                        UtilsClass.hideBusyAnimation(baseActivity);
                        if (response.body() != null) {
                            if (response.body().getStatus())
                            {
                                if (recipe.getFavID() == null){
                                    recipe.setFavID("2");
                                    holder.favouriteBtn.setBackground(mContext.getResources().getDrawable(R.drawable.ic_fav_sel));
                                }
                                else {
                                    recipe.setFavID(null);
                                    holder.favouriteBtn.setBackground(mContext.getResources().getDrawable(R.drawable.ic_fav_unsel));
                                }
                            }
                            else {
                                if (recipe.getFavID() == null){
                                    holder.favouriteBtn.setBackground(mContext.getResources().getDrawable(R.drawable.ic_fav_unsel));
                                }
                                else {
                                    holder.favouriteBtn.setBackground(mContext.getResources().getDrawable(R.drawable.ic_fav_sel));
                                }
                                Log.d(RecipeAdapter.class.getSimpleName(), "Response: " + response.body().getMessage());
                            }

                        } else {
                            if (recipe.getFavID() == null){
                                holder.favouriteBtn.setBackground(mContext.getResources().getDrawable(R.drawable.ic_fav_unsel));
                            }
                            else {
                                holder.favouriteBtn.setBackground(mContext.getResources().getDrawable(R.drawable.ic_fav_sel));
                            }

                            Log.d(RecipeAdapter.class.getSimpleName(), "Response: " + response.raw().message());
                        }
                    }

                    @Override
                    public void onFailure(Call<StatusModel> call, Throwable t) {
                        UtilsClass.hideBusyAnimation(baseActivity);
                        if (recipe.getFavID() == null){
                            holder.favouriteBtn.setBackground(mContext.getResources().getDrawable(R.drawable.ic_fav_unsel));
                        }
                        else {
                            holder.favouriteBtn.setBackground(mContext.getResources().getDrawable(R.drawable.ic_fav_sel));
                        }
                        Log.d(RecipeAdapter.class.getSimpleName(), "Response: " + "Something went wrong!!!!");

                    }
                });
            }
        });
    }


    // total number of rows
    @Override
    public int getItemCount() {
        return mData.size();
    }

    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView recipe_imageView;
        Button favouriteBtn;
        TextView recipe_name;
        TextView recipe_calorie;
        TextView recipe_discountPrice;
        Button addIngredientsBtn;
        TextView recipe_price;
        LinearLayout addIngredients;
        LinearLayout addCartLayout;
        LinearLayout addMoreToCartLayout;
        RelativeLayout recipeLayout;
        TextView recipeCartQuantity;
        TextView increaseCartQuantity;
        TextView decreaseCartQuantity;

        ViewHolder(View itemView) {
            super(itemView);
            recipe_imageView = itemView.findViewById(R.id.recipe_imageView);
            favouriteBtn= itemView.findViewById(R.id.favouriteBtn);
            recipe_name = itemView.findViewById(R.id.recipe_nameView);
            recipe_calorie= itemView.findViewById(R.id.recipe_calorieView);
            recipe_discountPrice = itemView.findViewById(R.id.recipe_discountcostView);
            recipe_price= itemView.findViewById(R.id.recipe_costView);
            addIngredients = itemView.findViewById(R.id.addCartLayout);
            recipeLayout= itemView.findViewById(R.id.recipeLayout);
            addIngredientsBtn = itemView.findViewById(R.id.addIngredientsBtn);
            addCartLayout = itemView.findViewById(R.id.addCartLayout);
            addMoreToCartLayout = itemView.findViewById(R.id.addMoreToCartLayout);
            recipeCartQuantity = itemView.findViewById(R.id.recipeCartQuantity);
            decreaseCartQuantity = itemView.findViewById(R.id.decrementRecipeOrder);
            increaseCartQuantity = itemView.findViewById(R.id.incrementRecipeOrder);

            recipe_imageView.setClipToOutline(true);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    // convenience method for getting data at click position
    RecipeModel.ApiRecipes getItem(int id) {
        return mData.get(id);
    }

    // allows clicks events to be caught
    void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}
