package com.greeneru.mealswapp.Models;

public class RecipeDummyModel {

        public String recipe_id;
        public String recipe_name;
        public int recipe_image;
        public String recipe_price;
        public String recipe_discountPrice;
        public String recipe_calories;
        public boolean isFavourite;

}
