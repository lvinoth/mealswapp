package com.greeneru.mealswapp.Utilities;

import android.content.Context;
import android.content.SharedPreferences;

import com.greeneru.mealswapp.R;

public class SharedPreferenceUtility {

    private static SharedPreferences sharedPreferences;


    public static SharedPreferences getSharedPreferenceInstance(Context context) {
        if (sharedPreferences == null)
            sharedPreferences = context.getSharedPreferences(
                    context.getString(R.string.USER_PREFERENCES),
                    Context.MODE_PRIVATE);

        return sharedPreferences;
    }

    //* SAVING USER ID & TOKEN *//
    public static void saveUserLoginDetails(Context context, String userID, String token) {
        SharedPreferences.Editor sharedPreferencesEditor = getSharedPreferenceInstance(context).edit();

        sharedPreferencesEditor.putString("user_id", userID);
        sharedPreferencesEditor.putString("user_token", token);
        sharedPreferencesEditor.commit();
    }

    public static void saveUserGoalId(Context context, String goalId) {
        SharedPreferences.Editor sharedPreferencesEditor = getSharedPreferenceInstance(context).edit();

        sharedPreferencesEditor.putString("goal_id", goalId);
        sharedPreferencesEditor.commit();
    }

    public static void saveUserGoal(Context context, String goal) {
        SharedPreferences.Editor sharedPreferencesEditor = getSharedPreferenceInstance(context).edit();

        sharedPreferencesEditor.putString("user_goal", goal);
        sharedPreferencesEditor.commit();
    }

    public static void saveFCMRegID(Context context, String regID) {
        SharedPreferences.Editor sharedPreferencesEditor = getSharedPreferenceInstance(context).edit();
        sharedPreferencesEditor.putString("device_id", regID);
        sharedPreferencesEditor.commit();
    }

    public static void savePreferencesForLogout(Context context) {

        SharedPreferences.Editor sharedPreferencesEditor = getSharedPreferenceInstance(context).edit();
        sharedPreferencesEditor.remove("user_id");
        sharedPreferencesEditor.remove("user_token");
        sharedPreferencesEditor.remove("user_name");
        sharedPreferencesEditor.remove("user_city");
        sharedPreferencesEditor.remove("user_country");
        sharedPreferencesEditor.remove("user_image");

        sharedPreferencesEditor.commit();
    }

    public static String getUserGoal(Context context)
    {
        return getSharedPreferenceInstance(context).getString("user_goal", "");
    }



    public static String getUserId(Context context) {

        return getSharedPreferenceInstance(context).getString("user_id", "EMPTY");
    }

    public static String getGoalId(Context context) {

        return getSharedPreferenceInstance(context).getString("goal_id", "-1");
    }


    public static String getUSerToken(Context context) {

        return getSharedPreferenceInstance(context).getString("user_token", "EMPTY");
    }

    public static String getFCMToken(Context context) {
        return getSharedPreferenceInstance(context).getString("device_id", "");
    }

}
