package com.greeneru.mealswapp.Retrofit;

import com.greeneru.mealswapp.Models.ApiRecipeDetailModel;
import com.greeneru.mealswapp.Models.CartModel;
import com.greeneru.mealswapp.Models.CategoryModel;
import com.greeneru.mealswapp.Models.CuisineModel;
import com.greeneru.mealswapp.Models.ProductModel;
import com.greeneru.mealswapp.Models.RecipeDetailsModel;
import com.greeneru.mealswapp.Models.RecipeModel;
import com.greeneru.mealswapp.Models.StatusModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ApiInterface {

    @FormUrlEncoded
    @POST("api/signup")
    Call<StatusModel> doSignUpEmail(@Field("firstName") String firstName, @Field("lastName") String lastName,
                                    @Field("email") String email, @Field("password") String password,
                                    @Field("source") String source, @Field("tm") String time,
                                    @Field("token") String token, @Field("goalid") String goalId);


    @FormUrlEncoded
    @POST("api/login")
    Call<StatusModel> doSignIn(@Field("username") String userName, @Field("password") String password,
                               @Field("tm") String time, @Field("token") String token);

    @GET("recipes/getRecipesList")
    Call<RecipeModel> getRecipesList(@Query("userId") String userId, @Query("meal") String meal,
                                     @Query("foodType") String foodType, @Query("cuisine") String cuisine,
                                     @Query("srch") String searchText, @Query("tablist") String tab);

    @GET("recipes/getRecipeDetail")
    Call<RecipeDetailsModel> getRecipesDetail(@Query("userId") String userId, @Query("id") String recipeId);

    @GET("recipes/getApiRecipeDetail")
    Call<ApiRecipeDetailModel> getApiRecipesDetail(@Query("userId") String userId, @Query("id") String recipeId);

    @GET("masters/cuisines")
    Call<CuisineModel> getCuisinesList(@Query("userId") String userId);

    @GET("product/products")
    Call<ProductModel> getProductsList(@Query("userId") String userId, @Query("cat_id") String categoryId,
                                       @Query("search_by") String searchString, @Query("tablist") String tablist);

    @GET("masters/categories")
    Call<CategoryModel> getCategoriesList(@Query("userId") String userId);

    @FormUrlEncoded
    @POST("product/addToFavorite")
    Call<StatusModel> makeFavorite(@Field("userId") String userId, @Field("rpId") String recipeId,
                                     @Field("isProduct") String isProduct, @Field("isFavourite") String isFavourite);

    /*
    * userId - user Id
    * recipeId - recipe/product id
    * Price - recipe/product price
    * qty - recipe/product quantity. No of servings for recipe/apirecipe. actual quantity for products.
    * cartType - "1 = product", "2 - recipe", "3 - apirecipe"
    * addDetail - "1 - added from detailed view","2 - added from outside list"
    * objListString - list of ingredients with quantity and id, if added from detailed view.
    * cartId - cartId for this recipe/product if ordered from detailed view.
    * addOrRemove - 0 - minus, 1 - add or remove the recipe/ product
    * */
    @FormUrlEncoded
    @POST("order/addtoCartRecipe")
    Call<StatusModel> addItemsToCart(@Field("userId") String userId, @Field("recipeId") String recipeId,
                                     @Field("Price") String Price, @Field("qty") String qty,
                                     @Field("cartType") String cartType, @Field("addDetail") String addDetail,
                                     @Field("objListString") String objListString, @Field("cartId") String cartId,
                                     @Field("addOrRemove") String addOrRemove);

    /*
     * recipeId - recipe/product id
     * Price - recipe/product price
     * qty - recipe/product quantity. empty for recipe/apirecipe. actual quantity for products.
     * cartType - "1 = product", "2 - recipe", "3 - apirecipe"
     * addDetail - "1 - added from detailed view","2 - added from outside list"
     * objListString - list of ingredients with quantity and id, if added from detailed view.
     * */
    @FormUrlEncoded
    @POST("order/updatecartCartRecipe")
    Call<StatusModel> updateItemsInCart(@Field("userId") String userId, @Field("recipeId") String recipeId,
                                     @Field("Price") String Price, @Field("qty") String qty,
                                     @Field("cartType") String cartType, @Field("addDetail") String addDetail,
                                     @Field("objListString") String objListString);

    /*
     * recipeId - recipe/product id
     * Price - recipe/product price
     * qty - recipe/product quantity. empty for recipe/apirecipe. actual quantity for products.
     * ingredientId - ingredient Id
     * addOrRemove - 0 - minus, 1 - add or remove the recipe/ product
     * */
    @FormUrlEncoded
    @POST("order/updateIngredientQty")
    Call<StatusModel> updateIngredientQty(@Field("userId") String userId, @Field("recipeId") String recipeId,
                                     @Field("Price") String Price, @Field("qty") String qty,
                                     @Field("ingredientId") String ingredientId, @Field("ingredient_Id") String ingredient_Id,
                                     @Field("addOrRemove") String addOrRemove);


    @GET("order/CartRecipes")
    Call<CartModel> getItemsInCart(@Query("userId") String userId);



    @FormUrlEncoded
    @POST("order/placeOrder")
    Call<StatusModel> placeOrder(@Field("userId") String userId);

}
