package com.greeneru.mealswapp;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.greeneru.mealswapp.Models.StatusModel;
import com.greeneru.mealswapp.Retrofit.ApiClient;
import com.greeneru.mealswapp.Retrofit.ApiInterface;
import com.greeneru.mealswapp.Utilities.SharedPreferenceUtility;

import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.widget.Toast.LENGTH_LONG;

public class LoginActivity extends AppCompatActivity {

    private ActionBar actionBar;
    private Toolbar toolbar;
    private Button emailSignInButton;
    private ApiInterface apiService;

    private EditText email;
    private EditText password;
    private Activity baseActivity;
    private TextView forgotPassword;

    private static final String TAG = LoginActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        baseActivity = this;
        UtilsClass.makeStatusBarTransparent(getWindow(), this);
        toolbar = findViewById(R.id.toolbar_signup);
        setSupportActionBar(toolbar);
        final TextView view = findViewById(R.id.toolbarTitleView);
        view.setText("LOG INTO YOUR ACCOUNT");

        actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);

        email = findViewById(R.id.emailTextLogin);
        password = findViewById(R.id.passwordTextLogin);
        emailSignInButton = findViewById(R.id.emailSignIn);
        forgotPassword = findViewById(R.id.forgotPassword);
        forgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(baseActivity, ForgotPasswordActivity.class));
            }
        });

        apiService = ApiClient.getClient().create(ApiInterface.class);


        emailSignInButton.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View v) {

                // validate username/ password
                String emailText = email.getText().toString();
                String passwordText = password.getText().toString();

                if (TextUtils.isEmpty(emailText)) {
                    showToastAtCentre("Please enter your email", LENGTH_LONG);
                    return;
                }
                if (TextUtils.isEmpty(passwordText)) {
                    showToastAtCentre("Please enter password", LENGTH_LONG);
                    return;
                }
                if (!UtilsClass.isValidEmail(emailText))
                {
                    showToastAtCentre("Please enter a valid email", LENGTH_LONG);
                    return;
                }

                doSignIn(emailText, passwordText);
            }

        });

    }


    private Toast mToast;

    private void showToastAtCentre(String message, int duration) {
        if (mToast != null) {
            mToast.cancel();
            mToast = null;
        }
        mToast = Toast.makeText(getApplicationContext(), message, duration);
        mToast.setGravity(Gravity.CENTER, 0, 0);
        mToast.show();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void doSignIn(String userName, String password) {
        UtilsClass.showBusyAnimation(baseActivity, "Logging in..");

        String time = UtilsClass.getSimpleDateTimeFormat().format(new Date());
        Log.d(TAG,"time: " + time);
        String token = UtilsClass.getBase64HashedString(time);

        Call<StatusModel> call = apiService.doSignIn(userName, password, time.trim(), token.trim());

        call.enqueue(new Callback<StatusModel>() {
            @Override
            public void onResponse(Call<StatusModel> call, Response<StatusModel> response) {
                UtilsClass.hideBusyAnimation(baseActivity);
                if (response.body().getStatus()) {
//                    TODO: Vinoth - use UserId for time being, need to replace it with a token mechanism
                    String userId = response.body().getUserId();
                    Log.d(TAG,"UserId: " + userId);
                    SharedPreferenceUtility.saveUserLoginDetails(getApplicationContext(), userId, "");

                    String goalId = response.body().getGoalId();
                    SharedPreferenceUtility.saveUserGoalId(getApplicationContext(),goalId);

                    Intent intent = getIntent();
                    setResult(RESULT_OK, intent);
                    finish();
                } else // Handle failure cases here
                {
                    String mess = response.body().getMessage();
                    Log.d(TAG, "Response: " + mess);
                    showToastAtCentre("Response: " + mess, LENGTH_LONG);
                }

            }

            @Override
            public void onFailure(Call<StatusModel> call, Throwable t) {
                UtilsClass.hideBusyAnimation(baseActivity);
                Log.d(TAG, "Response: " + "Something went wrong!!!!");
                showToastAtCentre("Response: " + t.getMessage(), LENGTH_LONG);
            }
        });
    }
}
