package com.greeneru.mealswapp.Interfaces;

public interface RecipeDataChangeInterface {
    void onRecipeFavourited(int position, String text);
    void onRecipeAddedToCart(int position, String text);
}
