package com.greeneru.mealswapp;


import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.greeneru.mealswapp.Adapters.ApiRecipesAdapter;
import com.greeneru.mealswapp.Adapters.ProductAdapter;
import com.greeneru.mealswapp.Adapters.ProductSearchAdapter;
import com.greeneru.mealswapp.Adapters.RecipeAdapter;
import com.greeneru.mealswapp.Models.ProductModel;
import com.greeneru.mealswapp.Models.RecipeModel;
import com.greeneru.mealswapp.Retrofit.ApiClient;
import com.greeneru.mealswapp.Retrofit.ApiInterface;
import com.greeneru.mealswapp.Utilities.SharedPreferenceUtility;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.widget.Toast.LENGTH_LONG;
import static android.widget.Toast.LENGTH_SHORT;


/**
 * A simple {@link Fragment} subclass.
 */
public class ProductFragment extends Fragment {

    private Adapter initializedAdapter = null;
    private Spinner spinner;
    private SearchView searchView1;
    int black_color = R.color.black;

    private Button category1Btn;
    private Button category2Btn;
    private Button category3Btn;
    private Button category4Btn;

    private TextView selectedCategoryHeader;
    private Toast mToast;
    private TextView noResultsView;

    private TextView recommendedText;
    private TextView trendingText;
    private TextView onDiscountText;

    private TextView searchResultsHeader;
    private TextView searchResultsAlternateHeader;

    private RecyclerView productsRecyclerView;
    private List<ProductModel.Products> productList;
    private ProductAdapter productadapter;

    private RecyclerView searchRecyclerView;
    private List<ProductModel.Products> searchProductList;
    private ProductSearchAdapter searchProductAdapter;

    private RecyclerView recommendedRecyclerView;
    private List<ProductModel.Products> recommendedProductList;
    private ProductAdapter recommendedProductsAdapter;

    private static final String TAG = ProductFragment.class.getSimpleName();
    private ApiInterface apiService;
    private CategoryFilters filters;
    private ImageView categoryFilter;

    private LinearLayout mainLayout;
    private LinearLayout searchLayout;
    private String selectedTab = "0";
    private Activity baseActivity;
    private int red_color;
    private int blue_color;
    private int green_color;
    private int default_color;

    private static final int CATEGORY_ACTIVITY_CODE = 101;


    public List<String> categories = new ArrayList<>();

    List<String> selectedCategories = new ArrayList<>();
    private LinearLayout bottomFiltersLayout;

    public ProductFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_product, container, false);
        baseActivity = (HomeActivity)getActivity();

        red_color = getActivity().getResources().getColor(R.color.app_red_color);
        blue_color = getActivity().getResources().getColor(R.color.app_blue_color);
        green_color = getActivity().getResources().getColor(R.color.app_green_color);
        default_color = getActivity().getResources().getColor(R.color.tab_selected_color);
        filters = new CategoryFilters();
        selectedCategories.clear();

        categories.add(0, "empty");
        categories.add(1,"Drinks");
        categories.add(2,"Bakery");
        categories.add(3,"Dairy");
        categories.add(4,"F&V");
        categories.add(5,"Rice & Grocery");
        categories.add(6,"Macro Snacks");
        categories.add(7,"Staples");
        categories.add(8,"Beverages");
        categories.add(9,"Baking & Dessert  Mixes");
        categories.add(10,"Process Foods");
        categories.add(11,"Instant Foods");

        productList = new ArrayList<>();
        productadapter = new ProductAdapter(getActivity(), productList, false, "-1");
        bottomFiltersLayout = view.findViewById(R.id.bottomFiltersLayout);

        searchProductList = new ArrayList<>();
        searchProductAdapter = new ProductSearchAdapter(getActivity(), searchProductList, false);

        recommendedProductList = new ArrayList<>();
        recommendedProductsAdapter = new ProductAdapter(getActivity(), recommendedProductList, false, "-1");

        searchLayout = view.findViewById(R.id.searchRecipeLayout);
        mainLayout = view.findViewById(R.id.mainLayout);

        spinner = view.findViewById(R.id.sort);
        categoryFilter = view.findViewById(R.id.categoryFilterView);
        categoryFilter.setOnClickListener(categoryFilterListener);

        category1Btn = view.findViewById(R.id.categoryBtn1);
        category2Btn = view.findViewById(R.id.categoryBtn2);
        category3Btn = view.findViewById(R.id.categoryBtn3);
        category4Btn = view.findViewById(R.id.categoryBtn4);

        searchResultsHeader = view.findViewById(R.id.searchResultsHeader);
        searchResultsAlternateHeader = view.findViewById(R.id.searchResultsAlternateHeader);

        recommendedText = view.findViewById(R.id.recommendedViewProd);
        trendingText = view.findViewById(R.id.trendingViewProd);
        onDiscountText = view.findViewById(R.id.discountViewProd);

        category1Btn.setOnClickListener(cuisineSelectedListener);
        category2Btn.setOnClickListener(cuisineSelectedListener);
        category3Btn.setOnClickListener(cuisineSelectedListener);
        category4Btn.setOnClickListener(cuisineSelectedListener);

        recommendedText.setOnClickListener(recommendedListener);
        trendingText.setOnClickListener(recommendedListener);
        onDiscountText.setOnClickListener(recommendedListener);
        selectedCategoryHeader = view.findViewById(R.id.selectedCategory);

        searchView1 = view.findViewById(R.id.searchView1);
        searchView1.setOnQueryTextListener(queryTextListener);
        productsRecyclerView = view.findViewById(R.id.productRecyclerView);
        searchRecyclerView = view.findViewById(R.id.productSearchRecyclerView);
        recommendedRecyclerView = view.findViewById(R.id.productSearchRecommendedRecyclerView);
        noResultsView = view.findViewById(R.id.noResultsView);

        apiService = ApiClient.getClient().create(ApiInterface.class);
        selectedCategoryHeader.setText("Drinks");
        setUpSort();

        return view;
    }


    public void callOnFirstTime()
    {
        setInitialFilters();
        getProductsList(selectedTab);
    }

    public void refreshProducts()
    {
        getProductsList(selectedTab);
    }


    public void setTitleParams()
    {
        int colorToSet = blue_color;
        switch (selectedTab)
        {
            case "0":
                colorToSet = blue_color;
                break;
            case "1":
                colorToSet = green_color;
                break;
            case "2":
                colorToSet = red_color;
                break;
        }

        ((HomeActivity)baseActivity).titleView.setTextColor(colorToSet);
        DrawableCompat.setTint(((HomeActivity)baseActivity).profileView.getDrawable(), colorToSet);
    }


    private void setInitialFilters() {
            filters.setCategory("0");
//            category1Btn.setText(categories.get(1));
//            category1Btn.setBackground(getActivity().getDrawable(R.drawable.cuisine_select));
            category1Btn.setText(categories.get(1));
            category2Btn.setText(categories.get(2));
            category3Btn.setText(categories.get(3));
            category4Btn.setText(categories.get(4));
    }

    private void setUpSort()
    {
        final ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(getActivity(), R.layout.spinner_item, getResources().getStringArray(R.array.sort_types));
        spinner.setAdapter(dataAdapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // This is to avoid the initial fire up while loading this screen. Do not remove
                if( initializedAdapter !=parent.getAdapter() ) {
                    initializedAdapter = parent.getAdapter();
                    return;
                }
                String data = dataAdapter.getItem(position);
                Log.d(TAG, "Sorting Chosen: " + data);
                //showToastAtCentre(data + " used for sorting! ",LENGTH_SHORT);

                if (productList != null && productList.size() > 0)
                {
                    productsRecyclerView.setVisibility(View.VISIBLE);
                    noResultsView.setVisibility(View.INVISIBLE);

                    switch (position)
                    {
                        case 0: // popularity
                            break;
                        case 1: // price low to high
                            Collections.sort(productList, new Comparator<ProductModel.Products>() {
                                @Override
                                public int compare(ProductModel.Products s1, ProductModel.Products s2) {
                                    return Double.valueOf(s1.getSalePrice()).compareTo(Double.valueOf(s2.getSalePrice()));
                                }
                            });
                            break;
                        case 2: // calories low to high
                            Collections.sort(productList, new Comparator<ProductModel.Products>() {
                                @Override
                                public int compare(ProductModel.Products s1, ProductModel.Products s2) {
                                    return Double.valueOf(s1.getCaloriesKcal()).compareTo(Double.valueOf(s2.getCaloriesKcal()));
                                }
                            });
                            break;
                        case 3: // alphabetical
                            Collections.sort(productList, new Comparator<ProductModel.Products>() {
                                @Override
                                public int compare(ProductModel.Products s1, ProductModel.Products s2) {
                                    return s1.getItemName().compareToIgnoreCase(s2.getItemName());
                                }
                            });
                            break;
                        /*case 4: // alphabetical high to low
                            // Sorts alphabetically
                            Collections.sort(productList, new Comparator<ProductModel.Products>() {
                                @Override
                                public int compare(ProductModel.Products s1, ProductModel.Products s2) {
                                    return s2.getItemName().compareToIgnoreCase(s1.getItemName());
                                }
                            });
                            break;*/
                    }

                    productadapter = new ProductAdapter(getActivity(), productList, false, selectedTab);
                    productadapter.notifyDataSetChanged();
                    productsRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
                    productsRecyclerView.setAdapter(productadapter);
                }
                else
                {
                    showToastAtCentre("Products not available..",LENGTH_SHORT);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    private View.OnClickListener categoryFilterListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
//            TODO: Replace cuisine activity with categories activity
            Intent catIntent = new Intent(getActivity(),CuisineActivity.class);
            catIntent.putExtra("source","category");
            startActivityForResult(catIntent, CATEGORY_ACTIVITY_CODE);
            getActivity().overridePendingTransition( R.anim.slide_up, R.anim.slide_down);
        }
    };

    private void showToastAtCentre(String message, int duration) {
        if (mToast != null) {
            mToast.cancel();
            mToast = null;
        }
        mToast = Toast.makeText(getActivity().getApplicationContext(), message, duration);
        mToast.setGravity(Gravity.CENTER, 0, 0);
        mToast.show();
    }

    View.OnClickListener cuisineSelectedListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            int id = v.getId();

            switch (id) {
                case R.id.categoryBtn1:
                    selectCategories(category1Btn);
                    selectedCategoryHeader.setText(category1Btn.getText().toString().trim());
                    break;

                case R.id.categoryBtn2:
                    selectCategories(category2Btn);
                    selectedCategoryHeader.setText(category2Btn.getText().toString().trim());
                    break;

                case R.id.categoryBtn3:
                    selectCategories(category3Btn);
                    selectedCategoryHeader.setText(category3Btn.getText().toString().trim());
                    break;

                case R.id.categoryBtn4:
                    selectCategories(category4Btn);
                    selectedCategoryHeader.setText(category4Btn.getText().toString().trim());
                    break;

                default:

                    break;
            }

        }
    };

    private void selectCategories(Button view) {

        String index = String.valueOf(categories.indexOf(view.getText().toString().trim()));
        StringBuilder selected_category = new StringBuilder();

        if (selectedCategories.contains(index)) {
            view.setBackground(getResources().getDrawable(R.drawable.cuisine_unselect));
            selectedCategories.remove(index);
        } else {
            view.setBackground(getResources().getDrawable(R.drawable.cuisine_select));
            selectedCategories.add(index);
        }


        if (selectedCategories.size() > 0) {
            selected_category.append(selectedCategories.get(0));

            for (int i = 1; i < selectedCategories.size(); i++) {
                selected_category.append(",").append(selectedCategories.get(i));
            }
        }
        else
        {
            selected_category.append("0");
        }

        filters.setCategory(selected_category.toString().trim());
        getProductsList(selectedTab);
    }

    private SearchView.OnQueryTextListener queryTextListener = new SearchView.OnQueryTextListener() {
        @Override
        public boolean onQueryTextSubmit(String query) {
            String searchText = searchView1.getQuery().toString().trim();
            if(!searchText.isEmpty())
            {
                getSearchProductsList(searchText);
            }
            return false;
        }

        @Override
        public boolean onQueryTextChange(String newText) {
            String searchText = searchView1.getQuery().toString().trim();
            int len = searchText.length();


            if (len == 0)
            {
                searchLayout.setVisibility(View.GONE);
                mainLayout.setVisibility(View.VISIBLE);
                bottomFiltersLayout.setVisibility(View.VISIBLE);
            }

            Log.e(TAG,"text count: " + len);
            return false;
        }
    };

    private Drawable getColoredBackground(int drawable, int color)
    {
        Drawable mDrawable = baseActivity.getResources().getDrawable(drawable);
        mDrawable.setColorFilter(new PorterDuffColorFilter(color, PorterDuff.Mode.SRC_IN));
        return  mDrawable;
    }

    View.OnClickListener recommendedListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            int id = v.getId();
            switch (id)
            {
                case R.id.recommendedViewProd:
                    selectedTab = "0";
                    searchView1.setVisibility(View.VISIBLE);
                    ((HomeActivity)baseActivity).setTitleParams(R.color.app_blue_color);

                    recommendedText.setBackground(getColoredBackground(R.drawable.recommend_select, blue_color));
                    recommendedText.setTextColor((Color.parseColor("#ffffff")));

                    trendingText.setBackground(getResources().getDrawable(R.drawable.trend_unselect));
                    trendingText.setTextColor(getResources().getColor(black_color));

                    onDiscountText.setBackground(getResources().getDrawable(R.drawable.discount_unselect));
                    onDiscountText.setTextColor(getResources().getColor(black_color));

                    break;

                case R.id.trendingViewProd:
                    selectedTab = "1";
                    searchView1.setVisibility(View.GONE);
                    ((HomeActivity)baseActivity).setTitleParams(R.color.app_green_color);

                    trendingText.setBackground(getColoredBackground(R.drawable.trend_select, green_color));
                    trendingText.setTextColor((Color.parseColor("#ffffff")));

                    recommendedText.setBackground(getResources().getDrawable(R.drawable.recommend_unselect));
                    recommendedText.setTextColor(getResources().getColor(black_color));

                    onDiscountText.setBackground(getResources().getDrawable(R.drawable.discount_unselect));
                    onDiscountText.setTextColor(getResources().getColor(black_color));
                    break;

                case R.id.discountViewProd:
                    selectedTab = "2";
                    searchView1.setVisibility(View.GONE);
                    ((HomeActivity)baseActivity).setTitleParams(R.color.app_red_color);


                    onDiscountText.setBackground(getColoredBackground(R.drawable.discount_select, red_color));
                    onDiscountText.setTextColor((Color.parseColor("#ffffff")));

                    trendingText.setBackground(getResources().getDrawable(R.drawable.trend_unselect));
                    trendingText.setTextColor(getResources().getColor(black_color));

                    recommendedText.setBackground(getResources().getDrawable(R.drawable.recommend_unselect));
                    recommendedText.setTextColor(getResources().getColor(black_color));
                    break;
            }

            getProductsList(selectedTab);
        }
    };

    private void getProductsList(String tab) {

        String category = filters.getCategory();
        UtilsClass.showBusyAnimation(getActivity(), "Loading..");

        Call<ProductModel> call = apiService.getProductsList(SharedPreferenceUtility.getUserId(getContext()),category,"",tab);

        call.enqueue(new Callback<ProductModel>() {
            @Override
            public void onResponse(Call<ProductModel> call, final Response<ProductModel> response) {
                UtilsClass.hideBusyAnimation(getActivity());

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (response.body() != null) {
                            if (response.body().getStatus()) {
                                productsRecyclerView.setVisibility(View.VISIBLE);
                                noResultsView.setVisibility(View.INVISIBLE);
                                productList = response.body().getProducts();
                                productadapter = new ProductAdapter(getActivity(), productList, false, selectedTab);
                                productsRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
                                productsRecyclerView.setAdapter(productadapter);

                            } else // Handle failure cases here
                            {
                                String mess = response.body().getMessage();
                                Log.d(TAG, "Response: " + mess);
                                productsRecyclerView.setVisibility(View.INVISIBLE);
                                noResultsView.setVisibility(View.VISIBLE);
                                productList.clear();
                                productadapter.notifyDataSetChanged();
//                        showToastAtCentre("Response: " + mess, LENGTH_LONG);
                            }
                        } else {
                            showToastAtCentre("Response: " + response.raw().message(), LENGTH_LONG);
                        }

                    }
                });

            }

            @Override
            public void onFailure(Call<ProductModel> call, Throwable t) {
                UtilsClass.hideBusyAnimation(getActivity());
                Log.d(TAG, "Response: " + "Something went wrong!!!!");
                showToastAtCentre("Response: " + t.getMessage(), LENGTH_LONG);
            }
        });

    }



    private void getSearchProductsList(String searchText) {

        UtilsClass.showBusyAnimation(getActivity(), "Loading..");
        Call<ProductModel> searchProductsCall = apiService.getProductsList(SharedPreferenceUtility.getUserId(getContext()),"", searchText, selectedTab);

        searchProductsCall.enqueue(new Callback<ProductModel>() {
            @Override
            public void onResponse(Call<ProductModel> call, final Response<ProductModel> response) {
                UtilsClass.hideBusyAnimation(getActivity());

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (response.body() != null) {
                            if (response.body().getStatus()) {

                                searchProductList = response.body().getProducts();
                                searchProductAdapter = new ProductSearchAdapter(getActivity(), searchProductList, false);

                                if (searchProductList.size() > 0)
                                {
                                    mainLayout.setVisibility(View.GONE);
                                    searchLayout.setVisibility(View.VISIBLE);
                                    bottomFiltersLayout.setVisibility(View.GONE);
                                    Log.d(TAG, "search layout visible");
                                }
                                else {
                                    showToastAtCentre("No products matches your keyword. Please try with different keyword.",LENGTH_LONG);
                                    searchLayout.setVisibility(View.GONE);
                                    mainLayout.setVisibility(View.VISIBLE);
                                    bottomFiltersLayout.setVisibility(View.VISIBLE);
                                    Log.d(TAG, "search layout hidden");
                                    return;
                                }

                                String header = "We found " + searchProductList.size() + " products that matches " + "\""+ searchView1.getQuery().toString().trim() + "\"";
                                searchResultsHeader.setText(header);

                                searchRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
                                searchRecyclerView.setAdapter(searchProductAdapter);

                                recommendedProductList = response.body().getProducts();
                                recommendedProductsAdapter = new ProductAdapter(getActivity(), recommendedProductList, false, selectedTab);

                                if(recommendedProductList.size() <= 0)
                                {
                                    searchResultsAlternateHeader.setVisibility(View.GONE);
                                    recommendedRecyclerView.setVisibility(View.GONE);
                                    return;
                                }
                                else{
                                    searchResultsAlternateHeader.setVisibility(View.VISIBLE);
                                    recommendedRecyclerView.setVisibility(View.VISIBLE);
                                }

                                recommendedRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
                                recommendedRecyclerView.setAdapter(recommendedProductsAdapter);

                                String text1 = "but here are ";
                                String text2 = + recommendedProductList.size() + " low calories alternatives";
                                String recommendedHeader = "<font color=#4B5262>" + text1 +"</font><font color=#56c2bb> " + text2 + "</font>";
                                searchResultsAlternateHeader.setText(Html.fromHtml(recommendedHeader));

                            } else // Handle failure cases here
                            {
                                String mess = response.body().getMessage();
                                Log.d(TAG, "Response status: " + mess);
                                showToastAtCentre("No products matches your keyword. Please try with different keyword.",LENGTH_LONG);
                                recommendedProductList.clear();
                                recommendedProductsAdapter.notifyDataSetChanged();
                                searchProductList.clear();
                                searchProductAdapter.notifyDataSetChanged();
//                        showToastAtCentre("Response: " + mess, LENGTH_LONG);
                            }
                        } else {
                            Log.d(TAG,"Response body: " + response.raw().message());
                        }

                    }
                });

            }

            @Override
            public void onFailure(Call<ProductModel> call, Throwable t) {
                UtilsClass.hideBusyAnimation(getActivity());
                Log.d(TAG, "Response: " + "Something went wrong!!!!");
//                showToastAtCentre("Response: " + t.getMessage(), LENGTH_LONG);
            }
        });
    }

    public class CategoryFilters {

        private String category;

        public String getCategory() {
            return category;
        }

        public void setCategory(String category) {
            this.category = category;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CATEGORY_ACTIVITY_CODE && resultCode == Activity.RESULT_OK)
        {
            Bundle b = data.getExtras();
            ArrayList array = b.getStringArrayList("SELECTED_CATEGORIES");

            int len = array.size();
            switch (len)
            {
                case 0:
                    break;
                case 1:
                    category1Btn.setText(String.valueOf(array.get(0)));
                    break;
                case 2:
                    category1Btn.setText(String.valueOf(array.get(0)));
                    category2Btn.setText(String.valueOf(array.get(1)));
                    break;
                case 3:
                    category1Btn.setText(String.valueOf(array.get(0)));
                    category2Btn.setText(String.valueOf(array.get(1)));
                    category3Btn.setText(String.valueOf(array.get(2)));
                    break;
                case 4:
                    category1Btn.setText(String.valueOf(array.get(0)));
                    category2Btn.setText(String.valueOf(array.get(1)));
                    category3Btn.setText(String.valueOf(array.get(2)));
                    category4Btn.setText(String.valueOf(array.get(3)));
                    break;
            }

        }
    }

}
