package com.greeneru.mealswapp.Models;

import java.util.List;

public class CategoryModel {

    private boolean status;

    private String message;

    private String userId;

    private List<Category> categories;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean stat) {
        this.status = stat;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public class Category {
        private String CatID;
        private String CategoryName;
        private String Status;
        private String AddedBy;
        private boolean isSelected;

        public String getCatID() {
            return CatID;
        }

        public void setCatID(String catID) {
            CatID = catID;
        }

        public String getCategoryName() {
            return CategoryName;
        }

        public void setCategoryName(String CategoryName) {
            CategoryName = CategoryName;
        }

        public String getStatus() {
            return Status;
        }

        public String getAddedBy() {
            return AddedBy;
        }

        public void setAddedBy(String addedBy) {
            AddedBy = addedBy;
        }

        public boolean isSelected() {
            return isSelected;
        }

        public void setSelected(boolean selected) {
            isSelected = selected;
        }
    }
}
