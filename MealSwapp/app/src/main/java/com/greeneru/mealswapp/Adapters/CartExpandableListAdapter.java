package com.greeneru.mealswapp.Adapters;


import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.greeneru.mealswapp.HomeActivity;
import com.greeneru.mealswapp.Models.CartModel;
import com.greeneru.mealswapp.Models.StatusModel;
import com.greeneru.mealswapp.R;
import com.greeneru.mealswapp.Retrofit.ApiClient;
import com.greeneru.mealswapp.Retrofit.ApiInterface;
import com.greeneru.mealswapp.Utilities.SharedPreferenceUtility;
import com.greeneru.mealswapp.UtilsClass;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CartExpandableListAdapter extends BaseExpandableListAdapter {

    private Context context;
    private Activity baseActivity;
    private List<CartModel.RecipesInCart> recipesInCarts;
    private List<CartModel.ProductsInCart> productsInCarts;
    private TextView cartItemTitleView;
    private TextView cartItemCalories;
    private TextView cartItemPrice;
    private ImageView cartItemImageView;
    private CheckBox cartItemCheckView;
    private TextView cartItemDecrement;
    private TextView cartItemQuantity;
    private TextView cartItemIncrement;
    private ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
    private quantityChangedInterface qInterface;

    public interface quantityChangedInterface {
        void onQuantityChanged();
    }

    public CartExpandableListAdapter(Context context, List<CartModel.RecipesInCart> recipesInCarts, quantityChangedInterface interfc) {
        this.context = context;
        this.baseActivity = (HomeActivity)context;
        this.recipesInCarts = recipesInCarts;
        this.qInterface = interfc;
    }

    @Override
    public Object getChild(int listPosition, int expandedListPosition) {
        return this.recipesInCarts.get(listPosition).getIngredients().get(expandedListPosition);
    }

    @Override
    public long getChildId(int listPosition, int expandedListPosition) {
        return expandedListPosition;
    }

    @Override
    public View getChildView(int listPosition, final int expandedListPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {
        final CartModel.ProductsInCart productsInCart = (CartModel.ProductsInCart) getChild(listPosition, expandedListPosition);
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.cart_item, null);
        }
        cartItemTitleView = convertView.findViewById(R.id.cartItemTitleView);
        cartItemCalories = convertView.findViewById(R.id.cartItemCalories);
        cartItemPrice = convertView.findViewById(R.id.cartItemPrice);
        cartItemImageView = convertView.findViewById(R.id.cartItemImageView);
        cartItemCheckView = convertView.findViewById(R.id.cartItemCheckView);
        cartItemDecrement = convertView.findViewById(R.id.cartItemDecrement);
        cartItemQuantity = convertView.findViewById(R.id.cartItemQuantity);
        cartItemIncrement = convertView.findViewById(R.id.cartItemIncrement);

        cartItemTitleView.setText(productsInCart.getProductName());
        cartItemCalories.setText(productsInCart.getCaloriesKcal() + " cal");
        cartItemPrice.setText("₹ " + productsInCart.getProductPrice());
        cartItemQuantity.setText(productsInCart.getProductQty());
        cartItemCheckView.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                productsInCart.setChecked(isChecked);
            }
        });

        cartItemIncrement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final int quantity =  Integer.parseInt(productsInCart.getProductQty().trim()) + 1;

                String cartId = productsInCart.getCartId() == null ? "" : productsInCart.getCartId();
                UtilsClass.showBusyAnimation(baseActivity,"");

                    Call<StatusModel> call = apiService.addItemsToCart(SharedPreferenceUtility.getUserId(context), productsInCart.getProductId(), productsInCart.getProductPrice(), String.valueOf(quantity), "1", "2", "", cartId, "1");

                    call.enqueue(new Callback<StatusModel>() {
                        @Override
                        public void onResponse(Call<StatusModel> call, final Response<StatusModel> response) {
                            UtilsClass.hideBusyAnimation(baseActivity);
                            if (response.body() != null) {
                                if (response.body().getStatus()) {

                                    String quantityStr = String.valueOf(quantity);
                                    productsInCart.setProductQty((quantityStr));
                                    cartItemQuantity.setText(quantityStr);
                                    //notifyDataSetChanged();
                                    if (qInterface != null) {
                                        qInterface.onQuantityChanged();
                                    }

                                } else {
                                    // retain the button background
                                    Log.d(ProductAdapter.class.getSimpleName(), "Response: " + response.body().getMessage());
                                }

                            } else {
                                // retain the button background
                                Log.d(ProductAdapter.class.getSimpleName(), "Response: " + response.raw().message());
                            }
                        }

                        @Override
                        public void onFailure(Call<StatusModel> call, Throwable t) {
                            UtilsClass.hideBusyAnimation(baseActivity);
                            // retain the button background
                            Log.d(ProductAdapter.class.getSimpleName(), "Response: " + "Something went wrong!!!!");

                        }
                    });

            }
        });

        cartItemDecrement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final int quantity =  Integer.parseInt(productsInCart.getProductQty().trim()) - 1;

                if (quantity != 0) {

                    String cartId = productsInCart.getCartId() == null ? "" : productsInCart.getCartId();
                    UtilsClass.showBusyAnimation(baseActivity,"");
                    Call<StatusModel> call = apiService.addItemsToCart(SharedPreferenceUtility.getUserId(context), productsInCart.getProductId(), productsInCart.getProductPrice(), String.valueOf(quantity), "1", "2", "", cartId, "0");

                    call.enqueue(new Callback<StatusModel>() {
                        @Override
                        public void onResponse(Call<StatusModel> call, final Response<StatusModel> response) {
                            UtilsClass.hideBusyAnimation(baseActivity);
                            if (response.body() != null) {
                                if (response.body().getStatus()) {

                                    String quantityStr = String.valueOf(quantity);
                                    productsInCart.setProductQty((quantityStr));
                                    cartItemQuantity.setText(quantityStr);
                                    //notifyDataSetChanged();
                                    if (qInterface != null) {
                                        qInterface.onQuantityChanged();
                                    }
                                } else {
                                    // retain the button background
                                    Log.d(ProductAdapter.class.getSimpleName(), "Response: " + response.body().getMessage());
                                }

                            } else {
                                // retain the button background
                                Log.d(ProductAdapter.class.getSimpleName(), "Response: " + response.raw().message());
                            }
                        }

                        @Override
                        public void onFailure(Call<StatusModel> call, Throwable t) {
                            UtilsClass.hideBusyAnimation(baseActivity);
                            // retain the button background
                            Log.d(ProductAdapter.class.getSimpleName(), "Response: " + "Something went wrong!!!!");

                        }
                    });
                }
            }
        });

        return convertView;
    }

    @Override
    public int getChildrenCount(int listPosition) {
        return this.recipesInCarts.get(listPosition).getIngredients().size();
    }

    @Override
    public Object getGroup(int listPosition) {
        return this.recipesInCarts.get(listPosition);

    }

    @Override
    public int getGroupCount() {
        return this.recipesInCarts.size();
    }

    @Override
    public long getGroupId(int listPosition) {
        return listPosition;
    }

    @Override
    public View getGroupView(int listPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {

        View view;
            if (listPosition == 0) {
                LayoutInflater layoutInflater = (LayoutInflater) this.context.
                        getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = new FrameLayout(context);
                convertView = view;//layoutInflater.inflate(R.layout.list_group_first, null);

                /*TextView listTitleTextView = convertView.findViewById(R.id.listTitle);
                listTitleTextView.setText("item");*/
            }
            else {
                    LayoutInflater layoutInflater = (LayoutInflater) this.context.
                            getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    convertView = layoutInflater.inflate(R.layout.list_group, null);

                    TextView listTitleTextView = convertView.findViewById(R.id.listTitle);
                    CheckBox cartGroupCheckView = convertView.findViewById(R.id.cartGroupCheckView);
                    TextView cartGroupDecrement = convertView.findViewById(R.id.cartGroupDecrement);
                    TextView cartGroupIncrement = convertView.findViewById(R.id.cartGroupIncrement);
                    final TextView cartGroupQuantity = convertView.findViewById(R.id.cartGroupQuantity);

                    final CartModel.RecipesInCart cartGroup = (CartModel.RecipesInCart) getGroup(listPosition);
                    listTitleTextView.setText(cartGroup.getRecipeName());
                    cartGroupQuantity.setText(cartGroup.getRecipeQty());

                    cartGroupCheckView.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        cartGroup.setChecked(isChecked);
                    }
                });

                cartGroupIncrement.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        final int quantity =  Integer.parseInt(cartGroup.getRecipeQty().trim()) + 1;

                            String cartId = cartGroup.getCartId() == null ? "" : cartGroup.getCartId();
                        UtilsClass.showBusyAnimation(baseActivity,"");
                            Call<StatusModel> call = apiService.addItemsToCart(SharedPreferenceUtility.getUserId(context), cartGroup.getRecipeId(), cartGroup.getRecipePrice(), String.valueOf(quantity), "2", "2", "", cartId, "1");

                            call.enqueue(new Callback<StatusModel>() {
                                @Override
                                public void onResponse(Call<StatusModel> call, final Response<StatusModel> response) {
                                    UtilsClass.hideBusyAnimation(baseActivity);
                                    if (response.body() != null) {
                                        if (response.body().getStatus()) {

                                            String quantityStr = String.valueOf(quantity);
                                            cartGroup.setRecipeQty((quantityStr));
                                            cartGroupQuantity.setText(quantityStr);
                                            //notifyDataSetChanged();
                                            if (qInterface != null) {
                                                qInterface.onQuantityChanged();
                                            }

                                        } else {
                                            // retain the button background
                                            Log.d(ProductAdapter.class.getSimpleName(), "Response: " + response.body().getMessage());
                                        }

                                    } else {
                                        // retain the button background
                                        Log.d(ProductAdapter.class.getSimpleName(), "Response: " + response.raw().message());
                                    }
                                }

                                @Override
                                public void onFailure(Call<StatusModel> call, Throwable t) {
                                    UtilsClass.hideBusyAnimation(baseActivity);
                                    // retain the button background
                                    Log.d(ProductAdapter.class.getSimpleName(), "Response: " + "Something went wrong!!!!");

                                }
                            });
                        }
                });


                cartGroupDecrement.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        final int quantity =  Integer.parseInt(cartGroup.getRecipeQty().trim()) - 1;

                        if (quantity != 0) {

                            String cartId = cartGroup.getCartId() == null ? "" : cartGroup.getCartId();
                            UtilsClass.showBusyAnimation(baseActivity,"");
                            Call<StatusModel> call = apiService.addItemsToCart(SharedPreferenceUtility.getUserId(context), cartGroup.getRecipeId(), cartGroup.getRecipePrice(), String.valueOf(quantity), "2", "2", "", cartId, "0");

                            call.enqueue(new Callback<StatusModel>() {
                                @Override
                                public void onResponse(Call<StatusModel> call, final Response<StatusModel> response) {
                                    UtilsClass.hideBusyAnimation(baseActivity);
                                    if (response.body() != null) {
                                        if (response.body().getStatus()) {

                                            String quantityStr = String.valueOf(quantity);
                                            cartGroup.setRecipeQty((quantityStr));
                                            cartGroupQuantity.setText(quantityStr);
                                            //notifyDataSetChanged();
                                            if (qInterface != null) {
                                                qInterface.onQuantityChanged();
                                            }

                                        } else {
                                            // retain the button background
                                            Log.d(ProductAdapter.class.getSimpleName(), "Response: " + response.body().getMessage());
                                        }

                                    } else {
                                        // retain the button background
                                        Log.d(ProductAdapter.class.getSimpleName(), "Response: " + response.raw().message());
                                    }
                                }

                                @Override
                                public void onFailure(Call<StatusModel> call, Throwable t) {
                                    UtilsClass.hideBusyAnimation(baseActivity);
                                    // retain the button background
                                    Log.d(ProductAdapter.class.getSimpleName(), "Response: " + "Something went wrong!!!!");

                                }
                            });
                        }

                    }
                });

            }

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int listPosition, int expandedListPosition) {
        return true;
    }
}
