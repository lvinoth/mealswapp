package com.greeneru.mealswapp;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import static com.greeneru.mealswapp.TourPageView.GOALS.BUILD_MUSCLE;
import static com.greeneru.mealswapp.TourPageView.GOALS.GENERAL_HEALTH;
import static com.greeneru.mealswapp.TourPageView.GOALS.LOSE_WEIGHT;
import static com.greeneru.mealswapp.TourPageView.GOALS.LOWER_CHOLESTEROL;
import static com.greeneru.mealswapp.TourPageView.GOALS.LOWER_DIABETES;
import static com.greeneru.mealswapp.TourPageView.GOALS.REDUCE_BLOOD_PRESSURE;

public class TourPageView extends ViewPager {

    private GestureDetector mGesture;
    List<TourPageView.GOALS> selectedGoal = new ArrayList<>(6);

    private TextView loseWeightView;
    private TextView lowerDiabetesView;
    private TextView reduceBloodPressureView;

    private TextView buildMuscleView;
    private TextView lowerCholesterolView;
    private TextView generalHealthView;

    public enum GOALS {
        LOSE_WEIGHT,
        BUILD_MUSCLE,
        LOWER_DIABETES,
        REDUCE_BLOOD_PRESSURE,
        LOWER_CHOLESTEROL,
        GENERAL_HEALTH
    }

    public TourPageView(@NonNull Context context) {
        super(context);
        init();
    }



    public TourPageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();

    }

    private void init() {
        mGesture = new GestureDetector(getContext(), new GestureDetector.SimpleOnGestureListener() {

            @Override
            public boolean onSingleTapConfirmed(MotionEvent e) {
                //Handle your click here

                Toast.makeText(getContext(),"clicked!!",Toast.LENGTH_SHORT).show();

        loseWeightView = findViewById(R.id.loseWeightView);
        buildMuscleView = findViewById(R.id.buildMuscleView);
        reduceBloodPressureView = findViewById(R.id.reduceBloodPressureView);
        lowerDiabetesView = findViewById(R.id.lowerDiabetesView);
        lowerCholesterolView = findViewById(R.id.lowerCholesterolView);
        generalHealthView = findViewById(R.id.generalHealthView);

        loseWeightView.setOnClickListener(clickListener);
        buildMuscleView.setOnClickListener(clickListener);
        reduceBloodPressureView.setOnClickListener(clickListener);
        lowerDiabetesView.setOnClickListener(clickListener);
        lowerCholesterolView.setOnClickListener(clickListener);
        generalHealthView.setOnClickListener(clickListener);

                return true;
            }

        });
    }


    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        if (!mGesture.onTouchEvent(event)) {
            super.onInterceptTouchEvent(event);
        }
        return true;
    }

    View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int id = v.getId();



            switch (id)
            {
                case R.id.loseWeightView:
                    selectGoalsView(LOSE_WEIGHT,loseWeightView);
                    break;
                case R.id.buildMuscleView:
                    selectGoalsView(BUILD_MUSCLE,buildMuscleView);
                    break;
                case R.id.reduceBloodPressureView:
                    selectGoalsView(REDUCE_BLOOD_PRESSURE,reduceBloodPressureView);
                    break;
                case R.id.lowerCholesterolView:
                    selectGoalsView(LOWER_CHOLESTEROL,lowerCholesterolView);
                    break;
                case R.id.lowerDiabetesView:
                    selectGoalsView(LOWER_DIABETES,lowerDiabetesView);
                    break;
                case R.id.generalHealthView:
                    selectGoalsView(GENERAL_HEALTH,generalHealthView);
                    break;
            }
            //beginJourneyView.setEnabled(selectedGoal.size() > 0);
        }
    };

    private void selectGoalsView(TourPageView.GOALS goal, TextView view)
    {
        if (selectedGoal.contains(goal))
        {
            //set the drawable black_color, not the button black_color
            view.setBackground(getResources().getDrawable(R.drawable.rounded_border_goals));
            selectedGoal.remove(goal);
        }
        else
        {
            view.setBackground(getResources().getDrawable(R.drawable.rounded_border_goals_selected));
            selectedGoal.add(goal);
        }
    }
}
