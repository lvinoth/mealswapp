package com.greeneru.mealswapp;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class ViewPagerAdapter extends FragmentPagerAdapter {

    private String title[] = {"Recipes", "Products", "Cart"};
    private final List<Fragment> mFragmentList = new ArrayList<>();

    private RecipeFragment recipeFragment;
    private ProductFragment productFragment;
    private CartFragment cartFragment;

    public ViewPagerAdapter(FragmentManager manager) {
        super(manager);
        recipeFragment = new RecipeFragment();
        productFragment = new ProductFragment();
        cartFragment = new CartFragment();
    }

    @Override
    public Fragment getItem(int position) {

        switch (position)
        {
            case 0:
                return recipeFragment;
            case 1:
                return productFragment;
            case 2:
                return cartFragment;
            default:
                return null;
        }
    }

    public Fragment getItemAt(int position) {

        switch (position)
        {
            case 0:
                return recipeFragment;
            case 1:
                return productFragment;
            case 2:
                return cartFragment;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return title.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return title[position];
    }
}
