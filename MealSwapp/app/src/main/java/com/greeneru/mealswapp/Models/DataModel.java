package com.greeneru.mealswapp.Models;

public class DataModel {

    private String ingredientName;
    private String ingredientPrice;
    private String ingredientQuantity;
    private String ingredientNameRequiredQuantity;
    private String ingredientUnits;

    public DataModel(String iname, String iPrice, String iQuantity, String iRequiredQuantity, String iUnit) {
        this.ingredientName = iname;
        this.ingredientPrice = iPrice;
        this.ingredientQuantity = iQuantity;
        this.ingredientNameRequiredQuantity = iRequiredQuantity;
        this.ingredientUnits = iUnit;

    }


    public String getIngredientName() {
        return ingredientName;
    }

    public void setIngredientName(String ingredientName) {
        this.ingredientName = ingredientName;
    }

    public String getIngredientPrice() {
        return ingredientPrice;
    }

    public void setIngredientPrice(String ingredientPrice) {
        this.ingredientPrice = ingredientPrice;
    }

    public String getIngredientQuantity() {
        return ingredientQuantity;
    }

    public void setIngredientQuantity(String ingredientQuantity) {
        this.ingredientQuantity = ingredientQuantity;
    }

    public String getIngredientNameRequiredQuantity() {
        return ingredientNameRequiredQuantity;
    }

    public void setIngredientNameRequiredQuantity(String ingredientNameRequiredQuantity) {
        this.ingredientNameRequiredQuantity = ingredientNameRequiredQuantity;
    }

    public String getIngredientUnits() {
        return ingredientUnits;
    }

    public void setIngredientUnits(String ingredientUnits) {
        this.ingredientUnits = ingredientUnits;
    }
}