package com.greeneru.mealswapp;


import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.SwitchCompat;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.greeneru.mealswapp.Adapters.ApiRecipesAdapter;
import com.greeneru.mealswapp.Adapters.RecipeAdapter;
import com.greeneru.mealswapp.Interfaces.RecipeDataChangeInterface;
import com.greeneru.mealswapp.Models.RecipeModel;
import com.greeneru.mealswapp.Models.StatusModel;
import com.greeneru.mealswapp.Retrofit.ApiClient;
import com.greeneru.mealswapp.Retrofit.ApiInterface;
import com.greeneru.mealswapp.Retrofit.Data;
import com.greeneru.mealswapp.Utilities.SharedPreferenceUtility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.widget.Toast.LENGTH_LONG;
import static android.widget.Toast.LENGTH_SHORT;


/**
 * A simple {@link Fragment} subclass.
 */
public class RecipeFragment extends Fragment  {

    private Adapter initializedAdapter = null;
    private Spinner spinner;
    private TextView breakfastText;
    private TextView lunchText;
    private TextView snacksText;
    private TextView dinnerText;
    private EditText searchView;
    private SearchView searchView1;
    private TabLayout mealsTab;

    private Button cuisine1Btn;
    private Button cuisine2Btn;
    private Button cuisine3Btn;

    private TextView recipeMealHeader;
    private TextView recipeMealTip;
    private Toast mToast;
    private TextView noResultsView;

    private TextView recommendedText;
    private TextView trendingText;
    private TextView onDiscountText;

    private TextView searchResultsHeader;
    private TextView searchResultsAlternateHeader;

    int black_color = R.color.black;

    private LinearLayout breakfastLayout;
    private LinearLayout lunchLayout;
    private LinearLayout snacksLayout;
    private LinearLayout dinnerLayout;

    private RecyclerView recyclerView;
    private List<RecipeModel.Recipes> recipeList;
    private RecipeAdapter adapter;

    private RecyclerView apiRecyclerView;
    private List<RecipeModel.ApiRecipes> apiRecipeList;
    private ApiRecipesAdapter apiRecipeAdapter;

    private RecyclerView recommendedRecyclerView;
    private List<RecipeModel.Recipes> recommendedRecipeList;
    private RecipeAdapter recommendedRecipesAdapter;

    private static final String TAG = RecipeFragment.class.getSimpleName();
    private ApiInterface apiService;
    private RecipeFilters filters;
    private SwitchCompat foodTypeSwitch;
    private ImageView cuisineFilter;
    private LinearLayout bottomFiltersLayout;

    private LinearLayout mainLayout;
    private LinearLayout searchLayout;
    Call<RecipeModel> searchRecipesCall;
    private String selectedTab = "0";
    private Activity baseActivity;
    List<Data> data;
    private int red_color;
    private int blue_color;
    private int green_color;
    private int default_color;

    private static final int CUISINE_ACTIVITY_CODE = 101;
    public enum CUISINES {
        TAMIL,
        PUNJABI,
        KANNADA,
        CHINESE,
        MUGHALAI,
        GUJARATHI,
        BENGALI,
        ANDHRA,
        KERALA
    }

    public List<String> cuisines = new ArrayList<>();

    List<String> selectedCuisines = new ArrayList<>();

    public RecipeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "Fragment Resumed!!");
        if(UtilsClass.isIsChangesMadeInRecipes())
        {
            UtilsClass.setIsChangesMadeInRecipes(false);
            if (!searchView1.getQuery().toString().trim().isEmpty())
            {
                getSearchRecipesList(searchView1.getQuery().toString().trim());
            }
            else {

                getRecipesList(selectedTab);
            }

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_recipe, container, false);
        filters = new RecipeFilters();
        selectedCuisines.clear();
        mealsTab = view.findViewById(R.id.mealTab);
        mealsTab.setOnTabSelectedListener(tabSelectedListener);

        baseActivity = (HomeActivity)getActivity();

        red_color = getActivity().getResources().getColor(R.color.app_red_color);
        blue_color = getActivity().getResources().getColor(R.color.app_blue_color);
        green_color = getActivity().getResources().getColor(R.color.app_green_color);
        default_color = getActivity().getResources().getColor(R.color.tab_selected_color);

        recipeList = new ArrayList<>();
        adapter = new RecipeAdapter(getActivity(), recipeList, false, "-1");

        apiRecipeList = new ArrayList<>();
        apiRecipeAdapter = new ApiRecipesAdapter(getActivity(), apiRecipeList, false);

        recommendedRecipeList = new ArrayList<>();
        recommendedRecipesAdapter = new RecipeAdapter(getActivity(), recommendedRecipeList, false, "-1");

        searchLayout = view.findViewById(R.id.searchRecipeLayout);
        mainLayout = view.findViewById(R.id.mainLayout);

        foodTypeSwitch = view.findViewById(R.id.foodTypeSwitch);
        foodTypeSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                filters.setFoodType(isChecked ? "0" : "1");
                getRecipesList(selectedTab);
            }
        });
        cuisines.add(0,"empty");
        cuisines.add(1,"Punjabi");
        cuisines.add(2,"South Indian");
        cuisines.add(3,"Bengali");
        cuisines.add(4,"Malaysian");
        cuisines.add(5,"Karnataka");

        bottomFiltersLayout = view.findViewById(R.id.bottomFiltersLayout);
        spinner = view.findViewById(R.id.sort);
        cuisineFilter = view.findViewById(R.id.cuisineFilterView);
        cuisineFilter.setOnClickListener(cuisineFilterListener);

        breakfastLayout = view.findViewById(R.id.breakfastLayout);
        lunchLayout = view.findViewById(R.id.lunchLayout);
        snacksLayout = view.findViewById(R.id.snacksLayout);
        dinnerLayout = view.findViewById(R.id.dinnerLayout);

        breakfastText = view.findViewById(R.id.breakfastTextView);
        lunchText = view.findViewById(R.id.lunchTextView);
        snacksText = view.findViewById(R.id.snacksTextView);
        dinnerText = view.findViewById(R.id.dinnerTextView);

        searchResultsHeader = view.findViewById(R.id.searchResultsHeader);
        searchResultsAlternateHeader = view.findViewById(R.id.searchResultsAlternateHeader);

        cuisine1Btn = view.findViewById(R.id.tamilCuisineBtn);
        cuisine2Btn = view.findViewById(R.id.punjabiCuisineBtn);
        cuisine3Btn = view.findViewById(R.id.kannadaCuisineBtn);

        recommendedText = view.findViewById(R.id.recommendedView);
        trendingText = view.findViewById(R.id.trendingView);
        onDiscountText = view.findViewById(R.id.discountView);

        /*breakfastLayout.setOnClickListener(clickListener);
        lunchLayout.setOnClickListener(clickListener);
        snacksLayout.setOnClickListener(clickListener);
        dinnerLayout.setOnClickListener(clickListener);*/

        cuisine1Btn.setOnClickListener(cuisineSelectedListener);
        cuisine2Btn.setOnClickListener(cuisineSelectedListener);
        cuisine3Btn.setOnClickListener(cuisineSelectedListener);

        recommendedText.setOnClickListener(recommendedListener);
        trendingText.setOnClickListener(recommendedListener);
        onDiscountText.setOnClickListener(recommendedListener);

        recipeMealHeader = view.findViewById(R.id.recipe_mealOfTheDay);
        recipeMealTip = view.findViewById(R.id.recipe_tipView);
        /*searchView = view.findViewById(R.id.searchView);
        searchView.addTextChangedListener(watcher);*/

        searchView1 = view.findViewById(R.id.searchView1);
        searchView1.setOnQueryTextListener(queryTextListener);
        recyclerView = view.findViewById(R.id.recipeRecyclerView);
        apiRecyclerView = view.findViewById(R.id.recipeSearchRecyclerView);
        recommendedRecyclerView = view.findViewById(R.id.recipeSearchRecommendedRecyclerView);
        noResultsView = view.findViewById(R.id.noResultsView);

        apiService = ApiClient.getClient().create(ApiInterface.class);

        recommendedText.setBackground(getColoredBackground(R.drawable.recommend_select, blue_color));
        recommendedText.setTextColor((Color.parseColor("#ffffff")));
        setInitialFilters();
        getRecipesList(selectedTab);

        breakfastText.setTextColor(getResources().getColor(black_color));
        recipeMealHeader.setText(getHealthTip());
        recipeMealTip.setText(getRecipesTip());
        setUpSort();
        ((HomeActivity)baseActivity).setTitleParams(R.color.app_blue_color);
        mealsTab.setTabTextColors(default_color,blue_color);

        return view;
    }

    public void setTitleParams()
    {
        int colorToSet = blue_color;
        switch (selectedTab)
        {
            case "0":
                colorToSet = blue_color;
                break;
            case "1":
                colorToSet = green_color;
                break;
            case "2":
                colorToSet = red_color;
                break;
        }

        ((HomeActivity)baseActivity).titleView.setTextColor(colorToSet);
        DrawableCompat.setTint(((HomeActivity)baseActivity).profileView.getDrawable(), colorToSet);
    }

    TabLayout.OnTabSelectedListener tabSelectedListener = new TabLayout.OnTabSelectedListener() {
        @Override
        public void onTabSelected(TabLayout.Tab tab) {
            String mealType = "1";

            switch (tab.getPosition())
            {
                case 0:
                    mealType="1";
                    break;
                case 1:
                    mealType="2";
                    break;
                case 2:
                    mealType="3";
                    break;
                case 3:
                    mealType="4";
                    break;
            }

            filters.setMealType(mealType);
            getRecipesList(selectedTab);
        }

        @Override
        public void onTabUnselected(TabLayout.Tab tab) {
        }

        @Override
        public void onTabReselected(TabLayout.Tab tab) {

        }
    };

    private String getHealthTip()
    {
        String id = SharedPreferenceUtility.getGoalId(getContext());
        int goalid = Integer.parseInt(id);
        String tip = "";
        switch (goalid)
        {
            case 1:
                tip = "LOW CALORIE RECIPES";
            break;
            case 2:
                tip = "HIGH-PROTEIN RECIPES";
                break;
            case 3:
                tip = "DIABETES-FRIENDLY RECIPES";
                break;
            case 4:
                tip = "LOW CHOLESTEROL RECIPES";
                break;
            case 5:
                tip = "LOW SALT RECIPES";
                break;
            case 6:
                tip = "BALANCED DIET RECIPES";
                break;
                default:
                    tip = "HEALTHY RECIPES";
        }
            return tip;
    }

    private String getRecipesTip()
    {
        String id = SharedPreferenceUtility.getGoalId(getContext());
        int goalid = Integer.parseInt(id);
        String tip = "";
        switch (goalid)
        {
            case 1:
                tip = "Reducing calories in your diet is the easiest way to reduce your weight";
                break;
            case 2:
                tip = "To build muscle mass, you need to increase protein intake in your diet";
                break;
            case 3:
                tip = "To reduce diabetes, you need to focus on cutting down added sugars in your diet";
                break;
            case 4:
                tip = "To keep cholesterol under control, you need to reduce the intake of saturated fats";
                break;
            case 5:
                tip = "To keep blood pressure under control, you need to reduce sodium (salt) in your diet";
                break;
            case 6:
                tip = "Eating the right amount of all nutrients is essential in maintaing a healthy lifestyle";
                break;
            default:
                tip = "HEALTHY RECIPES";
        }
        return tip;
    }


    private void setUpSort()
    {
        final ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(getActivity(), R.layout.spinner_item, getResources().getStringArray(R.array.sort_types));
        spinner.setAdapter(dataAdapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // This is to avoid the initial fire up while loading this screen. Do not remove
                if( initializedAdapter !=parent.getAdapter() ) {
                    initializedAdapter = parent.getAdapter();
                    return;
                }
                String data = dataAdapter.getItem(position);
                Log.d(TAG, "Sorting Chosen: " + data);
                //showToastAtCentre(data + " used for sorting! ",LENGTH_SHORT);

                if (recipeList != null && recipeList.size() > 0)
                {
                    recyclerView.setVisibility(View.VISIBLE);
                    noResultsView.setVisibility(View.INVISIBLE);

                    switch (position)
                    {
                        case 0: // popularity
                            break;
                        case 1: // price low to high
                            // Sorts calories
                            Collections.sort(recipeList, new Comparator<RecipeModel.Recipes>() {
                                @Override
                                public int compare(RecipeModel.Recipes s1, RecipeModel.Recipes s2) {
                                    return Double.valueOf(s1.getSalePrice()).compareTo(Double.valueOf(s2.getSalePrice()));
                                }
                            });
                            break;
                        case 2: // calories low to high
                            Collections.sort(recipeList, new Comparator<RecipeModel.Recipes>() {
                                @Override
                                public int compare(RecipeModel.Recipes s1, RecipeModel.Recipes s2) {
                                    return Double.valueOf(s1.getCalorieskcal()).compareTo(Double.valueOf(s2.getCalorieskcal()));
                                }
                            });
                            break;
                        case 3: // alphabetical
                            // Sorts alphabetically
                            Collections.sort(recipeList, new Comparator<RecipeModel.Recipes>() {
                                @Override
                                public int compare(RecipeModel.Recipes s1, RecipeModel.Recipes s2) {
                                    return s1.getRecipeName().compareToIgnoreCase(s2.getRecipeName());
                                }
                            });
                            break;
                        /*case 4: // alphabetical high to low
                            // Sorts alphabetically
                            Collections.sort(recipeList, new Comparator<RecipeModel.Recipes>() {
                                @Override
                                public int compare(RecipeModel.Recipes s1, RecipeModel.Recipes s2) {
                                    return s2.getRecipeName().compareToIgnoreCase(s1.getRecipeName());
                                }
                            });
                            break;*/
                    }

                    adapter = new RecipeAdapter(getActivity(), recipeList, false, selectedTab);
                    adapter.notifyDataSetChanged();
                    recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));

                    recyclerView.setAdapter(adapter);
                }
                else
                {
                    showToastAtCentre("Recipes not available..",LENGTH_SHORT);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private View.OnClickListener cuisineFilterListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent cusIntent = new Intent(getActivity(),CuisineActivity.class);
            cusIntent.putExtra("source","cuisine");
            startActivityForResult(cusIntent, CUISINE_ACTIVITY_CODE);
            getActivity().overridePendingTransition( R.anim.slide_up, R.anim.slide_down);
        }
    };


    private void setInitialFilters() {
        filters.setCuisine("0"); // Default 0, lists all recipes as per meal type and food type
        filters.setMealType("1"); // Snacks.. change it according to the time
        filters.setFoodType(foodTypeSwitch.isChecked() ? "0" : "1");
    }

    private void getRecipesList(String tab) {
        String foodType = filters.getFoodType();
        String mealType = filters.getMealType();
        String cuisine = filters.getCuisine();
        UtilsClass.showBusyAnimation(baseActivity, "Loading..");

        Call<RecipeModel> call = apiService.getRecipesList(SharedPreferenceUtility.getUserId(getContext()), mealType, foodType, cuisine, "", tab);

        call.enqueue(new Callback<RecipeModel>() {
            @Override
            public void onResponse(Call<RecipeModel> call, final Response<RecipeModel> response) {


                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        UtilsClass.hideBusyAnimation(baseActivity);
                        if (response.body() != null) {
                            if (response.body().getStatus()) {
                                recyclerView.setVisibility(View.VISIBLE);
                                noResultsView.setVisibility(View.INVISIBLE);
                                recipeList = response.body().getRecipes();
                                adapter = new RecipeAdapter(getActivity(), recipeList, false, selectedTab);
                                recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
                                recyclerView.setAdapter(adapter);

                            } else // Handle failure cases here
                            {
                                String mess = response.body().getMessage();
                                Log.d(TAG, "Response: " + mess);
                                recyclerView.setVisibility(View.INVISIBLE);
                                noResultsView.setVisibility(View.VISIBLE);
                                recipeList.clear();
                                adapter.notifyDataSetChanged();
//                        showToastAtCentre("Response: " + mess, LENGTH_LONG);
                            }
                        } else {
                            showToastAtCentre("Response: " + response.raw().message(), LENGTH_LONG);
                        }

                    }
                });

            }

            @Override
            public void onFailure(Call<RecipeModel> call, Throwable t) {
                Log.d(TAG, "Response: " + "Something went wrong!!!!");
                showToastAtCentre("Response: " + t.getMessage(), LENGTH_LONG);
                UtilsClass.hideBusyAnimation(baseActivity);
                //UtilsClass.hideBusyAnimation(getActivity());
            }
        });

    }

    private void getSearchRecipesList(String search) {
        UtilsClass.showBusyAnimation(baseActivity, "Loading..");
        searchRecipesCall = apiService.getRecipesList(SharedPreferenceUtility.getUserId(getContext()), "", "", "", search,"");

        searchRecipesCall.enqueue(new Callback<RecipeModel>() {
            @Override
            public void onResponse(Call<RecipeModel> call, final Response<RecipeModel> response) {
                UtilsClass.hideBusyAnimation(baseActivity);

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (response.body() != null) {
                            if (response.body().getStatus()) {

                                apiRecipeList = response.body().getApi_recipes();
                                apiRecipeAdapter = new ApiRecipesAdapter(getActivity(), apiRecipeList, false);

                                if (apiRecipeList.size() > 0)
                                {
                                    mainLayout.setVisibility(View.GONE);
                                    bottomFiltersLayout.setVisibility(View.GONE);
                                    searchLayout.setVisibility(View.VISIBLE);
                                }
                                else {
                                    showToastAtCentre("No recipes matches your keyword. Please try with different keyword.",LENGTH_LONG);
                                    searchLayout.setVisibility(View.GONE);
                                    mainLayout.setVisibility(View.VISIBLE);
                                    bottomFiltersLayout.setVisibility(View.VISIBLE);
                                    return;
                                }
                                String header = "We found " + apiRecipeList.size() + " recipes that matches " + "\""+ searchView1.getQuery().toString().trim() + "\"";
                                searchResultsHeader.setText(header);

                                apiRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
                                apiRecyclerView.setAdapter(apiRecipeAdapter);


                                recommendedRecipeList = response.body().getRecipes();
                                recommendedRecipesAdapter = new RecipeAdapter(getActivity(), recommendedRecipeList, false, selectedTab);

                                if(recommendedRecipeList.size() <= 0)
                                {
                                    searchResultsAlternateHeader.setVisibility(View.GONE);
                                    recommendedRecyclerView.setVisibility(View.GONE);
                                    return;
                                }
                                else{
                                    searchResultsAlternateHeader.setVisibility(View.VISIBLE);
                                    recommendedRecyclerView.setVisibility(View.VISIBLE);
                                }

                                recommendedRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
                                recommendedRecyclerView.setAdapter(recommendedRecipesAdapter);

                                String text1 = "but here are ";
                                String text2 = + recommendedRecipeList.size() + " low calories alternatives";
                                String recommendedHeader = "<font color=#4B5262>" + text1 +"</font><font color=#56c2bb> " + text2 + "</font>";
                                searchResultsAlternateHeader.setText(Html.fromHtml(recommendedHeader));

                            } else // Handle failure cases here
                            {
                                showToastAtCentre("No recipes matches your keyword. Please try with different keyword.",LENGTH_LONG);
                                String mess = response.body().getMessage();
                                Log.d(TAG, "Response status: " + mess);
                                recommendedRecipeList.clear();
                                recommendedRecipesAdapter.notifyDataSetChanged();
                                apiRecipeList.clear();
                                apiRecipeAdapter.notifyDataSetChanged();
//                        showToastAtCentre("Response: " + mess, LENGTH_LONG);
                            }
                        } else {
                            Log.d(TAG,"Response body: " + response.raw().message());
                        }

                    }
                });

            }

            @Override
            public void onFailure(Call<RecipeModel> call, Throwable t) {
                UtilsClass.hideBusyAnimation(baseActivity);
                Log.d(TAG, "Response: " + "Something went wrong!!!!");
//                showToastAtCentre("Response: " + t.getMessage(), LENGTH_LONG);
            }
        });

    }
    private SearchView.OnQueryTextListener queryTextListener = new SearchView.OnQueryTextListener() {
        @Override
        public boolean onQueryTextSubmit(String query) {
            String searchText = searchView1.getQuery().toString().trim();
            if(!searchText.isEmpty())
            {
                getSearchRecipesList(searchText);
            }

            return false;
        }

        @Override
        public boolean onQueryTextChange(String newText) {
            String searchText = searchView1.getQuery().toString().trim();
            int len = searchText.length();


            if (len == 0)
            {
                searchLayout.setVisibility(View.GONE);
                mainLayout.setVisibility(View.VISIBLE);
                bottomFiltersLayout.setVisibility(View.VISIBLE);
            }

            Log.e(TAG,"text count: " + len);
            return false;
        }
    };

    private void showToastAtCentre(String message, int duration) {
        if (mToast != null) {
            mToast.cancel();
            mToast = null;
        }
        mToast = Toast.makeText(getActivity().getApplicationContext(), message, duration);
        mToast.setGravity(Gravity.CENTER, 0, 0);
        mToast.show();
    }


   /* private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int resId = v.getId();
            String mealOfTheDay = "";
            String mealType = "1";
            switch (resId) {
                case R.id.breakfastLayout:

                    breakfastLayout.setBackground(getResources().getDrawable(R.drawable.meal_selected));
                    lunchLayout.setBackground(getResources().getDrawable(R.drawable.meal_unselected));
                    snacksLayout.setBackground(getResources().getDrawable(R.drawable.meal_unselected));
                    dinnerLayout.setBackground(getResources().getDrawable(R.drawable.meal_unselected));

                    breakfastText.setTextColor(getResources().getColor(black_color));
                    lunchText.setTextColor((Color.parseColor("#000000")));
                    snacksText.setTextColor((Color.parseColor("#000000")));
                    dinnerText.setTextColor((Color.parseColor("#000000")));
                    mealOfTheDay = "Breakfast";
                    mealType="1";
                    break;
                case R.id.lunchLayout:

                    lunchLayout.setBackground(getResources().getDrawable(R.drawable.meal_selected));
                    breakfastLayout.setBackground(getResources().getDrawable(R.drawable.meal_unselected));
                    snacksLayout.setBackground(getResources().getDrawable(R.drawable.meal_unselected));
                    dinnerLayout.setBackground(getResources().getDrawable(R.drawable.meal_unselected));

                    lunchText.setTextColor(getResources().getColor(black_color));
                    breakfastText.setTextColor((Color.parseColor("#000000")));
                    snacksText.setTextColor((Color.parseColor("#000000")));
                    dinnerText.setTextColor((Color.parseColor("#000000")));
                    mealOfTheDay = "Lunch";
                    mealType="2";
                    break;
                case R.id.snacksLayout:

                    snacksLayout.setBackground(getResources().getDrawable(R.drawable.meal_selected));
                    breakfastLayout.setBackground(getResources().getDrawable(R.drawable.meal_unselected));
                    lunchLayout.setBackground(getResources().getDrawable(R.drawable.meal_unselected));
                    dinnerLayout.setBackground(getResources().getDrawable(R.drawable.meal_unselected));

                    snacksText.setTextColor(getResources().getColor(black_color));
                    lunchText.setTextColor((Color.parseColor("#000000")));
                    breakfastText.setTextColor((Color.parseColor("#000000")));
                    dinnerText.setTextColor((Color.parseColor("#000000")));
                    mealOfTheDay = "Snacks";
                    mealType="3";
                    break;
                case R.id.dinnerLayout:

                    dinnerLayout.setBackground(getResources().getDrawable(R.drawable.meal_selected));
                    breakfastLayout.setBackground(getResources().getDrawable(R.drawable.meal_unselected));
                    lunchLayout.setBackground(getResources().getDrawable(R.drawable.meal_unselected));
                    snacksLayout.setBackground(getResources().getDrawable(R.drawable.meal_unselected));

                    dinnerText.setTextColor(getResources().getColor(black_color));
                    lunchText.setTextColor((Color.parseColor("#000000")));
                    snacksText.setTextColor((Color.parseColor("#000000")));
                    breakfastText.setTextColor((Color.parseColor("#000000")));
                    mealOfTheDay = "Dinner";
                    mealType="4";
                    break;
            }
            recipeMealHeader.setText("Low-Calorie " + mealOfTheDay + " Recipes");
            filters.setMealType(mealType);
            getRecipesList(selectedTab);
        }
    };*/

    View.OnClickListener cuisineSelectedListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            int id = v.getId();

            switch (id) {
                case R.id.tamilCuisineBtn:
                    selectCuisines(cuisine1Btn);
                    break;

                case R.id.punjabiCuisineBtn:
                    selectCuisines(cuisine2Btn);
                    break;

                case R.id.kannadaCuisineBtn:
                    selectCuisines(cuisine3Btn);
                    break;

                default:

                    break;
            }

        }
    };

    private void selectCuisines(Button view) {

        String index = String.valueOf(cuisines.indexOf(view.getText().toString().trim()));
        StringBuilder selected_cuisine = new StringBuilder();

        if (selectedCuisines.contains(index)) {
            view.setBackground(getResources().getDrawable(R.drawable.cuisine_unselect));
            selectedCuisines.remove(index);
        } else {
            view.setBackground(getResources().getDrawable(R.drawable.cuisine_select));
            selectedCuisines.add(index);
        }


        if (selectedCuisines.size() > 0) {
            selected_cuisine.append(selectedCuisines.get(0));

            for (int i = 1; i < selectedCuisines.size(); i++) {
                selected_cuisine.append(",").append(selectedCuisines.get(i));
            }
        }
        else
        {
            selected_cuisine.append("0");
        }

        filters.setCuisine(selected_cuisine.toString().trim());
        getRecipesList(selectedTab);
    }

    private Drawable getColoredBackground(int drawable, int color)
    {
        Drawable mDrawable = baseActivity.getResources().getDrawable(drawable);
        mDrawable.setColorFilter(new PorterDuffColorFilter(color, PorterDuff.Mode.SRC_IN));
        return  mDrawable;
    }

    View.OnClickListener recommendedListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            int id = v.getId();
            switch (id)
            {
                case R.id.recommendedView:
                    selectedTab = "0";
                    searchView1.setVisibility(View.VISIBLE);
                    ((HomeActivity)baseActivity).setTitleParams(R.color.app_blue_color);
                    mealsTab.setTabTextColors(default_color,blue_color);
                    mealsTab.setSelectedTabIndicatorColor(blue_color);

                    recommendedText.setBackground(getColoredBackground(R.drawable.recommend_select, blue_color));
                    recommendedText.setTextColor((Color.parseColor("#ffffff")));


                    trendingText.setBackground(getResources().getDrawable(R.drawable.trend_unselect));
                    trendingText.setTextColor(getResources().getColor(black_color));

                    onDiscountText.setBackground(getResources().getDrawable(R.drawable.discount_unselect));
                    onDiscountText.setTextColor(getResources().getColor(black_color));

                    break;

                case R.id.trendingView:
                    selectedTab = "1";
                    searchView1.setVisibility(View.GONE);
                    ((HomeActivity)baseActivity).setTitleParams(R.color.app_green_color);
                    mealsTab.setTabTextColors(default_color,green_color);
                    mealsTab.setSelectedTabIndicatorColor(green_color);

                    trendingText.setBackground(getColoredBackground(R.drawable.trend_select, green_color));
                    trendingText.setTextColor((Color.parseColor("#ffffff")));

                    recommendedText.setBackground(getResources().getDrawable(R.drawable.recommend_unselect));
                    recommendedText.setTextColor(getResources().getColor(black_color));

                    onDiscountText.setBackground(getResources().getDrawable(R.drawable.discount_unselect));
                    onDiscountText.setTextColor(getResources().getColor(black_color));
                    break;

                case R.id.discountView:
                    selectedTab = "2";
                    searchView1.setVisibility(View.GONE);
                    ((HomeActivity)baseActivity).setTitleParams(R.color.app_red_color);
                    mealsTab.setTabTextColors(default_color,red_color);
                    mealsTab.setSelectedTabIndicatorColor(red_color);

                    onDiscountText.setBackground(getColoredBackground(R.drawable.discount_select, red_color));
                    onDiscountText.setTextColor((Color.parseColor("#ffffff")));

                    trendingText.setBackground(getResources().getDrawable(R.drawable.trend_unselect));
                    trendingText.setTextColor(getResources().getColor(black_color));

                    recommendedText.setBackground(getResources().getDrawable(R.drawable.recommend_unselect));
                    recommendedText.setTextColor(getResources().getColor(black_color));
                    break;
            }

            getRecipesList(selectedTab);
        }
    };


    public class RecipeFilters {
        private String mealType;
        private String FoodType;
        private String Cuisine;

         String getMealType() {
            return mealType;
        }

        void setMealType(String mealType) {
            this.mealType = mealType;
        }

        String getFoodType() {
            return FoodType;
        }

        public void setFoodType(String foodType) {
            FoodType = foodType;
        }

        String getCuisine() {
            return Cuisine;
        }

        void setCuisine(String cuisine) {
            Cuisine = cuisine;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CUISINE_ACTIVITY_CODE && resultCode == Activity.RESULT_OK)
        {
            Bundle b = data.getExtras();
            ArrayList array = b.getStringArrayList("SELECTED_CUISINES");
            //String[] cuisines = data.getStringArrayExtra("SELECTED_CUISINES");

            int len = array.size();
            switch (len)
            {
                case 0:
                    break;
                case 1:
                    cuisine1Btn.setText(String.valueOf(array.get(0)));
                    break;
                case 2:
                    cuisine1Btn.setText(String.valueOf(array.get(0)));
                    cuisine2Btn.setText(String.valueOf(array.get(1)));
                    break;
                case 3:
                    cuisine1Btn.setText(String.valueOf(array.get(0)));
                    cuisine2Btn.setText(String.valueOf(array.get(1)));
                    cuisine3Btn.setText(String.valueOf(array.get(2)));
                    break;

            }

        }
    }
}
