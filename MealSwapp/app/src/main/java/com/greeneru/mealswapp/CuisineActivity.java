package com.greeneru.mealswapp;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import com.greeneru.mealswapp.Adapters.CategoriesAdapter;
import com.greeneru.mealswapp.Adapters.CuisinesAdapter;
import com.greeneru.mealswapp.Adapters.RecipeDetailsAdapter;
import com.greeneru.mealswapp.Models.CategoryModel;
import com.greeneru.mealswapp.Models.CuisineModel;
import com.greeneru.mealswapp.Retrofit.ApiClient;
import com.greeneru.mealswapp.Retrofit.ApiInterface;
import com.greeneru.mealswapp.Utilities.SharedPreferenceUtility;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.widget.Toast.LENGTH_LONG;

public class CuisineActivity extends AppCompatActivity {

    private View outerSpace;
    private ApiInterface apiService;
    private CuisinesAdapter adapter;
    private List<CuisineModel.Cuisines> cuisinesList;
    private Activity baseActivity;
    private Toast mToast;
    private ListView cuisineListView;
    private static final String TAG = CuisineActivity.class.getSimpleName();
    private String source;
    private List<CategoryModel.Category> categoryList;
    private CategoriesAdapter categoryAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cuisine);
        UtilsClass.makeStatusBarTransparent(getWindow(),getApplicationContext());
        baseActivity = this;
        source = getIntent().getStringExtra("source");
        apiService = ApiClient.getClient().create(ApiInterface.class);
        cuisineListView = findViewById(R.id.cuisineListView);

        if (source != null && !source.equals("category"))
        {
            setUpCuisine();
        }
        else
        {
            setUpCategory();
        }

        outerSpace = findViewById(R.id.outerSpace);
        outerSpace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                finishAfterTransition();

                if (source != null && !source.equals("category"))
                {
                    ArrayList<String> selCuisines = new ArrayList<>();

                    for (CuisineModel.Cuisines cus: cuisinesList) {
                        if (cus.isSelected()){
                            selCuisines.add(cus.getCuisineName());
                        }
                    }

                    Bundle b = new Bundle();
                    b.putStringArrayList("SELECTED_CUISINES", selCuisines);

                    Intent intt = getIntent();
                    intt.putExtras(b);
                    setResult(RESULT_OK, intt);
                }

                finish();
            }
        });

    }

    private void setUpCuisine()
    {
        cuisinesList = new ArrayList<>();
        adapter = new CuisinesAdapter(cuisinesList, this);
        getCuisinesList();
    }

    private void setUpCategory()
    {
        categoryList = new ArrayList<>();
        categoryAdapter = new CategoriesAdapter(categoryList, this);
        getCategoriesList();
    }


    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_up, R.anim.slide_down);
    }

    private void getCuisinesList()
    {
        String userId = SharedPreferenceUtility.getUserId(getApplicationContext());
        Call<CuisineModel> call = apiService.getCuisinesList(userId);

        call.enqueue(new Callback<CuisineModel>() {
            @Override
            public void onResponse(Call<CuisineModel> call, Response<CuisineModel> response) {
                UtilsClass.hideBusyAnimation(baseActivity);
                if (response.body().getStatus())
                {
                    cuisinesList = response.body().getCuisines();
                    if (cuisinesList != null && !cuisinesList.isEmpty())
                    {
                        adapter = new CuisinesAdapter(cuisinesList, getApplicationContext());
                        cuisineListView.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                    }
                }
                else // Handle failure cases here
                {
                    String mess = response.body().getMessage();
                    Log.d(TAG, "Response: " + mess);
                    showToastAtCentre("Response: " + mess, LENGTH_LONG);
                }
            }

            @Override
            public void onFailure(Call<CuisineModel> call, Throwable t) {

            }
        });
    }


    private void getCategoriesList()
    {
        String userId = SharedPreferenceUtility.getUserId(getApplicationContext());
        Call<CategoryModel> call = apiService.getCategoriesList(userId);

        call.enqueue(new Callback<CategoryModel>() {
            @Override
            public void onResponse(Call<CategoryModel> call, Response<CategoryModel> response) {
                UtilsClass.hideBusyAnimation(baseActivity);
                if (response.body().getStatus())
                {
                    categoryList = response.body().getCategories();
                    if (categoryList != null && !categoryList.isEmpty())
                    {
                        categoryAdapter = new CategoriesAdapter(categoryList, getApplicationContext());
                        cuisineListView.setAdapter(categoryAdapter);
                        categoryAdapter.notifyDataSetChanged();
                    }
                }
                else // Handle failure cases here
                {
                    String mess = response.body().getMessage();
                    Log.d(TAG, "Response: " + mess);
                    showToastAtCentre("Response: " + mess, LENGTH_LONG);
                }
            }

            @Override
            public void onFailure(Call<CategoryModel> call, Throwable t) {

            }
        });
    }

    private void showToastAtCentre(String message, int duration)
    {
        if (mToast != null)
        {
            mToast.cancel();
            mToast = null;
        }
        mToast = Toast.makeText(getApplicationContext(), message, duration);
        mToast.setGravity(Gravity.CENTER, 0, 0);
        mToast.show();
    }
}
