package com.greeneru.mealswapp.Models;

import java.util.List;

public class ApiRecipeDetailModel {

    private boolean status;
    private String message;
    private Recipe recipe;
    private List<Nutrition> Nutrition;
    private List<Ingredients> Ingredients;
    private String totalprice;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Recipe getRecipe() {
        return recipe;
    }

    public void setRecipe(Recipe recipe) {
        this.recipe = recipe;
    }

    public List<ApiRecipeDetailModel.Nutrition> getNutrition() {
        return Nutrition;
    }

    public void setNutrition(List<ApiRecipeDetailModel.Nutrition> nutrition) {
        Nutrition = nutrition;
    }

    public List<ApiRecipeDetailModel.Ingredients> getIngredients() {
        return Ingredients;
    }

    public void setIngredients(List<ApiRecipeDetailModel.Ingredients> ingredients) {
        Ingredients = ingredients;
    }

    public String getTotalprice() {
        return totalprice;
    }

    public void setTotalprice(String totalprice) {
        this.totalprice = totalprice;
    }

    public class Recipe{

        private String RecipeID;
        private String recipeName;
        private String Calories;
        private String totalTime;
        private String recipeImage;
        private String price;
        private String FavID;
        private String cartId;
        private String cartqty;

        public String getRecipeID() {
            return RecipeID;
        }

        public void setRecipeID(String recipeID) {
            RecipeID = recipeID;
        }

        public String getRecipeName() {
            return recipeName;
        }

        public void setRecipeName(String recipeName) {
            this.recipeName = recipeName;
        }

        public String getCalories() {
            return Calories;
        }

        public void setCalories(String calories) {
            Calories = calories;
        }

        public String getTotalTime() {
            return totalTime;
        }

        public void setTotalTime(String totalTime) {
            this.totalTime = totalTime;
        }

        public String getRecipeImage() {
            return recipeImage;
        }

        public void setRecipeImage(String recipeImage) {
            this.recipeImage = recipeImage;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getFavID() {
            return FavID;
        }

        public void setFavID(String favID) {
            FavID = favID;
        }
        public String getCartId() {
            return cartId;
        }

        public void setCartId(String cartId) {
            this.cartId = cartId;
        }

        public String getCartqty() {
            return cartqty;
        }

        public void setCartqty(String cartqty) {

            this.cartqty = cartqty;
            if (cartqty == null)
            {
                this.cartqty = "0";
            }
        }

    }

    public class Nutrition {
        private String itemName;
        private String itemValue;
        private String unit;

        public String getItemName() {
            return itemName;
        }

        public void setItemName(String itemName) {
            this.itemName = itemName;
        }

        public String getItemValue() {
            return itemValue;
        }

        public void setItemValue(String itemValue) {
            this.itemValue = itemValue;
        }

        public String getUnit() {
            return unit;
        }

        public void setUnit(String unit) {
            this.unit = unit;
        }
    }

    public class Ingredients
    {
        private String Id;
        private String itemName;
        private String itemValue;
        private String apiunit;
        private String SalePrice;
        private String disCountPrice;
        private String Weight;
        private String unit;
        private String ingredient_Id;
        private String ingredient_qty;
        private String orderQuantity;
        private boolean userRemoved;



        public String getItemName() {
            return itemName;
        }

        public void setItemName(String itemName) {
            this.itemName = itemName;
        }

        public String getItemValue() {
            return itemValue;
        }

        public void setItemValue(String itemValue) {
            this.itemValue = itemValue;
        }

        public String getUnit() {
            return unit;
        }

        public void setUnit(String unit) {
            this.unit = unit;
        }

        public String getSalePrice() {
            return SalePrice;
        }

        public void setSalePrice(String salePrice) {
            SalePrice = salePrice;
        }

        public String getDisCountPrice() {
            return disCountPrice;
        }

        public void setDisCountPrice(String disCountPrice) {
            this.disCountPrice = disCountPrice;
        }

        public String getWeight() {
            return Weight;
        }

        public void setWeight(String weight) {
            Weight = weight;
        }

        public String getApiunit() {
            return apiunit;
        }

        public void setApiunit(String apiunit) {
            this.apiunit = apiunit;
        }

        public String getId() {
            return Id;
        }

        public void setId(String id) {
            Id = id;
        }

        public String getOrderQuantity() {
            return orderQuantity;
        }

        public void setOrderQuantity(String orderQuantity) {
            this.orderQuantity = orderQuantity;
        }

        public String getIngredient_Id() {
            return ingredient_Id;
        }

        public void setIngredient_Id(String ingredient_Id) {
            this.ingredient_Id = ingredient_Id;
        }

        public String getIngredient_qty() {
            return ingredient_qty;
        }

        public void setIngredient_qty(String ingredient_qty) {
            this.ingredient_qty = ingredient_qty;
        }

        public boolean isUserRemoved() {
            return userRemoved;
        }

        public void setUserRemoved(boolean userRemoved) {
            this.userRemoved = userRemoved;
        }
    }


}
